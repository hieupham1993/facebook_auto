package jp.co.wssj.autosocial;

import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.chrome.ChromeDriverService;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javafx.application.Application;
import javafx.stage.Stage;
import jp.co.wssj.autosocial.api.responses.WssjLoginResponse;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.gui.splash.SplashStage;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.Utils;

public class App extends Application {

    public interface IApplicationStateChangeListener {

        default void onAppStart() {
        }

        default void onAppStopping() {
        }
    }

    private static final DatabaseHelper DATABASE;

    private static App instance;

    private List<IApplicationStateChangeListener> stateListeners;

    private User loggedFacebookUser;

    private WssjLoginResponse.FacebookAccountInfo facebookAccount;

    private Set<Cookie> cookies;

    static {
        String userDir = System.getProperty("user.dir");
        setupWebDriver(userDir);
        setupLog4J(userDir);
        DATABASE = DatabaseHelper.getInstance();
    }

    public static App getInstance() {
        return instance;
    }

    private static void setupWebDriver(String userDir) {
        if (SystemUtils.IS_OS_WINDOWS) {
            System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, userDir + "/webdriver/chromedriver.exe");
        } else if (SystemUtils.IS_OS_MAC) {
            File webDriver = new File(userDir, "/webdriver/chromedriver");
            Utils.grantFilePermission(webDriver);
            System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, userDir + "/webdriver/chromedriver");
        }
    }

    private static void setupLog4J(String userDir) {
        File logConfig = new File(userDir + "/" + Constants.LOG4J_CONFIG_FILE_NAME);
        System.setProperty("log4j.configurationFile", logConfig.getPath());
    }

    @Override
    public void start(Stage primaryStage) {
        setInstance(this);
        stateListeners = new ArrayList<>();
        new SplashStage().show();
        notifyAppStarted();
    }

    @Override
    public void stop() {
        DATABASE.closeConnection();
        notifyAppStopping();
    }

    public void setFacebookAccount(WssjLoginResponse.FacebookAccountInfo facebookAccount) {
        this.facebookAccount = facebookAccount;
    }

    public WssjLoginResponse.FacebookAccountInfo getFacebookAccount() {
        return facebookAccount;
    }

    public Set<Cookie> getFacebookCookies() {
        return cookies;
    }

    public void setFacebookCookies(Set<Cookie> cookies) {
        this.cookies = cookies;
    }

    private void setInstance(App instance) {
        App.instance = instance;
    }

    public void registerAppStateChangeListener(IApplicationStateChangeListener listener) {
        if (!stateListeners.contains(listener)) {
            stateListeners.add(listener);
        }
    }

    private void notifyAppStarted() {
        List<IApplicationStateChangeListener> listeners = new ArrayList<>(stateListeners);
        for (IApplicationStateChangeListener listener : listeners) {
            if (listener != null) {
                listener.onAppStart();
            }
        }
    }

    private void notifyAppStopping() {
        List<IApplicationStateChangeListener> listeners = new ArrayList<>(stateListeners);
        for (IApplicationStateChangeListener listener : listeners) {
            if (listener != null) {
                listener.onAppStopping();
            }
        }
    }

    public User getLoggedFacebookUser() {
        return loggedFacebookUser;
    }

    public void setLoggedFacebookUser(User loggedFacebookUser) {
        this.loggedFacebookUser = loggedFacebookUser;
    }

    public static void main(String[] args) {
        new UncaughtExceptionHandler();
        logSystemInfo();
        DATABASE.createTable();
        launch(args);
    }

    private static void logSystemInfo() {
        String systemInfo = "OS name: " + SystemUtils.OS_NAME +
                " - " +
                "OS version: " + SystemUtils.OS_VERSION +
                " - " +
                "OS arch: " + SystemUtils.OS_ARCH +
                " - " +
                "App version: " + BuildConfig.VERSION_NAME;
        Logger.debug("App", systemInfo);
    }
}
