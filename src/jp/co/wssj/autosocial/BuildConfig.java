package jp.co.wssj.autosocial;

import org.apache.commons.lang3.SystemUtils;

/**
 * Created by HieuPT on 11/10/2017.
 */
public final class BuildConfig {

    public static final boolean DEBUG = true;

    public static final String VERSION_NAME = "1.2.11.3";

    public static final String APP_NAME = "マスターシステム２";

    public static final String PLATFORM = SystemUtils.OS_NAME;

    public static final String SERVER_DEV = "http://150.95.148.203:8085";

    public static final String SERVER_RELEASE = "http://2.master-system.biz";

    private BuildConfig() {
        //no instance
    }
}
