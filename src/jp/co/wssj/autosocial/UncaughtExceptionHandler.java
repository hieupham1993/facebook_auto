package jp.co.wssj.autosocial;

import org.apache.commons.lang3.exception.ExceptionUtils;

import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.Utils;

/**
 * Created by HieuPT on 12/21/2017.
 */
public class UncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {

    public UncaughtExceptionHandler() {
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        Logger.catching(e);
        e.printStackTrace();
        Utils.sendLogToServer(ExceptionUtils.getStackTrace(e));
    }
}
