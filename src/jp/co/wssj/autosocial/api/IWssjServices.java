package jp.co.wssj.autosocial.api;

import jp.co.wssj.autosocial.BuildConfig;
import jp.co.wssj.autosocial.api.requests.WssjFacebookAccountCheckRequest;
import jp.co.wssj.autosocial.api.requests.WssjFilterFacebookFriendIdRequest;
import jp.co.wssj.autosocial.api.requests.WssjFilterFacebookLikeRequest;
import jp.co.wssj.autosocial.api.requests.WssjIncreaseSuccessRequest;
import jp.co.wssj.autosocial.api.requests.WssjListFacebookIdRequest;
import jp.co.wssj.autosocial.api.requests.WssjLoginRequest;
import jp.co.wssj.autosocial.api.requests.WssjSaveFacebookFollowRequest;
import jp.co.wssj.autosocial.api.requests.WssjSaveFacebookFriendRequest;
import jp.co.wssj.autosocial.api.requests.WssjSaveFacebookLikeRequest;
import jp.co.wssj.autosocial.api.requests.WssjSaveUserFacebook;
import jp.co.wssj.autosocial.api.requests.WssjUserUnFriendRequest;
import jp.co.wssj.autosocial.api.requests.WssjUserUnfollowRequest;
import jp.co.wssj.autosocial.api.requests.WssjWriteLogRequest;
import jp.co.wssj.autosocial.api.responses.WssjBaseResponse;
import jp.co.wssj.autosocial.api.responses.WssjCheckVersionResponse;
import jp.co.wssj.autosocial.api.responses.WssjFilterFacebookLikeResponse;
import jp.co.wssj.autosocial.api.responses.WssjGetUserFacebook;
import jp.co.wssj.autosocial.api.responses.WssjListUnFriendResponse;
import jp.co.wssj.autosocial.api.responses.WssjLoginResponse;
import jp.co.wssj.autosocial.api.responses.WssjRefreshTokenResponse;
import jp.co.wssj.autosocial.api.responses.WssjSaveFacebookAccountResponse;
import jp.co.wssj.autosocial.api.responses.WssjTaskResponse;
import jp.co.wssj.autosocial.api.responses.WssjUnfollowResponse;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.RetrofitClient;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by HieuPT on 11/15/2017.
 */
public interface IWssjServices {

    String END_POINT = Constants.BASE_WSSJ_URL + "/api/v1/";

    IWssjServices SERVICES = RetrofitClient.getInstance().getService(IWssjServices.END_POINT, IWssjServices.class);

    @POST("login")
    Call<WssjLoginResponse> login(@Body WssjLoginRequest loginInfo);

    @GET("task/get")
    Call<WssjTaskResponse> getTask(@Header("Authorization") String token);

    @GET("task/get")
    Call<WssjTaskResponse> getTask(@Header("Authorization") String token, @Query("id") int taskId);

    @GET("check_version?version_name=" + BuildConfig.VERSION_NAME)
    Call<WssjCheckVersionResponse> checkVersion();

    @POST("facebook/increment_num_action")
    Call<WssjBaseResponse<Object>> increaseSuccessCount(@Header("Authorization") String token, @Body WssjIncreaseSuccessRequest requestBody);

    @POST("facebook/acount")
    Call<WssjSaveFacebookAccountResponse> saveFacebookAccount(@Header("Authorization") String token, @Body WssjLoginRequest account);

    @POST("facebook_id/save")
    Call<WssjBaseResponse<Object>> saveUserIdFacebook(@Header("Authorization") String token, @Body WssjSaveUserFacebook userFacebook);

    @POST("facebook_id/check")
    Call<WssjGetUserFacebook> getListUserIdFacebook(@Header("Authorization") String token, @Body WssjListFacebookIdRequest requestBody);

    @POST("facebook/check_friend")
    Call<WssjGetUserFacebook> getFilteredListFriendIdFacebook(@Header("Authorization") String token, @Body WssjFilterFacebookFriendIdRequest requestBody);

    @POST("facebook/check_follow")
    Call<WssjGetUserFacebook> getFollowableListFacebook(@Header("Authorization") String token, @Body WssjFilterFacebookFriendIdRequest requestBody);

    @GET("refresh")
    Call<WssjRefreshTokenResponse> refreshToken(@Header("Authorization") String token);

    @POST("facebook/check_account")
    Call<WssjBaseResponse<Object>> checkFacebookAccount(@Header("Authorization") String token, @Body WssjFacebookAccountCheckRequest requestBody);

    @POST("facebook/friend")
    Call<WssjBaseResponse<Object>> saveFacebookFriend(@Header("Authorization") String token, @Body WssjSaveFacebookFriendRequest requestBody);

    @POST("facebook/get_friend")
    Call<WssjListUnFriendResponse> getListUserUnFriend(@Header("Authorization") String token, @Body WssjUserUnFriendRequest request);

    @POST("facebook/follow")
    Call<WssjBaseResponse<Object>> saveFacebookFollow(@Header("Authorization") String token, @Body WssjSaveFacebookFollowRequest requestBody);

    @POST("facebook/get_follow")
    Call<WssjUnfollowResponse> getListUserUnfollow(@Header("Authorization") String token, @Body WssjUserUnfollowRequest requestBody);

    @POST("facebook/save_has_follow")
    Call<WssjBaseResponse<Object>> updateFollowStatus(@Header("Authorization") String token, @Body WssjSaveFacebookFollowRequest requestBody);

    @POST("facebook/unfollow")
    Call<WssjBaseResponse<Object>> updateUnfollowStatus(@Header("Authorization") String token, @Body WssjSaveFacebookFollowRequest requestBody);

    @POST("facebook/write_log")
    Call<WssjBaseResponse<Object>> writeLog(@Header("Authorization") String token, @Body WssjWriteLogRequest requestBody);

    @POST("facebook/check_like")
    Call<WssjFilterFacebookLikeResponse> getFilteredListLikeFacebook(@Header("Authorization") String token, @Body WssjFilterFacebookLikeRequest requestBody);

    @POST("facebook/save_like")
    Call<WssjBaseResponse<Object>> saveFacebookLike(@Header("Authorization") String token, @Body WssjSaveFacebookLikeRequest requestBody);
}
