package jp.co.wssj.autosocial.api.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 12/1/2017.
 */
public class WssjFacebookAccountCheckRequest {

    @SerializedName("account_facebook")
    private final String account;

    public WssjFacebookAccountCheckRequest(String account) {
        this.account = account;
    }

    public String getAccount() {
        return account;
    }
}
