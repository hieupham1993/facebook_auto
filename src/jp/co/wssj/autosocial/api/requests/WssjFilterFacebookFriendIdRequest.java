package jp.co.wssj.autosocial.api.requests;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HieuPT on 11/27/2017.
 */
public class WssjFilterFacebookFriendIdRequest {

    @SerializedName("facebook_id_list")
    private List<String> facebookIdList;

    public WssjFilterFacebookFriendIdRequest() {
        facebookIdList = new ArrayList<>();
    }

    public List<String> getFacebookIdList() {
        return facebookIdList;
    }

    public void addAllFacebookId(List<String> facebookIdList) {
        if (facebookIdList != null) {
            this.facebookIdList.addAll(facebookIdList);
        }
    }
}
