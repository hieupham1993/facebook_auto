package jp.co.wssj.autosocial.api.requests;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by HieuPT on 11/27/2017.
 */
public class WssjFilterFacebookLikeRequest {

    @SerializedName("data")
    private List<WssjLikeMapData> data;

    public WssjFilterFacebookLikeRequest(List<WssjLikeMapData> data) {
        this.data = data;
    }

    public List<WssjLikeMapData> getData() {
        return data;
    }

    public static class WssjLikeMapData {

        @SerializedName("post_id")
        private String postId;

        @SerializedName("facebook_id_list")
        private List<String> facebookIdList;

        public WssjLikeMapData(String postId, List<String> facebookIdList) {
            this.postId = postId;
            this.facebookIdList = facebookIdList;
        }

        public String getPostId() {
            return postId;
        }

        public List<String> getFacebookIdList() {
            return facebookIdList;
        }
    }
}
