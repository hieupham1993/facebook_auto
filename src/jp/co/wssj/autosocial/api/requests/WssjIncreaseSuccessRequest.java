package jp.co.wssj.autosocial.api.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 11/15/2017.
 */
public class WssjIncreaseSuccessRequest {

    @SerializedName("task_id")
    private final int taskId;

    @SerializedName("number_increment")
    private final int numberIncrement;

    public WssjIncreaseSuccessRequest(int taskId) {
        this(taskId, 1);
    }

    public WssjIncreaseSuccessRequest(int taskId, int numberIncrement) {
        this.taskId = taskId;
        this.numberIncrement = numberIncrement;
    }

    public int getTaskId() {
        return taskId;
    }

    public int getNumberIncrement() {
        return numberIncrement;
    }
}
