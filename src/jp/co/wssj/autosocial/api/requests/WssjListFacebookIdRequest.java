package jp.co.wssj.autosocial.api.requests;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HieuPT on 11/27/2017.
 */
public class WssjListFacebookIdRequest {

    @SerializedName("task_type")
    private int taskType;

    @SerializedName("facebook_id_list")
    private List<String> facebookIdList;

    public WssjListFacebookIdRequest() {
        facebookIdList = new ArrayList<>();
    }

    public int getTaskType() {
        return taskType;
    }

    public void setTaskType(int taskType) {
        this.taskType = taskType;
    }

    public List<String> getFacebookIdList() {
        return facebookIdList;
    }

    public void addAllFacebookId(List<String> facebookIdList) {
        if (facebookIdList != null) {
            this.facebookIdList.addAll(facebookIdList);
        }
    }
}
