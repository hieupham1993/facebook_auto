package jp.co.wssj.autosocial.api.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 12/1/2017.
 */
public class WssjSaveFacebookFollowRequest {

    @SerializedName("facebook_id")
    private final String facebookId;

    public WssjSaveFacebookFollowRequest(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getFacebookId() {
        return facebookId;
    }
}
