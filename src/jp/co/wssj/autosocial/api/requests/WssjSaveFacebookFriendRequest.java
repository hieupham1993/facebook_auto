package jp.co.wssj.autosocial.api.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 12/1/2017.
 */
public class WssjSaveFacebookFriendRequest {

    public static final int STATUS_INSERT = 0;

    public static final int STATUS_UPDATE = 1;

    @SerializedName("facebook_id")
    private final String facebookId;

    @SerializedName("status")
    private final int status;

    public WssjSaveFacebookFriendRequest(String facebookId, int status) {
        this.facebookId = facebookId;
        this.status = status;
    }

    public String getFacebookId() {
        return facebookId;
    }
}
