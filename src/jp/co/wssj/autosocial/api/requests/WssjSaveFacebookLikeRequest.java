package jp.co.wssj.autosocial.api.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 12/1/2017.
 */
public class WssjSaveFacebookLikeRequest {

    @SerializedName("facebook_id")
    private final String facebookId;

    @SerializedName("post_id")
    private final String postId;

    public WssjSaveFacebookLikeRequest(String facebookId, String postId) {
        this.facebookId = facebookId;
        this.postId = postId;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public String getPostId() {
        return postId;
    }
}
