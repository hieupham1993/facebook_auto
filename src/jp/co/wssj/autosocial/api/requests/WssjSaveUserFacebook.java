package jp.co.wssj.autosocial.api.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Nguyen Huu Ta on 18/11/2017.
 */

public class WssjSaveUserFacebook {

    @SerializedName("task_type")
    private final int taskType;

    @SerializedName("facebook_id")
    private final String facebookId;

    public WssjSaveUserFacebook(int taskType, String facebookId) {
        this.taskType = taskType;
        this.facebookId = facebookId;
    }

    public int getTaskType() {
        return taskType;
    }

    public String getFacebookId() {
        return facebookId;
    }
}
