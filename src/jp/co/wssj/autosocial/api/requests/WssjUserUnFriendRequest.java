package jp.co.wssj.autosocial.api.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Nguyen Huu Ta on 7/12/2017.
 */

public class WssjUserUnFriendRequest {

    public WssjUserUnFriendRequest(int day) {
        dayUnFriend = day;
    }

    @SerializedName("max_date")
    private int dayUnFriend;

    public int getDayUnFriend() {
        return dayUnFriend;
    }
}
