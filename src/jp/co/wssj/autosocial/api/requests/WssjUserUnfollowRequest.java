package jp.co.wssj.autosocial.api.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Nguyen Huu Ta on 7/12/2017.
 */

public class WssjUserUnfollowRequest {

    public WssjUserUnfollowRequest(int day) {
        this.day = day;
    }

    @SerializedName("max_date")
    private int day;

    public int getDay() {
        return day;
    }
}
