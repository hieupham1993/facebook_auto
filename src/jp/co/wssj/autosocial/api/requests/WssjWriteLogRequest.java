package jp.co.wssj.autosocial.api.requests;

import com.google.gson.annotations.SerializedName;

import jp.co.wssj.autosocial.BuildConfig;

/**
 * Created by HieuPT on 12/25/2017.
 */
public class WssjWriteLogRequest {

    @SerializedName("platform")
    private final String platform = BuildConfig.PLATFORM;

    @SerializedName("app_version")
    private final String appVersion = BuildConfig.VERSION_NAME;

    @SerializedName("message")
    private final String message;

    public WssjWriteLogRequest(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
