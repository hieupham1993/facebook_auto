package jp.co.wssj.autosocial.api.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 11/15/2017.
 */
public class WssjBaseResponse<T> {

    @SerializedName("data")
    private T data;

    @SerializedName("success")
    private boolean isSuccess;

    @SerializedName("message")
    private String message;

    public T getData() {
        return data;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public String getMessage() {
        return message;
    }
}
