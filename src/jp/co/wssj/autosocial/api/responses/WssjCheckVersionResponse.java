package jp.co.wssj.autosocial.api.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 11/19/2017.
 */
public class WssjCheckVersionResponse extends WssjBaseResponse<WssjCheckVersionResponse.Data> {

    public static class Data {

        @SerializedName("is_newest")
        private boolean isNewest;

        @SerializedName("is_required")
        private boolean isRequired;

        public boolean isNewest() {
            return isNewest;
        }

        public boolean isRequired() {
            return isRequired;
        }
    }
}
