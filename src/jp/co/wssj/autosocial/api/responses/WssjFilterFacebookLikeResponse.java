package jp.co.wssj.autosocial.api.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Nguyen Huu Ta on 18/11/2017.
 */

public class WssjFilterFacebookLikeResponse extends WssjBaseResponse<WssjFilterFacebookLikeResponse.Data> {

    public static class Data {

        @SerializedName("facebook_data")
        private List<WssjLikeMapData> facebookData;

        public List<WssjLikeMapData> getFacebookData() {
            return facebookData;
        }
    }

    public static class WssjLikeMapData {

        @SerializedName("post_id")
        private String postId;

        @SerializedName("facebook_id_list")
        private List<String> facebookIdList;

        public String getPostId() {
            return postId;
        }

        public List<String> getFacebookIdList() {
            return facebookIdList;
        }
    }
}
