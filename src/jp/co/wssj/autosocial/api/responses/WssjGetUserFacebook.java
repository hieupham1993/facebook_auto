package jp.co.wssj.autosocial.api.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Nguyen Huu Ta on 18/11/2017.
 */

public class WssjGetUserFacebook extends WssjBaseResponse<WssjGetUserFacebook.Data> {

    public static class Data {

        @SerializedName("facebook_id_list")
        private List<String> facebookIdList;

        public List<String> getFacebookIdList() {
            return facebookIdList;
        }
    }

}
