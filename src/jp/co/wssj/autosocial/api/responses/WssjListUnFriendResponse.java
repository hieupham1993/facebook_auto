package jp.co.wssj.autosocial.api.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Nguyen Huu Ta on 7/12/2017.
 */

public class WssjListUnFriendResponse extends WssjBaseResponse<List<WssjListUnFriendResponse.UnFriendData>> {

    public class UnFriendData {

        @SerializedName("id")
        private int id;

        @SerializedName("user_id")
        private int userId;

        @SerializedName("facebook_id")
        private String facebookId;

        @SerializedName("create_at")
        private String create;

        @SerializedName("update_at")
        private String modify;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getFacebookId() {
            return facebookId;
        }

        public void setFacebookId(String facebookId) {
            this.facebookId = facebookId;
        }

        public String getCreate() {
            return create;
        }

        public void setCreate(String create) {
            this.create = create;
        }

        public String getModify() {
            return modify;
        }

        public void setModify(String modify) {
            this.modify = modify;
        }
    }
}
