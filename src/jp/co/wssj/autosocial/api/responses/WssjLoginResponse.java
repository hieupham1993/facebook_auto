package jp.co.wssj.autosocial.api.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class WssjLoginResponse extends WssjBaseResponse<WssjLoginResponse.Data> {

    public static class Data {

        @SerializedName("email")
        private String email;

        @SerializedName("user_name")
        private String userName;

        @SerializedName("access_token")
        private String accessToken;

        @SerializedName("account_facebook")
        private List<FacebookAccountInfo> facebookAccounts;

        @SerializedName("token_type")
        private String tokenType;

        @SerializedName("expires_in")
        private int expireIn;

        public String getEmail() {
            return email;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public String getUserName() {
            return userName;
        }

        public List<FacebookAccountInfo> getFacebookAccounts() {
            return facebookAccounts;
        }

        public String getTokenType() {
            return tokenType;
        }

        public int getExpireIn() {
            return expireIn;
        }
    }

    public static class FacebookAccountInfo {

        @SerializedName("email")
        private String email;

        @SerializedName("password")
        private String password;

        public FacebookAccountInfo(String email, String password) {
            this.email = email;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }
}
