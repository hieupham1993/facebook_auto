package jp.co.wssj.autosocial.api.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 11/19/2017.
 */
public class WssjRefreshTokenResponse extends WssjBaseResponse<WssjRefreshTokenResponse.Data> {

    public static class Data {

        @SerializedName("access_token")
        private String accessToken;

        @SerializedName("token_type")
        private String tokenType;

        @SerializedName("expires_in")
        private int expireIn;

        public String getAccessToken() {
            return accessToken;
        }

        public String getTokenType() {
            return tokenType;
        }

        public int getExpireIn() {
            return expireIn;
        }
    }
}
