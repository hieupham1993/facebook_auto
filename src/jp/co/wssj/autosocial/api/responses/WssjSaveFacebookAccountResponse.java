package jp.co.wssj.autosocial.api.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Nguyen Huu Ta on 16/11/2017.
 */

public class WssjSaveFacebookAccountResponse extends WssjBaseResponse<WssjSaveFacebookAccountResponse.SaveFacebookAccount> {

    public static class SaveFacebookAccount {

        @SerializedName("email")
        private String email;

        @SerializedName("password")
        private String password;
    }
}
