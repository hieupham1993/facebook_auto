package jp.co.wssj.autosocial.api.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 11/16/2017.
 */
public class WssjTaskResponse extends WssjBaseResponse<WssjTaskResponse.Data> {

    public static class Data {

        @SerializedName("rank")
        private int rank;

        @SerializedName("id")
        private int taskId;

        @SerializedName("user_id")
        private int userId;

        @SerializedName("type_action")
        private int typeAction;

        @SerializedName("type_task")
        private int typeTask;

        @SerializedName("url")
        private String url;

        @SerializedName("time_action_min")
        private int timeActionMin;

        @SerializedName("time_action_max")
        private int timeActionMax;

        @SerializedName("max_action")
        private int maxAction;

        @SerializedName("status")
        private int status;

        @SerializedName("message_content")
        private String message;

        @SerializedName("number_has_run")
        private int numberHasRun;

        @SerializedName("remain")
        private int remainAction;

        @SerializedName("max_date")
        private int maxDate;

        public int getRank() {
            return rank;
        }

        public int getTaskId() {
            return taskId;
        }

        public int getUserId() {
            return userId;
        }

        public int getTypeAction() {
            return typeAction;
        }

        public int getTypeTask() {
            return typeTask;
        }

        public String getUrl() {
            return url;
        }

        public int getTimeActionMin() {
            return timeActionMin;
        }

        public int getTimeActionMax() {
            return timeActionMax;
        }

        public int getMaxAction() {
            return maxAction;
        }

        public int getStatus() {
            return status;
        }

        public String getMessage() {
            return message;
        }

        public int getNumberHasRun() {
            return numberHasRun;
        }

        public int getRemainAction() {
            return remainAction;
        }

        public int getMaxDate() {
            return maxDate;
        }
    }
}
