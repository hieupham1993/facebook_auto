package jp.co.wssj.autosocial.api.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Nguyen Huu Ta on 7/12/2017.
 */

public class WssjUnfollowResponse extends WssjBaseResponse<WssjUnfollowResponse.Data> {

    public class Data {

        @SerializedName("facebook_id_list")
        private List<Information> facebookIdList;

        public List<Information> getFacebookInfoList() {
            return facebookIdList;
        }

    }

    public static class Information {

        @SerializedName("id")
        private int id;

        @SerializedName("user_id")
        private int userId;

        @SerializedName("facebook_id")
        private String facebookId;

        @SerializedName("create_at")
        private String create;

        @SerializedName("update_at")
        private String modify;

        public int getId() {
            return id;
        }

        public int getUserId() {
            return userId;
        }

        public String getFacebookId() {
            return facebookId;
        }

        public String getCreate() {
            return create;
        }

        public String getModify() {
            return modify;
        }
    }
}
