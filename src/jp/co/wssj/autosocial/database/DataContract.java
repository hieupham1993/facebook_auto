package jp.co.wssj.autosocial.database;

/**
 * Created by HieuPT on 11/13/2017.
 */
class DataContract {

    static class PreferenceEntry {

        public static final String TABLE_NAME = "Preference";

        public static final String COLUMN_EMAIL = "Email";

        public static final String COLUMN_PASSWORD = "Password";

        public static final String COLUMN_AUTO_LOGIN = "AutoLogin";

        public static final String COLUMN_AUTO_START = "AutoStart";

        public static final String COLUMN_ACCESS_TOKEN = "AccessToken";

        public static final String COLUMN_TOKEN_TYPE = "TokenType";

        public static final String COLUMN_TOKEN_EXPIRE_TIME = "TokenExpireTime";

        private PreferenceEntry() {
            //no instance
        }
    }

    static class BadLikePostEntry {

        public static final String TABLE_NAME = "BadLikePost";

        public static final String COLUMN_USER_ID = "UserId";

        public static final String COLUMN_POST_ID = "PostId";

        private BadLikePostEntry() {
            //no instance
        }
    }

    private DataContract() {
        //no instance
    }
}
