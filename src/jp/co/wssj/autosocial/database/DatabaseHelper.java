package jp.co.wssj.autosocial.database;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import jp.co.wssj.autosocial.models.entities.Preference;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.TextUtils;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class DatabaseHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();

    private static final String DIRECTORY = "data";

    private static final String DB_NAME = "data.db";

    private static DatabaseHelper instance;

    public static DatabaseHelper getInstance() {
        if (instance == null) {
            instance = new DatabaseHelper();
        }
        return instance;
    }

    private Connection connection;

    private DatabaseHelper() {
        try {
            Class.forName("org.sqlite.JDBC");
            ensureDbDirectoryExist();
            connection = DriverManager.getConnection("jdbc:sqlite:" + DIRECTORY + "/" + DB_NAME);
            Logger.debug(TAG, "Create database connection successfully");
        } catch (ClassNotFoundException e) {
            Logger.warning(TAG, "ClassNotFoundException: " + e.getMessage());
            Logger.catching(e);
        } catch (SQLException e) {
            Logger.warning(TAG, "SQLException: " + e.getMessage());
            Logger.catching(e);
        }
    }

    private void ensureDbDirectoryExist() {
        File dbDirectory = new File(DIRECTORY);
        if (!dbDirectory.exists()) {
            dbDirectory.mkdir();
        }
    }

    public void createTable() {
        try {
            Statement statement = connection.createStatement();
            createPreferenceTable(statement);
            createBadLikePostTable(statement);
        } catch (SQLException e) {
            Logger.warning(TAG, "SQLException: " + e.getMessage());
            Logger.catching(e);
        }
    }

    private void createPreferenceTable(Statement statement) {
        try {
            String sql = "CREATE TABLE IF NOT EXISTS " + DataContract.PreferenceEntry.TABLE_NAME
                    + "(" + DataContract.PreferenceEntry.COLUMN_AUTO_LOGIN + " INTEGER,"
                    + DataContract.PreferenceEntry.COLUMN_AUTO_START + " INTEGER,"
                    + DataContract.PreferenceEntry.COLUMN_EMAIL + " VARCHAR,"
                    + DataContract.PreferenceEntry.COLUMN_PASSWORD + " VARCHAR,"
                    + DataContract.PreferenceEntry.COLUMN_ACCESS_TOKEN + " TEXT,"
                    + DataContract.PreferenceEntry.COLUMN_TOKEN_TYPE + " VARCHAR,"
                    + DataContract.PreferenceEntry.COLUMN_TOKEN_EXPIRE_TIME + " INTEGER"
                    + ")";
            statement.execute(sql);
        } catch (SQLException e) {
            Logger.warning(TAG, "SQLException: " + e.getMessage());
            Logger.catching(e);
        }
    }

    private void createBadLikePostTable(Statement statement) {
        try {
            String sql = "CREATE TABLE IF NOT EXISTS " + DataContract.BadLikePostEntry.TABLE_NAME
                    + "(" + DataContract.BadLikePostEntry.COLUMN_USER_ID + " VARCHAR,"
                    + DataContract.BadLikePostEntry.COLUMN_POST_ID + " VARCHAR,"
                    + "PRIMARY KEY("
                    + DataContract.BadLikePostEntry.COLUMN_USER_ID + ","
                    + DataContract.BadLikePostEntry.COLUMN_POST_ID
                    + ")"
                    + ")";
            statement.execute(sql);
        } catch (SQLException e) {
            Logger.warning(TAG, "SQLException: " + e.getMessage());
            Logger.catching(e);
        }
    }

    public void closeConnection() {
        try {
            Logger.debug(TAG, "Close db connection");
            connection.close();
        } catch (SQLException e) {
            Logger.warning(TAG, "SQLException: " + e.getMessage());
            Logger.catching(e);
        }
    }

    public void updatePreference(Preference preference) {
        Logger.debug(TAG, "#updatePreference");
        if (preference != null) {
            try {
                Statement statement = connection.createStatement();
                clearPreference();
                int autoLogin = preference.isAutoLogin() ? 1 : 0;
                int autoStart = preference.isAutoStart() ? 1 : 0;
                String sql = "INSERT OR IGNORE INTO " + DataContract.PreferenceEntry.TABLE_NAME
                        + " ("
                        + DataContract.PreferenceEntry.COLUMN_EMAIL + ","
                        + DataContract.PreferenceEntry.COLUMN_PASSWORD + ","
                        + DataContract.PreferenceEntry.COLUMN_AUTO_LOGIN + ","
                        + DataContract.PreferenceEntry.COLUMN_AUTO_START + ","
                        + DataContract.PreferenceEntry.COLUMN_ACCESS_TOKEN + ","
                        + DataContract.PreferenceEntry.COLUMN_TOKEN_TYPE + ","
                        + DataContract.PreferenceEntry.COLUMN_TOKEN_EXPIRE_TIME
                        + ")"
                        + " VALUES"
                        + " ("
                        + "'" + preference.getEmail() + "',"
                        + "'" + preference.getPassword() + "',"
                        + autoLogin + ","
                        + autoStart + ","
                        + "'" + preference.getAccessToken() + "',"
                        + "'" + preference.getTokenType() + "',"
                        + preference.getTokenExpireTime()
                        + ")";
                statement.execute(sql);
            } catch (SQLException e) {
                Logger.warning(TAG, "SQLException: " + e.getMessage());
                Logger.catching(e);
            }
        }
    }

    public void updateAccessToken(String accessToken, long tokenExpireTime) {
        Logger.debug(TAG, "#updateAccessToken");
        try {
            Statement statement = connection.createStatement();
            String sql = "UPDATE " + DataContract.PreferenceEntry.TABLE_NAME
                    + " SET "
                    + DataContract.PreferenceEntry.COLUMN_ACCESS_TOKEN + "=" + "'" + accessToken + "',"
                    + DataContract.PreferenceEntry.COLUMN_TOKEN_EXPIRE_TIME + "=" + tokenExpireTime;
            statement.execute(sql);
        } catch (SQLException e) {
            Logger.warning(TAG, "SQLException: " + e.getMessage());
            Logger.catching(e);
        }
    }

    public Preference getPreference() {
        Logger.debug(TAG, "#getPreference");
        try {
            Statement statement = connection.createStatement();
            String sql = "SELECT *"
                    + " FROM " + DataContract.PreferenceEntry.TABLE_NAME;
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet != null && !resultSet.isClosed()) {
                Preference preference = new Preference();
                preference.setEmail(resultSet.getString(DataContract.PreferenceEntry.COLUMN_EMAIL));
                preference.setPassword(resultSet.getString(DataContract.PreferenceEntry.COLUMN_PASSWORD));
                preference.setAccessToken(resultSet.getString(DataContract.PreferenceEntry.COLUMN_ACCESS_TOKEN));
                preference.setAutoLogin(resultSet.getInt(DataContract.PreferenceEntry.COLUMN_AUTO_LOGIN) == 1);
                preference.setAutoStart(resultSet.getInt(DataContract.PreferenceEntry.COLUMN_AUTO_START) == 1);
                preference.setTokenType(resultSet.getString(DataContract.PreferenceEntry.COLUMN_TOKEN_TYPE));
                preference.setTokenExpireTime(resultSet.getLong(DataContract.PreferenceEntry.COLUMN_TOKEN_EXPIRE_TIME));
                resultSet.close();
                return preference;
            }
        } catch (SQLException e) {
            Logger.warning(TAG, "SQLException: " + e.getMessage());
            Logger.catching(e);
        }
        return new Preference();
    }

    public void updateBadLikePost(String userId, String postId) {
        Logger.debug(TAG, "#updateBadLikePost");
        if (!TextUtils.isEmpty(userId) && !TextUtils.isEmpty(postId)) {
            try {
                Statement statement = connection.createStatement();
                String sql = "INSERT OR IGNORE INTO " + DataContract.BadLikePostEntry.TABLE_NAME
                        + " ("
                        + DataContract.BadLikePostEntry.COLUMN_USER_ID + ","
                        + DataContract.BadLikePostEntry.COLUMN_POST_ID
                        + ")"
                        + " VALUES"
                        + " ("
                        + "'" + userId + "',"
                        + "'" + postId + "'"
                        + ")";
                statement.execute(sql);
            } catch (SQLException e) {
                Logger.warning(TAG, "SQLException: " + e.getMessage());
                Logger.catching(e);
            }
        }
    }

    public boolean isBadLikePost(String userId, String postId) {
        if (!TextUtils.isEmpty(userId) && !TextUtils.isEmpty(postId)) {
            try {
                Statement statement = connection.createStatement();
                String sql = "SELECT *"
                        + " FROM " + DataContract.BadLikePostEntry.TABLE_NAME
                        + " WHERE ("
                        + DataContract.BadLikePostEntry.COLUMN_USER_ID + "=" + "'" + userId + "'"
                        + " AND "
                        + DataContract.BadLikePostEntry.COLUMN_POST_ID + "=" + "'" + postId + "'"
                        + ")";
                ResultSet resultSet = statement.executeQuery(sql);
                if (resultSet != null && !resultSet.isClosed()) {
                    boolean isBadLikePost = resultSet.next();
                    resultSet.close();
                    Logger.debug(TAG, "#isBadLikePost: " + isBadLikePost);
                    return isBadLikePost;
                }
            } catch (SQLException e) {
                Logger.warning(TAG, "SQLException: " + e.getMessage());
                Logger.catching(e);
            }
            Logger.debug(TAG, "#isBadLikePost: false");
            return false;
        }
        Logger.debug(TAG, "#isBadLikePost: true");
        return true;
    }

    public void clearPreference() {
        Logger.debug(TAG, "#clearPreference");
        try {
            Statement statement = connection.createStatement();
            statement.execute("DELETE FROM " + DataContract.PreferenceEntry.TABLE_NAME);
        } catch (SQLException e) {
            Logger.warning(TAG, "SQLException: " + e.getMessage());
            Logger.catching(e);
        }
    }
}
