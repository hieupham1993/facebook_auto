package jp.co.wssj.autosocial.exceptions;

import jp.co.wssj.autosocial.annotations.DoNotReport;

/**
 * Created by HieuPT on 4/16/2018.
 */
@DoNotReport
public class WebDriverNotFoundException extends Exception {

    public WebDriverNotFoundException(String message, Throwable e) {
        super(message, e);
    }
}
