package jp.co.wssj.autosocial.exceptions;

import jp.co.wssj.autosocial.annotations.DoNotReport;

/**
 * Created by HieuPT on 4/16/2018.
 */
@DoNotReport
public class WebDriverNotSupportedException extends Exception {

    public WebDriverNotSupportedException(String message) {
        super(message);
    }
}
