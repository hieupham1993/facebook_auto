package jp.co.wssj.autosocial.gui.dialogsetting;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.models.entities.Preference;

/**
 * Created by Nguyen Huu Ta on 19/11/2017.
 */

public class DialogSettingsController implements Initializable {

    @FXML
    public CheckBox auto_start_login;

    @FXML
    public CheckBox auto_start_facebook;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Preference preference = DatabaseHelper.getInstance().getPreference();
        auto_start_login.setSelected(preference.isAutoLogin());
        auto_start_facebook.setSelected(preference.isAutoStart());
    }

    private Stage getStage() {
        return (Stage) auto_start_login.getScene().getWindow();
    }

    private void closeWindow() {
        getStage().close();
    }

    public void onSave(ActionEvent actionEvent) {
        Preference preference = DatabaseHelper.getInstance().getPreference();
        preference.setAutoLogin(auto_start_login.isSelected());
        preference.setAutoStart(auto_start_facebook.isSelected());
        DatabaseHelper.getInstance().updatePreference(preference);
        closeWindow();
    }
}
