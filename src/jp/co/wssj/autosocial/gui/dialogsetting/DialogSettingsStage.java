package jp.co.wssj.autosocial.gui.dialogsetting;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.utils.Logger;

/**
 * Created by Nguyen Huu Ta on 19/11/2017.
 */

public class DialogSettingsStage extends Stage {

    public DialogSettingsStage(Stage owner) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("layout_dialog_settings.fxml"));
            setTitle("Setting");
            setResizable(false);
            initModality(Modality.WINDOW_MODAL);
            initOwner(owner);
            getIcons().add(new Image(App.class.getResource("gui/settings.png").toExternalForm()));
            setScene(new Scene(root));
        } catch (IOException e) {
            Logger.warning(getClass().getSimpleName(), "IOException: " + e.getMessage());
            Logger.catching(e);
        }
    }
}
