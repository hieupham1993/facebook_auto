package jp.co.wssj.autosocial.gui.login;

/**
 * Created by HieuPT on 11/13/2017.
 */
public interface ILoginView {

    void closeScreen();

    void displayMainScreen();

    void showLoginFailureDialog(String message);

    void showProgress(boolean shown);

    void enableLoginButton(boolean enable);
}
