package jp.co.wssj.autosocial.gui.login;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.BuildConfig;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.gui.main.MainStage;
import jp.co.wssj.autosocial.models.entities.Preference;
import jp.co.wssj.autosocial.presenters.LoginPresenter;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.TextUtils;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class LoginController implements Initializable, ILoginView {

    @FXML
    private AnchorPane layoutProgressLoading;

    @FXML
    private Label version_label;

    @FXML
    private TextField email_text_field;

    @FXML
    private TextField password_text_field;

    @FXML
    private CheckBox auto_login_checkbox;

    @FXML
    private CheckBox auto_start_checkbox;

    @FXML
    private Button login_button;

    @FXML
    private Label app_name_label;

    private Stage stage;

    private LoginPresenter presenter;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        app_name_label.setText(BuildConfig.APP_NAME);
        version_label.setText(String.format(Constants.VERSION_TEXT, BuildConfig.VERSION_NAME));
        Preference preference = DatabaseHelper.getInstance().getPreference();
        if (!TextUtils.isEmpty(preference.getEmail())) {
            email_text_field.setText(preference.getEmail());
            if (!TextUtils.isEmpty(preference.getPassword())) {
                password_text_field.setText(preference.getPassword());
            }
        }
        auto_login_checkbox.setSelected(preference.isAutoLogin());
        auto_start_checkbox.setSelected(preference.isAutoStart());
        presenter = new LoginPresenter(this);
    }

    public void onLoginButtonClicked(ActionEvent actionEvent) {
        String email = email_text_field.getText();
        String password = password_text_field.getText();
        boolean isAutoLogin = auto_login_checkbox.isSelected();
        boolean isAutoStart = auto_start_checkbox.isSelected();
        validateInfoLogin(email, password, isAutoLogin, isAutoStart);
    }

    private void validateInfoLogin(String email, String password, boolean isAutoLogin, boolean isAutoStart) {
        presenter.validateInfoLogin(email, password, message -> {
            if (TextUtils.isEmpty(message)) {
                presenter.onLoginButtonClicked(email, password, isAutoLogin, isAutoStart);
            } else {
                showDialog(message);
            }
        });
    }

    private void showDialog(String message) {
        ButtonType buttonOk = new ButtonType("確認", ButtonBar.ButtonData.OK_DONE);
        Alert alert = new Alert(Alert.AlertType.INFORMATION, null, buttonOk);
        alert.setTitle("通知");
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(App.class.getResource("gui/style.css").toExternalForm());
        dialogPane.getStyleClass().add("dialog");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    private Stage getStage() {
        if (stage == null) {
            stage = (Stage) login_button.getScene().getWindow();
        }
        return stage;
    }

    @Override
    public void closeScreen() {
        Platform.runLater(() -> getStage().close());
    }

    @Override
    public void displayMainScreen() {
        Task<Void> sleeper = new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Logger.catching(e);
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> {
            showProgress(false);
            enableLoginButton(true);
            new MainStage(getStage());
        });
        new Thread(sleeper).start();

    }

    @Override
    public void showLoginFailureDialog(String message) {
        if (!TextUtils.isEmpty(message)) {
            Platform.runLater(() -> showDialog(message));
        }
    }

    @Override
    public void showProgress(boolean shown) {
        layoutProgressLoading.setVisible(shown);
    }

    @Override
    public void enableLoginButton(boolean enable) {
        login_button.setDisable(!enable);
    }
}
