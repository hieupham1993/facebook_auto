package jp.co.wssj.autosocial.gui.main;

/**
 * Created by HieuPT on 11/13/2017.
 */
public interface IMainView {

    void showLoginForm();

    void showProfileInfoPanel();

    void enableLoginForm();

    void disableLoginForm();

    void enableControlPanel();

    void disableControlPanel();

    void startLoginService(String userId, String password, boolean saveToServer);

    void startRobot();

    void stopRobot();

    void showProgress();

    void hideProgress();

    void showLoginErrorDialog();
}
