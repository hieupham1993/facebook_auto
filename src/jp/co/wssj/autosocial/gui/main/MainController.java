package jp.co.wssj.autosocial.gui.main;

import org.apache.commons.lang3.SystemUtils;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.BuildConfig;
import jp.co.wssj.autosocial.api.responses.WssjLoginResponse;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.gui.ConfirmAlertDialog;
import jp.co.wssj.autosocial.gui.dialogsetting.DialogSettingsStage;
import jp.co.wssj.autosocial.gui.login.LoginStage;
import jp.co.wssj.autosocial.interfaces.IJavascript;
import jp.co.wssj.autosocial.messenger.Message;
import jp.co.wssj.autosocial.messenger.MessageHandler;
import jp.co.wssj.autosocial.messenger.Messenger;
import jp.co.wssj.autosocial.models.entities.Preference;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.presenters.MainPresenter;
import jp.co.wssj.autosocial.tasks.BrowserTask;
import jp.co.wssj.autosocial.tasks.LoginFacebookTask;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.TextUtils;
import jp.co.wssj.autosocial.utils.Utils;
import jp.co.wssj.autosocial.webcontroller.facebook.FacebookWebController;
import netscape.javascript.JSObject;

public class MainController implements Initializable, App.IApplicationStateChangeListener, IMainView, IJavascript {

    private static final String TAG = MainController.class.getSimpleName();

    @FXML
    private Label system_information_label;

    @FXML
    private ImageView facebook_image_view;

    @FXML
    private ImageView setting_image_view;

    @FXML
    private ImageView logout_image_view;

    @FXML
    private VBox progressLoading;

    @FXML
    private Button add_task_button;

    @FXML
    private WebView log_web_view;

    @FXML
    private AnchorPane login_form;

    @FXML
    private AnchorPane profile_info_panel;

    @FXML
    private ImageView avatar_image_view;

    @FXML
    private Label fb_user_name_label;

    @FXML
    private Label fb_email_label;

    @FXML
    private Label status_label;

    @FXML
    private Button start_button;

    @FXML
    private Button stop_button;

    @FXML
    private TextField fb_username_text_field;

    @FXML
    private PasswordField fb_password_field;

    private ScheduledExecutorService robotService;

    private ExecutorService loginService;

    private LoginFacebookTask loginFacebookTask;

    private BrowserTask robotTask;

    private MainPresenter presenter;

    private final Messenger messenger = new Messenger(new MessageHandler() {

        @Override
        protected void onMessageReceived(Message msg) {
            Platform.runLater(() -> {
                switch (msg.what) {
                    case Constants.Msg.MSG_LOGIN_SUCCESS:
                        User loggedUser = (User) msg.objects[0];
                        onLoginSuccess(loggedUser);
                        break;
                    case Constants.Msg.MSG_LOGIN_FAILURE:
                        onLoginFailure();
                        break;
                    case Constants.Msg.MSG_FB_SECURITY_WARN:
                        String message = (String) msg.objects[0];
                        onFacebookSecurityWarn(message);
                        break;
                }
            });
        }
    });

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initSystemInfo();
        setupTooltip();
        setupLogPanel();
        presenter = new MainPresenter(this);
        App.getInstance().registerAppStateChangeListener(this);
        disableControlPanel();
        showLoginForm();
        WssjLoginResponse.FacebookAccountInfo facebookAccount = App.getInstance().getFacebookAccount();
        if (facebookAccount != null) {
            showProgress();
            disableLoginForm();
            fb_username_text_field.setText(facebookAccount.getEmail());
            fb_password_field.setText(facebookAccount.getPassword());
            startLoginService(facebookAccount.getEmail(), facebookAccount.getPassword(), false);
        }
    }

    private void setupLogPanel() {
        log_web_view.setContextMenuEnabled(false);
        WebEngine webEngine = log_web_view.getEngine();
        webEngine.getLoadWorker().stateProperty().addListener((observable, oldValue, newValue) -> {
            if (Worker.State.SUCCEEDED.equals(newValue)) {
                JSObject window = (JSObject) webEngine.executeScript("window");
                window.setMember("jsInterface", MainController.this);
            }
        });
        webEngine.load(getClass().getResource("log_panel.html").toExternalForm());
        Logger.setup(webEngine);
    }

    private void initSystemInfo() {
        String systemInfo = "OS name: " + SystemUtils.OS_NAME +
                " - " +
                "OS version: " + SystemUtils.OS_VERSION +
                " - " +
                "OS arch: " + SystemUtils.OS_ARCH +
                " - " +
                "App version: " + BuildConfig.VERSION_NAME;
        system_information_label.setText(systemInfo);
    }

    private void setupTooltip() {
        Tooltip.install(setting_image_view, new Tooltip("設定"));
        Tooltip.install(logout_image_view, new Tooltip("ログアウト"));
        Tooltip.install(facebook_image_view, new Tooltip("Facebook表示"));
    }

    private void setRunning(boolean running) {
        if (running) {
            status_label.setText("稼働中");
            start_button.setDisable(true);
            stop_button.setDisable(false);
        } else {
            status_label.setText("待機中");
            start_button.setDisable(false);
            stop_button.setDisable(true);
        }
    }

    private void onLoginSuccess(User user) {
        hideProgress();
        Logger.append(TAG, Constants.LogFormat.FB_FINISH_LOGIN);
        showProfileInfoPanel();
        fillProfileInfo(user);
        enableControlPanel();
        App.getInstance().setLoggedFacebookUser(user);
        if (getPreference().isAutoStart()) {
            presenter.startRobot();
        } else {
            setRunning(false);
        }
    }

    private void onLoginFailure() {
        hideProgress();
        enableLoginForm();
    }

    private void onFacebookSecurityWarn(String message) {
        Logger.append(TAG, message);
        stopRobot();
        new ConfirmAlertDialog(getStage(), Alert.AlertType.WARNING, "警告", message, null);
    }

    private Preference getPreference() {
        return DatabaseHelper.getInstance().getPreference();
    }

    private void fillProfileInfo(User user) {
        if (user != null) {
            if (!TextUtils.isEmpty(user.getAvatarUrl())) {
                avatar_image_view.setImage(new Image(user.getAvatarUrl()));
            }
            fb_user_name_label.setText(user.getName());
            fb_email_label.setText(getPreference().getEmail());
        }
    }

    private void initRobotService() {
        robotService = Executors.newSingleThreadScheduledExecutor();
        robotTask = new BrowserTask();
        robotTask.setMessenger(messenger);
    }

    private void startRobotService() {
        robotService.scheduleWithFixedDelay(robotTask, 0, 500, TimeUnit.MILLISECONDS);
    }

    @Override
    public void onAppStopping() {
        stopLogging();
        stopRobot();
    }

    private void stopLogging() {
        if (loginFacebookTask != null) {
            loginFacebookTask.destroy();
        }
        Utils.stopService(loginService);
    }

    @Override
    public void stopRobot() {
        Logger.append(TAG, Constants.LogFormat.ROBOT_STOP);
        Task<Void> sleeper = new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                if (robotTask != null) {
                    robotTask.destroy();
                }
                Utils.stopService(robotService);
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> {
            hideProgress();
            setRunning(false);
            if (App.getInstance().getLoggedFacebookUser() == null) {
                disableControlPanel();
            }
        });
        new Thread(sleeper).start();
    }

    @Override
    public void showProgress() {
        progressLoading.setVisible(true);
    }

    @Override
    public void hideProgress() {
        progressLoading.setVisible(false);
    }

    @Override
    public void showLoginErrorDialog() {
        Platform.runLater(() -> new ConfirmAlertDialog(getStage(), Alert.AlertType.INFORMATION, "警告", "このfacebookアカウントを既に登録されています。", null));
    }

    public void onLoginButtonClicked(ActionEvent actionEvent) {
        String userId = fb_username_text_field.getText();
        String password = fb_password_field.getText();
        if (!TextUtils.isEmpty(userId) && !TextUtils.isEmpty(password)) {
            presenter.onLoginButtonClicked(userId, password);
        }
    }

    @Override
    public void showLoginForm() {
        enableLoginForm();
        login_form.setVisible(true);
        profile_info_panel.setVisible(false);
    }

    @Override
    public void showProfileInfoPanel() {
        login_form.setVisible(false);
        profile_info_panel.setVisible(true);
    }

    @Override
    public void enableLoginForm() {
        login_form.setDisable(false);
    }

    @Override
    public void disableLoginForm() {
        login_form.setDisable(true);
    }

    @Override
    public void enableControlPanel() {
        start_button.setDisable(false);
    }

    @Override
    public void disableControlPanel() {
        start_button.setDisable(true);
        stop_button.setDisable(true);
    }

    @Override
    public void startLoginService(String userId, String password, boolean saveToServer) {
        Platform.runLater(() -> {
            Logger.append(TAG, String.format(Constants.LogFormat.FB_START_LOGIN, userId));
            loginService = Executors.newSingleThreadExecutor();
            loginFacebookTask = new LoginFacebookTask(userId, password, saveToServer);
            loginFacebookTask.setMessenger(messenger);
            loginService.execute(loginFacebookTask);
        });
    }

    @Override
    public void startRobot() {
        Logger.append(TAG, Constants.LogFormat.ROBOT_START);
        initRobotService();
        startRobotService();
        setRunning(true);
    }

    public void onStartRobotButtonClicked(ActionEvent actionEvent) {
        presenter.startRobot();
    }

    public void onStopRobotButtonClicked(ActionEvent actionEvent) {
        presenter.onStopRobotButtonClicked();
    }

    public void changeAccountFacebook(MouseEvent mouseEvent) {
        new ConfirmAlertDialog(getStage(), Alert.AlertType.CONFIRMATION, "警告", "アカウントを変更すると、実施しているタスクが停止されます。よろしいですか？", new ConfirmAlertDialog.IConfirmAlertDialogResult() {

            @Override
            public void onOk() {
                App.getInstance().setFacebookCookies(null);
                App.getInstance().setLoggedFacebookUser(null);
                App.getInstance().setFacebookAccount(null);
                showLoginForm();
                onStopRobotButtonClicked(null);
                disableControlPanel();
                fb_username_text_field.setText(Constants.EMPTY_STRING);
                fb_password_field.setText(Constants.EMPTY_STRING);
            }
        });
    }

    public void openTaskManagerBrowser(ActionEvent actionEvent) {
        String token = DatabaseHelper.getInstance().getPreference().getAccessToken();
        if (!TextUtils.isEmpty(token)) {
            token = token.replace("Bearer ", Constants.EMPTY_STRING);
            Utils.openBrowserSystem(Constants.BASE_WSSJ_URL + "/login?token=" + token);
        }
    }

    public void openSetting(MouseEvent mouseEvent) {
        new DialogSettingsStage(getStage()).show();
    }

    private Stage getStage() {
        return (Stage) start_button.getScene().getWindow();
    }

    public void onLogoutButtonClicked(MouseEvent mouseEvent) {
        new ConfirmAlertDialog(getStage(), Alert.AlertType.CONFIRMATION, "警告", "アカウントを変更すると、実施しているタスクが停止されます。よろしいですか？", new ConfirmAlertDialog.IConfirmAlertDialogResult() {

            @Override
            public void onOk() {
                App.getInstance().setFacebookCookies(null);
                App.getInstance().setLoggedFacebookUser(null);
                App.getInstance().setFacebookAccount(null);
                onStopRobotButtonClicked(null);
                disableControlPanel();
                new LoginStage(getStage());
            }
        });
    }

    public void onOpenProfilePageButtonClicked(MouseEvent mouseEvent) {
        Utils.openBrowserSystem(FacebookWebController.FB_BASE_URL);
    }

    @Override
    public void openUrl(String url) {
        Utils.openBrowserSystem(url);
    }
}

