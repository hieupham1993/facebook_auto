package jp.co.wssj.autosocial.gui.main;

import java.io.IOException;

import javafx.animation.FadeTransition;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;
import jp.co.wssj.autosocial.BuildConfig;
import jp.co.wssj.autosocial.utils.Logger;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class MainStage {

    private static final String TAG = MainStage.class.getSimpleName();

    public MainStage(Stage stage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("layout_main.fxml"));
            stage.setTitle(BuildConfig.APP_NAME);
            FadeTransition ft = new FadeTransition(Duration.millis(2000), root);
            ft.setFromValue(0.0);
            ft.setToValue(1.0);
            ft.play();
            stage.setScene(new Scene(root));
            stage.setMinWidth(1096);
            stage.setMinHeight(700);
            stage.setWidth(1096);
            stage.setHeight(700);
            stage.setResizable(true);
            stage.centerOnScreen();
            stage.show();
        } catch (IOException e) {
            Logger.warning(TAG, "IOException: " + e.getMessage());
            Logger.catching(e);
        }
    }
}
