package jp.co.wssj.autosocial.gui.splash;

/**
 * Created by HieuPT on 11/13/2017.
 */
public interface ISplashView {

    void displayLoginScreen();

    void displayMainScreen();

    void showRequireUpdateDialog(boolean isRequired);
}
