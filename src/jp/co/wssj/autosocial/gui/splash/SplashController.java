package jp.co.wssj.autosocial.gui.splash;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.gui.ConfirmAlertDialog;
import jp.co.wssj.autosocial.gui.login.LoginStage;
import jp.co.wssj.autosocial.gui.main.MainStage;
import jp.co.wssj.autosocial.presenters.SplashPresenter;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.Utils;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class SplashController implements Initializable, ISplashView, App.IApplicationStateChangeListener {

    @FXML
    private AnchorPane root_layout;

    private Stage stage;

    private SplashPresenter presenter;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        App.getInstance().registerAppStateChangeListener(this);
        presenter = new SplashPresenter(this);
    }

    private Stage getStage() {
        if (stage == null) {
            stage = (Stage) root_layout.getScene().getWindow();
        }
        return stage;
    }

    @Override
    public void displayLoginScreen() {
        Task<Void> sleeper = new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Logger.catching(e);
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> new LoginStage(getStage()));
        new Thread(sleeper).start();
    }

    @Override
    public void displayMainScreen() {
        Task<Void> sleeper = new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Logger.catching(e);
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> new MainStage(getStage()));
        new Thread(sleeper).start();
    }

    @Override
    public void showRequireUpdateDialog(boolean isRequired) {
        Platform.runLater(() -> new ConfirmAlertDialog(getStage(), Alert.AlertType.CONFIRMATION, Constants.DIALOG_TITLE, Constants.NEW_APP_VERSION_AVAILABLE, new ConfirmAlertDialog.IConfirmAlertDialogResult() {

            @Override
            public void onOk() {
                Utils.openBrowserSystem(Constants.BASE_WSSJ_URL);
                Platform.exit();
            }

            @Override
            public void onCancel() {
                if (isRequired) {
                    Platform.exit();
                } else {
                    presenter.onAppIsUpToDate();
                }
            }
        }));
    }

    @Override
    public void onAppStart() {
        presenter.onStart();
    }
}
