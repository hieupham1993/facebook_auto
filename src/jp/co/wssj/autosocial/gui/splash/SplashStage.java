package jp.co.wssj.autosocial.gui.splash;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.utils.Logger;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class SplashStage extends Stage {

    private static final String TAG = SplashStage.class.getSimpleName();

    public SplashStage() {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("layout_splash.fxml"));
            setResizable(false);
            setMinWidth(600);
            setMinHeight(600);
            setWidth(600);
            setHeight(600);
            getIcons().add(new Image(App.class.getResource("ms2_logo.png").toExternalForm()));
            setScene(new Scene(root));
        } catch (IOException e) {
            Logger.warning(TAG, "IOException: " + e.getMessage());
            Logger.catching(e);
        }
    }
}
