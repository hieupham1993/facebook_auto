package jp.co.wssj.autosocial.messenger;

public abstract class MessageHandler {

    protected abstract void onMessageReceived(Message msg);

    public final void send(Message msg) {
        if (msg != null) {
            onMessageReceived(msg);
        }
    }
}
