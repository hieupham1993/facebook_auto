package jp.co.wssj.autosocial.messenger;

public class Messenger {

    private final MessageHandler messageHandler;

    public Messenger(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    public void sendMessage(Message message) {
        messageHandler.send(message);
    }
}
