package jp.co.wssj.autosocial.models;

import jp.co.wssj.autosocial.api.IWssjServices;
import jp.co.wssj.autosocial.api.requests.WssjFacebookAccountCheckRequest;
import jp.co.wssj.autosocial.api.requests.WssjLoginRequest;
import jp.co.wssj.autosocial.api.responses.WssjBaseResponse;
import jp.co.wssj.autosocial.api.responses.WssjCheckVersionResponse;
import jp.co.wssj.autosocial.api.responses.WssjLoginResponse;
import jp.co.wssj.autosocial.api.responses.WssjRefreshTokenResponse;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.models.entities.Preference;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.RequestQueue;
import jp.co.wssj.autosocial.utils.TextUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class ApiModel {

    private static final String TAG = ApiModel.class.getSimpleName();

    private interface IFailureCallback {

        void onFailure(String message);
    }

    public interface ILoginCallback extends IFailureCallback {

        void onSuccess(WssjLoginResponse response);

    }

    public interface IRefreshTokenCallback extends IFailureCallback {

        void onSuccess(WssjRefreshTokenResponse.Data responseData);
    }

    public interface ICheckFacebookAccountCallback extends IFailureCallback {

        void onSuccess(boolean usable);
    }

    public interface ICheckVersionCallback {

        void onNewVersion(boolean isRequired);

        void onUpToDate();
    }

    public void login(String email, String password, ILoginCallback callback) {
        Logger.debug(TAG, "#login: " + email + " - " + password);
        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
            Logger.debug(TAG, "Username: " + email + " - Password: " + password);
            RequestQueue.getInstance().addRequest(IWssjServices.SERVICES.login(new WssjLoginRequest(email, password)), new Callback<WssjLoginResponse>() {

                @Override
                public void onResponse(Call<WssjLoginResponse> call, Response<WssjLoginResponse> response) {
                    WssjLoginResponse responseData = response.body();
                    if (responseData != null) {
                        if (responseData.isSuccess()) {
                            callback.onSuccess(responseData);
                        } else {
                            callback.onFailure(responseData.getMessage());
                        }
                    } else {
                        callback.onFailure(Constants.INTERNET_ERROR_MESSAGE);
                    }
                }

                @Override
                public void onFailure(Call<WssjLoginResponse> call, Throwable throwable) {
                    callback.onFailure(Constants.INTERNET_ERROR_MESSAGE);
                }
            });
        } else {
            callback.onFailure(Constants.EMPTY_STRING);
        }
    }

    public void refreshToken(String token, IRefreshTokenCallback callback) {
        Logger.debug(TAG, "#refreshToken");
        if (!TextUtils.isEmpty(token)) {
            RequestQueue.getInstance().addRequest(IWssjServices.SERVICES.refreshToken(token), new Callback<WssjRefreshTokenResponse>() {

                @Override
                public void onResponse(Call<WssjRefreshTokenResponse> call, Response<WssjRefreshTokenResponse> response) {
                    WssjRefreshTokenResponse responseData = response.body();
                    if (responseData != null) {
                        if (responseData.isSuccess()) {
                            WssjRefreshTokenResponse.Data data = responseData.getData();
                            if (data != null) {
                                callback.onSuccess(data);
                            } else {
                                callback.onFailure(responseData.getMessage());
                            }
                        } else {
                            callback.onFailure(responseData.getMessage());
                        }
                    } else {
                        callback.onFailure(Constants.EMPTY_STRING);
                    }
                }

                @Override
                public void onFailure(Call<WssjRefreshTokenResponse> call, Throwable throwable) {
                    callback.onFailure(Constants.EMPTY_STRING);
                }
            });
        } else {
            callback.onFailure(Constants.EMPTY_STRING);
        }
    }

    public void checkFacebookAccount(String facebookAccount, ICheckFacebookAccountCallback callback) {
        Logger.debug(TAG, "#checkFacebookAccount");
        if (!TextUtils.isEmpty(facebookAccount)) {
            Preference preference = DatabaseHelper.getInstance().getPreference();
            String accessToken = preference.getAccessToken();
            if (!TextUtils.isEmpty(accessToken)) {
                RequestQueue.getInstance().addRequest(IWssjServices.SERVICES.checkFacebookAccount(accessToken, new WssjFacebookAccountCheckRequest(facebookAccount)), new Callback<WssjBaseResponse<Object>>() {

                    @Override
                    public void onResponse(Call<WssjBaseResponse<Object>> call, Response<WssjBaseResponse<Object>> response) {
                        WssjBaseResponse responseData = response.body();
                        if (responseData != null) {
                            callback.onSuccess(responseData.isSuccess());
                        } else {
                            callback.onFailure(Constants.EMPTY_STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<WssjBaseResponse<Object>> call, Throwable throwable) {
                        callback.onFailure("Failure");
                    }
                });
            }
        }
    }

    public void checkVersion(ICheckVersionCallback callback) {
        RequestQueue.getInstance().addRequest(IWssjServices.SERVICES.checkVersion(), new Callback<WssjCheckVersionResponse>() {

            @Override
            public void onResponse(Call<WssjCheckVersionResponse> call, Response<WssjCheckVersionResponse> response) {
                WssjCheckVersionResponse versionResponse = response.body();
                if (versionResponse != null && versionResponse.isSuccess()) {
                    WssjCheckVersionResponse.Data data = versionResponse.getData();
                    if (data != null && !data.isNewest()) {
                        callback.onNewVersion(data.isRequired());
                        return;
                    }
                }
                callback.onUpToDate();
            }

            @Override
            public void onFailure(Call<WssjCheckVersionResponse> call, Throwable throwable) {
                callback.onUpToDate();
            }
        });
    }
}
