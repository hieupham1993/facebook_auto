package jp.co.wssj.autosocial.models.entities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.TextUtils;
import jp.co.wssj.autosocial.utils.Utils;

/**
 * Created by HieuPT on 11/11/2017.
 */
public class Article {

    private static final String REGEX_GROUP_POST = "(?:.*facebook.com/groups/\\w.+/permalink/)(\\d+).*";

    private static final String REGEX_USER_POST = "(?:.*facebook.com/\\w.+/posts/)(\\d+).*";

    private static final String REGEX_USER_PHOTO = "(?:.*facebook.com/photo.php\\?.*fbid=)(\\d+).*";

    private static final String REGEX_USER_VIDEO = "(?:.*facebook.com/\\w.+/videos/\\w.+/)(\\d+).*";

    private static final String REGEX_FAN_PAGE_POST = "(?:.*facebook.com/\\w.+/posts/)(\\d+).*";

    private static final String REGEX_FAN_PAGE_PHOTO = "(?:.*facebook.com/\\w.+/photos/\\w.+/)(\\d+).*";

    private static final String REGEX_FAN_PAGE_VIDEO = "(?:.*facebook.com/\\w.+/videos/)(\\d+).*";

    private static final String REGEX_FAN_PAGE_NOTE = "(?:.*facebook.com/notes/\\w.+/\\w.+/)(\\d+).*";

    private final String articleId;

    private final String url;

    private final List<User> userReactions;

    private Article(String url, String articleId) {
        this.articleId = articleId;
        this.url = url;
        userReactions = new ArrayList<>();
    }

    public String getUrl() {
        return url;
    }

    public String getArticleId() {
        return articleId;
    }

    public List<User> getUserReactions() {
        return userReactions;
    }

    public void clearReactions() {
        userReactions.clear();
    }

    public void addReactions(Collection<? extends User> reactions) {
        userReactions.addAll(reactions);
    }

    public static Article from(String url) {
        String convertedUrl = Utils.convertToFbWebUrl(url);
        if (!TextUtils.isEmpty(convertedUrl)) {
            String articleId = getArticleIdFromUrl(convertedUrl);
            if (!TextUtils.isEmpty(articleId)) {
                return new Article(convertedUrl, articleId);
            }
        }
        return null;
    }

    private static String getArticleIdFromUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            String regex = null;
            if (url.matches(REGEX_GROUP_POST)) {
                regex = REGEX_GROUP_POST;
            } else if (url.matches(REGEX_USER_POST)) {
                regex = REGEX_USER_POST;
            } else if (url.matches(REGEX_USER_PHOTO)) {
                regex = REGEX_USER_PHOTO;
            } else if (url.matches(REGEX_USER_VIDEO)) {
                regex = REGEX_USER_VIDEO;
            } else if (url.matches(REGEX_FAN_PAGE_POST)) {
                regex = REGEX_FAN_PAGE_POST;
            } else if (url.matches(REGEX_FAN_PAGE_PHOTO)) {
                regex = REGEX_FAN_PAGE_PHOTO;
            } else if (url.matches(REGEX_FAN_PAGE_VIDEO)) {
                regex = REGEX_FAN_PAGE_VIDEO;
            } else if (url.matches(REGEX_FAN_PAGE_NOTE)) {
                regex = REGEX_FAN_PAGE_NOTE;
            }
            if (!TextUtils.isEmpty(regex)) {
                Matcher matcher = Pattern.compile(regex).matcher(url);
                if (matcher.find()) {
                    return matcher.group(1);
                }
            }
        }
        return Constants.EMPTY_STRING;
    }
}
