package jp.co.wssj.autosocial.models.entities;

import java.net.URI;

import jp.co.wssj.autosocial.utils.TextUtils;
import jp.co.wssj.autosocial.webcontroller.facebook.FacebookWebController;

/**
 * Created by HieuPT on 12/5/2017.
 */
public class FanPage {

    private String pageId;

    private String uniqueId;

    private String name;

    public FanPage(String pageId) {
        this.pageId = pageId;
    }

    public String getPageId() {
        return pageId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPageUrl() {
        return FacebookWebController.FB_BASE_URL + "/" + pageId;
    }

    public String getPostUrl() {
        return getPageUrl() + "/posts";
    }

    public static FanPage fromUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            URI uri = URI.create(url);
            String pageIdPath = uri.getRawPath();
            String[] segments = pageIdPath.split("/");
            if (segments.length >= 2) {
                return new FanPage(segments[1]);
            }
        }
        return null;
    }
}
