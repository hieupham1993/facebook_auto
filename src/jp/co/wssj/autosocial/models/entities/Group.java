package jp.co.wssj.autosocial.models.entities;

import java.net.URI;

import jp.co.wssj.autosocial.utils.TextUtils;
import jp.co.wssj.autosocial.webcontroller.facebook.FacebookWebController;

/**
 * Created by HieuPT on 11/24/2017.
 */
public class Group {

    private String groupId;

    private String uniqueId;

    private String name;

    public Group(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroupUrl() {
        return FacebookWebController.FB_BASE_URL + "/groups/" + groupId;
    }

    public String getGroupMemberUrl() {
        return getGroupUrl() + "/members";
    }

    public String getGroupDiscussionUrl() {
        return getGroupUrl() + "/?ref=group_header";
    }

    public static Group fromUrl(String groupUrl) {
        if (!TextUtils.isEmpty(groupUrl)) {
            URI uri = URI.create(groupUrl);
            if (!TextUtils.isEmpty(uri.getPath()) && uri.getPath().contains("groups")) {
                String[] fragments = uri.getPath().split("/");
                if (fragments.length >= 3) {
                    String groupId = fragments[2];
                    return new Group(groupId);
                }
            }
        }
        return null;
    }

    public static boolean isGroupUrl(String url) {
        Group group = fromUrl(url);
        return group != null;
    }
}
