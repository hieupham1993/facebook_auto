package jp.co.wssj.autosocial.models.entities;

import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.TextUtils;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class Preference {

    private String email;

    private String password;

    private boolean isAutoLogin;

    private boolean isAutoStart;

    private String accessToken;

    private String tokenType;

    private long tokenExpireTime;

    public String getEmail() {
        if (!TextUtils.isEmpty(email)) {
            return email;
        }
        return Constants.EMPTY_STRING;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        if (!TextUtils.isEmpty(password)) {
            return password;
        }
        return Constants.EMPTY_STRING;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAutoLogin() {
        return isAutoLogin;
    }

    public void setAutoLogin(boolean autoLogin) {
        isAutoLogin = autoLogin;
    }

    public boolean isAutoStart() {
        return isAutoStart;
    }

    public void setAutoStart(boolean autoStart) {
        isAutoStart = autoStart;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public long getTokenExpireTime() {
        return tokenExpireTime;
    }

    public void setTokenExpireTime(long tokenExpireTime) {
        this.tokenExpireTime = tokenExpireTime;
    }
}
