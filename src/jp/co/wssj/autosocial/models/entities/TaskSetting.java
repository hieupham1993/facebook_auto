package jp.co.wssj.autosocial.models.entities;

import java.net.URI;

import jp.co.wssj.autosocial.utils.TextUtils;

/**
 * Created by HieuPT on 11/14/2017.
 */
public class TaskSetting {

    private final int taskIndex;

    private final int taskId;

    private final Option taskOption;

    private final String taskUrl;

    private final String rawTaskUrl;

    private final String taskMessage;

    private final int currentActionCount;

    private final int maxDay;

    public TaskSetting(int taskId, int taskIndex, String taskUrl, TaskSetting.Option taskOption, String taskMessage, int currentActionCount, int maxDay) {
        this.taskId = taskId;
        this.taskIndex = taskIndex;
        this.taskOption = taskOption;
        this.rawTaskUrl = taskUrl;
        if (!TextUtils.isEmpty(taskUrl)) {
            this.taskUrl = URI.create(taskUrl).toASCIIString();
        } else {
            this.taskUrl = taskUrl;
        }
        this.taskMessage = taskMessage;
        this.currentActionCount = currentActionCount;
        this.maxDay = maxDay;
    }

    public int getTaskId() {
        return taskId;
    }

    public int getTaskIndex() {
        return taskIndex;
    }

    public Option getTaskOption() {
        return taskOption;
    }

    public String getTaskUrl() {
        return taskUrl;
    }

    public String getRawTaskUrl() {
        return rawTaskUrl;
    }

    public String getTaskMessage() {
        return taskMessage;
    }

    public int getCurrentActionCount() {
        return currentActionCount;
    }

    public int getMaxDay() {
        return maxDay;
    }

    public static class Option {

        private int minTime;

        private int maxTime;

        private int maxAction;

        public Option(int minTime, int maxTime, int maxAction) {
            this.minTime = minTime;
            this.maxTime = maxTime;
            this.maxAction = maxAction;
        }

        public int getMinTime() {
            return minTime;
        }

        public void setMinTime(int minTime) {
            this.minTime = minTime;
        }

        public int getMaxTime() {
            return maxTime;
        }

        public void setMaxTime(int maxTime) {
            this.maxTime = maxTime;
        }

        public int getMaxAction() {
            return maxAction;
        }

        public void setMaxAction(int maxAction) {
            this.maxAction = maxAction;
        }
    }
}
