package jp.co.wssj.autosocial.models.entities;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.List;

import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.TextUtils;
import jp.co.wssj.autosocial.webcontroller.facebook.FacebookWebController;

/**
 * Created by HieuPT on 11/11/2017.
 */
public class User {

    public static final int TYPE_ID = 1;

    public static final int TYPE_USER_NAME = 2;

    private String uniqueId;

    private String userId;

    private int type;

    private String avatarUrl;

    private String name;

    private boolean isFollowable;

    private boolean canAddFriend;

    public User(String profileUrl) {
        User user = getUserFromProfileUrl(profileUrl);
        if (user != null) {
            this.userId = user.userId;
            this.type = user.type;
        } else {
            this.userId = Constants.EMPTY_STRING;
            this.type = TYPE_USER_NAME;
        }
    }

    public User(String userId, int type) {
        this.userId = userId;
        switch (type) {
            case TYPE_ID:
            case TYPE_USER_NAME:
                this.type = type;
                break;
            default:
                this.type = TYPE_USER_NAME;
                break;

        }
    }

    public String getUserId() {
        return userId;
    }

    public String getProfileUrl() {
        switch (type) {
            case TYPE_ID:
                return FacebookWebController.FB_PROFILE_URL + "?id=" + userId;
            default:
                return FacebookWebController.FB_BASE_URL + "/" + userId;
        }
    }

    public String getActivityLogSelfPostUrl() {
        String profileUrl = getProfileUrl();
        switch (type) {
            case TYPE_ID:
                return profileUrl + "&sk=allactivity&privacy_source=activity_log&log_filter=cluster_11";
            default:
                return profileUrl + "/allactivity?privacy_source=activity_log&log_filter=cluster_11";
        }
    }

    public String getMessengerUrl() {
        return FacebookWebController.FB_MESSAGES_BASE_URL + userId;
    }

    public String getFriendsUrl() {
        String profileUrl = getProfileUrl();
        switch (type) {
            case TYPE_ID:
                return profileUrl + "&sk=friends";
            default:
                return profileUrl + "/friends";
        }
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public boolean isFollowable() {
        return isFollowable;
    }

    public void setFollowable(boolean followable) {
        isFollowable = followable;
    }

    public boolean canAddFriend() {
        return canAddFriend;
    }

    public void setCanAddFriend(boolean canAddFriend) {
        this.canAddFriend = canAddFriend;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getName() {
        if (name == null) {
            return Constants.EMPTY_STRING;
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    private User getUserFromProfileUrl(String userProfileUrl) {
        if (!TextUtils.isEmpty(userProfileUrl)) {
            URI uri = URI.create(userProfileUrl);
            if (userProfileUrl.contains("facebook.com/profile.php")) {
                List<NameValuePair> queryParams = URLEncodedUtils.parse(uri, Charset.defaultCharset());
                if (queryParams != null && !queryParams.isEmpty()) {
                    for (NameValuePair content : queryParams) {
                        if (content != null && "id".equals(content.getName())) {
                            return new User(content.getValue(), TYPE_ID);
                        }
                    }
                }
            } else {
                String userNamePath = uri.getPath();
                String userName = userNamePath.substring(userNamePath.indexOf('/') + 1);
                return new User(userName, TYPE_USER_NAME);
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj || obj instanceof User && TextUtils.equal(((User) obj).getUserId(), userId);
    }
}
