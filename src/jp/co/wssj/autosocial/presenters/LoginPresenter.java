package jp.co.wssj.autosocial.presenters;

import org.joda.time.DateTime;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.api.responses.WssjLoginResponse;
import jp.co.wssj.autosocial.api.responses.WssjRefreshTokenResponse;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.gui.login.ILoginView;
import jp.co.wssj.autosocial.models.ApiModel;
import jp.co.wssj.autosocial.models.entities.Preference;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.TextUtils;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class LoginPresenter {

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private final ILoginView view;

    private final ApiModel apiModel;

    public interface IValidateEmail {

        void validateFinish(String message);
    }

    public LoginPresenter(ILoginView view) {
        this.view = view;
        apiModel = new ApiModel();
    }

    public void onLoginButtonClicked(String email, String password, boolean isAutoLogin, boolean isAutoStart) {
        view.showProgress(true);
        view.enableLoginButton(false);
        apiModel.login(email, password, new ApiModel.ILoginCallback() {

            @Override
            public void onSuccess(WssjLoginResponse response) {
                WssjLoginResponse.Data loginData = response.getData();
                Preference preference = new Preference();
                preference.setEmail(email);
                preference.setPassword(password);
                String token = "Bearer " + loginData.getAccessToken();
                preference.setAccessToken(token);
                List<WssjLoginResponse.FacebookAccountInfo> facebookAccounts = loginData.getFacebookAccounts();
                if (facebookAccounts != null && !facebookAccounts.isEmpty()) {
                    App.getInstance().setFacebookAccount(facebookAccounts.get(0));
                }
                preference.setAutoLogin(isAutoLogin);
                preference.setAutoStart(isAutoStart);
                preference.setTokenType(loginData.getTokenType());
                preference.setTokenExpireTime(DateTime.now().plusMinutes(loginData.getExpireIn()).getMillis());
                DatabaseHelper.getInstance().updatePreference(preference);
                refreshToken(token);
            }

            @Override
            public void onFailure(String message) {
                view.showProgress(false);
                view.enableLoginButton(true);
                view.showLoginFailureDialog(message);
            }
        });
    }

    private void refreshToken(String token) {
        apiModel.refreshToken(token, new ApiModel.IRefreshTokenCallback() {

            @Override
            public void onSuccess(WssjRefreshTokenResponse.Data responseData) {
                DatabaseHelper.getInstance().updateAccessToken("Bearer " + responseData.getAccessToken(), DateTime.now().plusMinutes(responseData.getExpireIn()).getMillis());
                view.displayMainScreen();
            }

            @Override
            public void onFailure(String message) {
                view.showProgress(false);
                view.enableLoginButton(true);
                view.showLoginFailureDialog(message);
            }
        });
    }

    public void validateInfoLogin(String email, String password, IValidateEmail callback) {
        String message = Constants.EMPTY_STRING;
        if (TextUtils.isEmpty(email)) {
            message = "メールアドレスを入力してください。";
        } else if (TextUtils.isEmpty(password)) {
            message = "パスワードを入力してください。";
        } else if (!isEmail(email)) {
            message = "メール形式を正しく入力してください。";
        }
        callback.validateFinish(message);
    }

    private boolean isEmail(String email) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }

}
