package jp.co.wssj.autosocial.presenters;

import jp.co.wssj.autosocial.gui.main.IMainView;
import jp.co.wssj.autosocial.models.ApiModel;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class MainPresenter {

    private IMainView view;

    private ApiModel model;

    public MainPresenter(IMainView view) {
        this.view = view;
        this.model = new ApiModel();
    }

    public void onLoginButtonClicked(String userId, String password) {
        view.disableLoginForm();
        view.showProgress();
        model.checkFacebookAccount(userId, new ApiModel.ICheckFacebookAccountCallback() {

            @Override
            public void onSuccess(boolean usable) {
                if (usable) {
                    view.startLoginService(userId, password, true);
                } else {
                    view.hideProgress();
                    view.enableLoginForm();
                    view.showLoginErrorDialog();
                }
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.enableLoginForm();
            }
        });
    }

    public void startRobot() {
        view.startRobot();
    }

    public void onStopRobotButtonClicked() {
        view.showProgress();
        view.stopRobot();
    }
}
