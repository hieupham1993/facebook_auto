package jp.co.wssj.autosocial.presenters;

import org.joda.time.DateTime;

import java.util.List;

import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.api.responses.WssjLoginResponse;
import jp.co.wssj.autosocial.api.responses.WssjRefreshTokenResponse;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.gui.splash.ISplashView;
import jp.co.wssj.autosocial.models.ApiModel;
import jp.co.wssj.autosocial.models.entities.Preference;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class SplashPresenter {

    private final ISplashView view;

    private final ApiModel apiModel;

    private final DatabaseHelper database;

    public SplashPresenter(ISplashView view) {
        this.view = view;
        apiModel = new ApiModel();
        database = DatabaseHelper.getInstance();
    }

    public void onStart() {
        apiModel.checkVersion(new ApiModel.ICheckVersionCallback() {

            @Override
            public void onNewVersion(boolean isRequired) {
                view.showRequireUpdateDialog(isRequired);
            }

            @Override
            public void onUpToDate() {
                onAppIsUpToDate();
            }
        });
    }

    public void onAppIsUpToDate() {
        Preference preference = database.getPreference();
        if (!preference.isAutoLogin()) {
            view.displayLoginScreen();
        } else {
            String email = preference.getEmail();
            String password = preference.getPassword();
            apiModel.login(email, password, new ApiModel.ILoginCallback() {

                @Override
                public void onSuccess(WssjLoginResponse response) {
                    WssjLoginResponse.Data loginData = response.getData();
                    Preference preference = database.getPreference();
                    String token = "Bearer " + loginData.getAccessToken();
                    preference.setAccessToken(token);
                    List<WssjLoginResponse.FacebookAccountInfo> facebookAccounts = loginData.getFacebookAccounts();
                    if (facebookAccounts != null && !facebookAccounts.isEmpty()) {
                        App.getInstance().setFacebookAccount(facebookAccounts.get(0));
                    }
                    preference.setTokenType(loginData.getTokenType());
                    preference.setTokenExpireTime(DateTime.now().plusMinutes(loginData.getExpireIn()).getMillis());
                    database.updatePreference(preference);
                    refreshToken(token);
                }

                @Override
                public void onFailure(String message) {
                    view.displayLoginScreen();
                }
            });
        }
    }

    private void refreshToken(String token) {
        apiModel.refreshToken(token, new ApiModel.IRefreshTokenCallback() {

            @Override
            public void onSuccess(WssjRefreshTokenResponse.Data responseData) {
                database.updateAccessToken("Bearer " + responseData.getAccessToken(), DateTime.now().plusMinutes(responseData.getExpireIn()).getMillis());
                view.displayMainScreen();
            }

            @Override
            public void onFailure(String message) {
                view.displayLoginScreen();
            }
        });
    }
}
