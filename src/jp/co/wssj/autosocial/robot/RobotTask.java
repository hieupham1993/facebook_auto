package jp.co.wssj.autosocial.robot;

import org.openqa.selenium.WebDriver;

import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import jp.co.wssj.autosocial.api.IWssjServices;
import jp.co.wssj.autosocial.api.requests.WssjIncreaseSuccessRequest;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.messenger.Message;
import jp.co.wssj.autosocial.messenger.Messenger;
import jp.co.wssj.autosocial.models.entities.Preference;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.RequestQueue;
import jp.co.wssj.autosocial.utils.TextUtils;
import jp.co.wssj.autosocial.utils.Utils;
import jp.co.wssj.autosocial.webcontroller.facebook.FacebookWebController;

/**
 * Created by HieuPT on 11/14/2017.
 */
public abstract class RobotTask extends FacebookWebController {

    private static final long TIME_LOG_INTERVAL = 5;

    private static final TimeUnit LOG_INTERVAL_TIME_UNIT = TimeUnit.MINUTES;

    private TaskSetting taskSetting;

    private int successCount;

    private int successActionCount;

    private Messenger messenger;

    private final IWssjServices services;

    private final ScheduledExecutorService logIntervalService;

    public RobotTask(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver);
        this.taskSetting = taskSetting;
        this.successCount = taskSetting.getCurrentActionCount();
        this.services = IWssjServices.SERVICES;
        this.logIntervalService = Executors.newSingleThreadScheduledExecutor();
        Logger.debug(getClass().getSimpleName(), "successCount " + taskSetting.getCurrentActionCount() + "/" + taskSetting.getTaskOption().getMaxAction());
        if (hasOption()) {
            Objects.requireNonNull(taskSetting.getTaskOption(), "Option is required for this task");
        }
        if (hasUrl() && TextUtils.isEmpty(taskSetting.getRawTaskUrl())) {
            throw new NullPointerException("Url is required for this task");
        }
    }

    public final void setMessenger(Messenger messenger) {
        this.messenger = messenger;
    }

    public final Messenger getMessenger() {
        return messenger;
    }

    protected final void sendMessage(Message message) {
        if (messenger != null) {
            messenger.sendMessage(message);
        }
    }

    protected TaskSetting getTaskSetting() {
        return taskSetting;
    }

    protected int getRandomInterval() {
        if (hasOption()) {
            TaskSetting.Option taskOption = taskSetting.getTaskOption();
            return Utils.getRandomNumber(taskOption.getMinTime(), taskOption.getMaxTime());
        }
        return 0;
    }

    protected final void increaseSuccessCount() {
        increaseSuccessCount(null);
    }

    protected final void increaseSuccessCount(String logUrl) {
        successActionCount++;
        successCount++;
        Logger.debug(getLogTag(), "increaseSuccessCount: " + successCount);
        Logger.debug(getLogTag(), "increaseSuccessActionCount: " + successActionCount);
        Preference preference = DatabaseHelper.getInstance().getPreference();
        if (!TextUtils.isEmpty(logUrl)) {
            Logger.append(getLogTag(), String.format(Constants.LogFormat.ACTION_LOG, successCount, getActionTypeText(), logUrl));
        }
        RequestQueue.getInstance().executeRequest(getWssjServices().increaseSuccessCount(preference.getAccessToken(), new WssjIncreaseSuccessRequest(getTaskSetting().getTaskId())), new RequestQueue.SendLogErrorToServer<>());
    }

    protected final int getSuccessCount() {
        return successCount;
    }

    protected boolean isReachMaxAction() {
        if (hasOption()) {
            TaskSetting.Option taskOption = taskSetting.getTaskOption();
            return successCount >= taskOption.getMaxAction();
        }
        return false;
    }

    protected IWssjServices getWssjServices() {
        return services;
    }

    protected final boolean hasMessage() {
        return !TextUtils.isEmpty(taskSetting.getTaskMessage());
    }

    public final void perform() throws Exception {
        if (hasOption()) {
            TaskSetting.Option option = taskSetting.getTaskOption();
            if (hasUrl() && hasMessage()) {
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_WITH_FULL_SETTING, taskSetting.getTaskIndex(), getActionTypeText(),
                        getTaskName(), getTaskSetting().getRawTaskUrl(), getTaskSetting().getTaskMessage(), option.getMinTime(), option.getMaxTime(), option.getMaxAction()));
            } else if (hasUrl()) {
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_WITH_OPTION_AND_URL, taskSetting.getTaskIndex(), getActionTypeText(),
                        getTaskName(), getTaskSetting().getRawTaskUrl(), option.getMinTime(), option.getMaxTime(), option.getMaxAction()));
            } else if (hasMessage()) {
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_WITH_OPTION_AND_MESSAGE, taskSetting.getTaskIndex(), getActionTypeText(),
                        getTaskName(), getTaskSetting().getTaskMessage(), option.getMinTime(), option.getMaxTime(), option.getMaxAction()));
            } else {
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_WITH_OPTION, taskSetting.getTaskIndex(), getActionTypeText(),
                        getTaskName(), option.getMinTime(), option.getMaxTime(), option.getMaxAction()));
            }
        } else {
            if (hasUrl() && hasMessage()) {
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_WITH_URL_AND_MESSAGE, taskSetting.getTaskIndex(), getActionTypeText(),
                        getTaskName(), getTaskSetting().getRawTaskUrl(), getTaskSetting().getTaskMessage()));
            } else if (hasUrl()) {
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_WITH_URL, taskSetting.getTaskIndex(), getActionTypeText(),
                        getTaskName(), getTaskSetting().getRawTaskUrl()));
            } else if (hasMessage()) {
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_WITH_MESSAGE, taskSetting.getTaskIndex(), getActionTypeText(),
                        getTaskName(), getTaskSetting().getTaskMessage()));
            } else {
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE, taskSetting.getTaskIndex(), getActionTypeText(), getTaskName()));
            }
        }
        Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_SEARCH_START, getSearchTarget()));
        startLogIntervalTask();
        try {
            doWork();
        } finally {
            stopLogIntervalTask();
        }
        if (successActionCount <= 0) {
            Logger.append(getLogTag(), String.format(Constants.LogFormat.NO_ACTION_LOG, getActionTypeText()));
        }
    }

    private void startLogIntervalTask() {
        logIntervalService.scheduleWithFixedDelay(() -> Logger.append(getLogTag(), Constants.LogFormat.TASK_SEARCHING), TIME_LOG_INTERVAL, TIME_LOG_INTERVAL, LOG_INTERVAL_TIME_UNIT);
    }

    protected final void stopLogIntervalTask() {
        Logger.debug(getLogTag(), "#stopLogIntervalTask");
        Utils.stopService(logIntervalService);
    }

    protected String getLogTag() {
        return getClass().getSimpleName();
    }

    protected abstract void doWork() throws Exception;

    protected abstract boolean hasOption();

    protected abstract boolean hasUrl();

    protected abstract String getSearchTarget();

    protected abstract String getActionTypeText();

    protected abstract String getTaskName();

    public abstract int getTaskType();

    public abstract int getActionType();
}
