package jp.co.wssj.autosocial.robot;

import org.openqa.selenium.WebDriver;

import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.robot.follow.FollowTask1;
import jp.co.wssj.autosocial.robot.follow.FollowTask2;
import jp.co.wssj.autosocial.robot.follow.FollowTask3;
import jp.co.wssj.autosocial.robot.follow.FollowTask4;
import jp.co.wssj.autosocial.robot.friend.FriendTask1;
import jp.co.wssj.autosocial.robot.friend.FriendTask2;
import jp.co.wssj.autosocial.robot.friend.FriendTask3;
import jp.co.wssj.autosocial.robot.friend.FriendTask4;
import jp.co.wssj.autosocial.robot.friend.FriendTask5;
import jp.co.wssj.autosocial.robot.friend.FriendTask6;
import jp.co.wssj.autosocial.robot.friend.FriendTask7;
import jp.co.wssj.autosocial.robot.friend.FriendTask8;
import jp.co.wssj.autosocial.robot.like.LikeTask1;
import jp.co.wssj.autosocial.robot.like.LikeTask10;
import jp.co.wssj.autosocial.robot.like.LikeTask2;
import jp.co.wssj.autosocial.robot.like.LikeTask3;
import jp.co.wssj.autosocial.robot.like.LikeTask4;
import jp.co.wssj.autosocial.robot.like.LikeTask5;
import jp.co.wssj.autosocial.robot.like.LikeTask6;
import jp.co.wssj.autosocial.robot.like.LikeTask7;
import jp.co.wssj.autosocial.robot.like.LikeTask8;
import jp.co.wssj.autosocial.robot.like.LikeTask9;
import jp.co.wssj.autosocial.robot.message.MessageTask1;
import jp.co.wssj.autosocial.robot.message.MessageTask2;
import jp.co.wssj.autosocial.robot.message.MessageTask3;
import jp.co.wssj.autosocial.robot.message.MessageTask4;
import jp.co.wssj.autosocial.robot.message.MessageTask5;

/**
 * Created by HieuPT on 11/14/2017.
 */
public class RobotTaskFactory {

    public static final int ACTION_TYPE_LIKE = 0;

    public static final int ACTION_TYPE_FRIEND = 1;

    public static final int ACTION_TYPE_FOLLOW = 2;

    public static final int ACTION_TYPE_SEND_MESSAGE = 3;

    public static RobotTask getTask(int actionType, int taskType, WebDriver webDriver, TaskSetting taskSetting) {
        switch (actionType) {
            case ACTION_TYPE_LIKE:
                return getLikeTask(taskType, webDriver, taskSetting);
            case ACTION_TYPE_FRIEND:
                return getFriendTask(taskType, webDriver, taskSetting);
            case ACTION_TYPE_FOLLOW:
                return getFollowTask(taskType, webDriver, taskSetting);
            case ACTION_TYPE_SEND_MESSAGE:
                return getSendMessageTask(taskType, webDriver, taskSetting);
        }
        return null;
    }

    private static RobotTask getLikeTask(int taskType, WebDriver webDriver, TaskSetting taskSetting) {
        switch (taskType) {
            case ActionLikeTask.TASK_1_ID:
                return new LikeTask1(webDriver, taskSetting);
            case ActionLikeTask.TASK_2_ID:
                return new LikeTask2(webDriver, taskSetting);
            case ActionLikeTask.TASK_3_ID:
                return new LikeTask3(webDriver, taskSetting);
            case ActionLikeTask.TASK_4_ID:
                return new LikeTask4(webDriver, taskSetting);
            case ActionLikeTask.TASK_5_ID:
                return new LikeTask5(webDriver, taskSetting);
            case ActionLikeTask.TASK_6_ID:
                return new LikeTask6(webDriver, taskSetting);
            case ActionLikeTask.TASK_7_ID:
                return new LikeTask7(webDriver, taskSetting);
            case ActionLikeTask.TASK_8_ID:
                return new LikeTask8(webDriver, taskSetting);
            case ActionLikeTask.TASK_9_ID:
                return new LikeTask9(webDriver, taskSetting);
            case ActionLikeTask.TASK_10_ID:
                return new LikeTask10(webDriver, taskSetting);

        }
        return null;
    }

    private static RobotTask getFriendTask(int taskType, WebDriver webDriver, TaskSetting taskSetting) {
        switch (taskType) {
            case ActionFriendTask.TASK_1_ID:
                return new FriendTask1(webDriver, taskSetting);
            case ActionFriendTask.TASK_2_ID:
                return new FriendTask2(webDriver, taskSetting);
            case ActionFriendTask.TASK_3_ID:
                return new FriendTask3(webDriver, taskSetting);
            case ActionFriendTask.TASK_4_ID:
                return new FriendTask4(webDriver, taskSetting);
            case ActionFriendTask.TASK_5_ID:
                return new FriendTask5(webDriver, taskSetting);
            case ActionFriendTask.TASK_7_ID:
                return new FriendTask7(webDriver, taskSetting);
            case ActionFriendTask.TASK_8_ID:
                return new FriendTask8(webDriver, taskSetting);
            case ActionFriendTask.TASK_6_ID:
                return new FriendTask6(webDriver, taskSetting);
        }
        return null;
    }

    private static RobotTask getFollowTask(int taskType, WebDriver webDriver, TaskSetting taskSetting) {
        switch (taskType) {
            case ActionFollowTask.TASK_1_ID:
                return new FollowTask1(webDriver, taskSetting);
            case ActionFollowTask.TASK_2_ID:
                return new FollowTask2(webDriver, taskSetting);
            case ActionFollowTask.TASK_3_ID:
                return new FollowTask3(webDriver, taskSetting);
            case ActionFollowTask.TASK_4_ID:
                return new FollowTask4(webDriver, taskSetting);
        }
        return null;
    }

    private static RobotTask getSendMessageTask(int taskType, WebDriver webDriver, TaskSetting taskSetting) {
        switch (taskType) {
            case ActionSendMessageTask.TASK_1_ID:
                return new MessageTask1(webDriver, taskSetting);
            case ActionSendMessageTask.TASK_2_ID:
                return new MessageTask2(webDriver, taskSetting);
            case ActionSendMessageTask.TASK_3_ID:
                return new MessageTask3(webDriver, taskSetting);
            case ActionSendMessageTask.TASK_4_ID:
                return new MessageTask4(webDriver, taskSetting);
            case ActionSendMessageTask.TASK_5_ID:
                return new MessageTask5(webDriver, taskSetting);
        }
        return null;
    }

    private RobotTaskFactory() {
        //no instance
    }

    public static class ActionLikeTask {

        public static final String ACTION_TYPE_TEXT = "いいね";

        public static final int TASK_1_ID = 0;

        public static final int TASK_2_ID = 1;

        public static final int TASK_3_ID = 2;

        public static final int TASK_4_ID = 3;

        public static final int TASK_5_ID = 4;

        public static final int TASK_6_ID = 5;

        public static final int TASK_7_ID = 6;

        public static final int TASK_8_ID = 7;

        public static final int TASK_9_ID = 8;

        public static final int TASK_10_ID = 9;

    }

    public static class ActionFriendTask {

        public static final String ACTION_TYPE_TEXT = "友達申請・承認・取り下げ";

        public static final int TASK_1_ID = 0;

        public static final int TASK_2_ID = 1;

        public static final int TASK_3_ID = 2;

        public static final int TASK_4_ID = 3;

        public static final int TASK_5_ID = 4;

        public static final int TASK_6_ID = 5;

        public static final int TASK_7_ID = 6;

        public static final int TASK_8_ID = 7;


    }

    public static class ActionFollowTask {

        public static final String ACTION_UNFOLLOW_TYPE_TEXT = "アンフォロー";

        public static final String ACTION_TYPE_TEXT = "フォロー";

        public static final int TASK_1_ID = 0;

        public static final int TASK_2_ID = 1;

        public static final int TASK_3_ID = 2;

        public static final int TASK_4_ID = 3;
    }

    public static class ActionSendMessageTask {

        public static final String ACTION_TYPE_TEXT = "メッセージ送信";

        public static final int TASK_1_ID = 0;

        public static final int TASK_2_ID = 1;

        public static final int TASK_3_ID = 2;

        public static final int TASK_4_ID = 3;

        public static final int TASK_5_ID = 4;
    }
}
