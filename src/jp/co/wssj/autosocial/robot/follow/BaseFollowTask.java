package jp.co.wssj.autosocial.robot.follow;

import org.openqa.selenium.WebDriver;

import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.robot.RobotTask;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;

/**
 * Created by HieuPT on 12/7/2017.
 */
public abstract class BaseFollowTask extends RobotTask {

    public BaseFollowTask(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected String getActionTypeText() {
        return RobotTaskFactory.ActionFollowTask.ACTION_TYPE_TEXT;
    }

    @Override
    public int getActionType() {
        return RobotTaskFactory.ACTION_TYPE_FOLLOW;
    }
}
