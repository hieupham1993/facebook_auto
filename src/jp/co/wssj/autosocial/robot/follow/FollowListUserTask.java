package jp.co.wssj.autosocial.robot.follow;

import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

import jp.co.wssj.autosocial.messenger.Message;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.TextUtils;
import jp.co.wssj.autosocial.utils.Utils;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.FollowingErrorResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.IActionResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.MessagingErrorResponse;

/**
 * Created by HieuPT on 12/7/2017.
 */
public abstract class FollowListUserTask extends BaseFollowTask {

    private static final int DELAY_SEND_MESSAGE_MIN_TIME = 30;

    private static final int DELAY_SEND_MESSAGE_MAX_TIME = 300;

    public FollowListUserTask(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected void doWork() {
        List<User> listUser = initUserList();
        if (listUser != null && !listUser.isEmpty()) {
            stopLogIntervalTask();
            Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_SEARCH_FINISH, getSearchTarget()));
            Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_START, getActionTypeText()));
            startRegisterFollowing(listUser);
        }
    }

    private void startRegisterFollowing(List<User> userList) {
        for (User user : userList) {
            if (!isReachMaxAction()) {
                followingTo(user);
            } else {
                break;
            }
        }
    }

    protected void followingTo(User user) {
        if (user != null && user.isFollowable()) {
            followingTo(user.getProfileUrl(), new IActionResponse<Void, FollowingErrorResponse>() {

                @Override
                public void onSuccess(Void data) {
                    onRegisterFollowingSuccess(user);
                    increaseSuccessCount(user.getProfileUrl());
                    if (hasMessage()) {
                        FollowListUserTask.this.wait(Utils.getRandomNumber(DELAY_SEND_MESSAGE_MIN_TIME, DELAY_SEND_MESSAGE_MAX_TIME), TimeUnit.SECONDS);
                        String message = TextUtils.replaceAll(Constants.MESSAGE_REPLACE_NAME_REGEX, getTaskSetting().getTaskMessage(), user.getName());
                        sendMessageTo(user.getUserId(), message, new IActionResponse<Void, MessagingErrorResponse>() {

                            @Override
                            public void onSuccess(Void data) {
                                prepareForNextAction();
                            }

                            @Override
                            public void onError(MessagingErrorResponse errorResponse) {
                                switch (errorResponse.getCode()) {
                                    case MessagingErrorResponse.CODE_SECURITY_CHECK:
                                        String message = String.format(Constants.LogFormat.FB_SECURITY_WARN, getActionTypeText());
                                        sendMessage(Message.obtain(Constants.Msg.MSG_FB_SECURITY_WARN, message));
                                        break;
                                    default:
//                                        Logger.append(getLogTag(), String.format(Constants.LogFormat.ACTION_SEND_MESSAGE_FAILURE, user.getProfileUrl()));
//                                        Logger.append(getLogTag(), Constants.LogFormat.ACTION_SEND_MESSAGE_FAILURE_EXTRA_INFO);
                                        prepareForNextAction();
                                        break;
                                }
                            }
                        });
                    } else {
                        prepareForNextAction();
                    }
                }

                private void prepareForNextAction() {
                    if (!isReachMaxAction()) {
                        FollowListUserTask.this.wait(getRandomInterval(), TimeUnit.SECONDS);
                    }
                }

                @Override
                public void onError(FollowingErrorResponse errorResponse) {
//                    Logger.append(getLogTag(), String.format(Constants.LogFormat.ACTION_FOLLOW_FAILURE, user.getProfileUrl()));
//                    Logger.append(getLogTag(), Constants.LogFormat.ACTION_FOLLOW_FAILURE_EXTRA_INFO);
                }
            });
        }
    }

    protected abstract void onRegisterFollowingSuccess(User user);

    protected abstract List<User> initUserList();
}
