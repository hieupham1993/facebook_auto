package jp.co.wssj.autosocial.robot.follow;

import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;
import jp.co.wssj.autosocial.utils.Logger;

/**
 * Created by HieuPT on 11/17/2017.
 */
public class FollowTask1 extends UploadFollowingUserTask {

    private static final String TAG = FollowTask1.class.getSimpleName();

    private static final String MSG_USER_DO_NOT_FOLLOWING_ANYONE = "アカウント%sは誰もフォローしていないようです。別のアカウントを設定してください。";

    public FollowTask1(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected List<User> initIntentList() {
        List<User> intentList = new ArrayList<>();
        List<User> followingList = getFollowingList(getTaskSetting().getTaskUrl());
        if (followingList != null && !followingList.isEmpty()) {
            intentList.addAll(followingList);
            followingList.forEach(user -> {
                if (user != null) {
                    List<User> childFollowingList = getFollowingList(user.getProfileUrl());
                    if (childFollowingList != null && !childFollowingList.isEmpty()) {
                        childFollowingList.forEach(currentUser -> {
                            if (!intentList.contains(currentUser)) {
                                intentList.add(currentUser);
                            }
                        });
                    }
                }
            });
        } else {
            Logger.append(TAG, String.format(MSG_USER_DO_NOT_FOLLOWING_ANYONE, getTaskSetting().getRawTaskUrl()));
        }
        return intentList;
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return true;
    }

    @Override
    protected String getSearchTarget() {
        return "ユーザーがフォロー中の人および、そのフォロー相手がフォローしている人";
    }

    @Override
    protected String getTaskName() {
        return "指定したユーザーがフォロー中の人および、そのフォロー相手がフォローしている人をフォローする";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionFollowTask.TASK_1_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
