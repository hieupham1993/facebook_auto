package jp.co.wssj.autosocial.robot.follow;

import org.openqa.selenium.WebDriver;

import java.util.List;

import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;

/**
 * Created by HieuPT on 12/7/2017.
 */
public class FollowTask2 extends UploadFollowingUserTask {

    private static final String TAG = FollowTask2.class.getSimpleName();

    public FollowTask2(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected List<User> initIntentList() {
        return getFollowerList(getTaskSetting().getTaskUrl());
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return true;
    }

    @Override
    protected String getSearchTarget() {
        return "ユーザーのフォロワー";
    }

    @Override
    protected String getTaskName() {
        return "指定したユーザーのフォロワーをフォローする";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionFollowTask.TASK_2_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
