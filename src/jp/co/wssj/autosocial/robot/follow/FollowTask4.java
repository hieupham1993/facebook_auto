package jp.co.wssj.autosocial.robot.follow;

import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.api.requests.WssjSaveFacebookFollowRequest;
import jp.co.wssj.autosocial.api.requests.WssjUserUnfollowRequest;
import jp.co.wssj.autosocial.api.responses.WssjUnfollowResponse;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.models.entities.Preference;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.RequestQueue;
import jp.co.wssj.autosocial.utils.TextUtils;
import jp.co.wssj.autosocial.utils.Utils;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.IActionResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.UnFollowErrorResponse;

/**
 * Created by HieuPT on 12/7/2017.
 */
public class FollowTask4 extends BaseFollowTask {

    private static final String TAG = FollowTask4.class.getSimpleName();

    private static final int DELAY_UNFOLLOW_MIN_TIME = 20;

    private static final int DELAY_UNFOLLOW_MAX_TIME = 30;

    public FollowTask4(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected void doWork() {
        List<String> unfollowIntentList = getListIdUnfollow();
        if (unfollowIntentList != null && !unfollowIntentList.isEmpty()) {
            List<String> unfollowedIdList = new ArrayList<>(unfollowIntentList);
            User loggedFbUser = App.getInstance().getLoggedFacebookUser();
            Preference preference = DatabaseHelper.getInstance().getPreference();
            List<User> followerUsers = getFollowerList(loggedFbUser.getProfileUrl(), true);
            if (followerUsers != null && !followerUsers.isEmpty()) {
                List<String> followedIdList = new ArrayList<>();
                for (User user : followerUsers) {
                    if (user != null) {
                        if (unfollowIntentList.contains(user.getUserId())) {
                            followedIdList.add(user.getUserId());
                        }
                    }
                }
                if (!followedIdList.isEmpty()) {
                    unfollowedIdList.removeAll(followedIdList);
                    for (String followedId : followedIdList) {
                        if (!TextUtils.isEmpty(followedId)) {
                            saveFollowedFacebookId(preference.getAccessToken(), followedId);
                        }
                    }
                }
            }
            List<User> followerUserList = unfollowedIdList.stream().map(s -> {
                if (!TextUtils.isEmpty(s)) {
                    return new User(s, User.TYPE_USER_NAME);
                }
                return null;
            }).filter(Objects::nonNull).collect(Collectors.toList());
            if (followerUserList != null && !followerUserList.isEmpty()) {
                stopLogIntervalTask();
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_SEARCH_FINISH, getSearchTarget()));
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_START, getActionTypeText()));
                int followerSize = followerUserList.size();
                for (int i = 0; i < followerSize; i++) {
                    final User user = followerUserList.get(i);
                    final int index = i;
                    if (user != null) {
                        unfollow(user.getProfileUrl(), new IActionResponse<Void, UnFollowErrorResponse>() {

                            @Override
                            public void onSuccess(Void data) {
                                saveUnfollowedFacebookId(preference.getAccessToken(), user.getUserId());
                                increaseSuccessCount(user.getProfileUrl());
                                if (index < followerSize - 1) {
                                    FollowTask4.this.wait(Utils.getRandomNumber(DELAY_UNFOLLOW_MIN_TIME, DELAY_UNFOLLOW_MAX_TIME), TimeUnit.SECONDS);
                                }
                            }
                        });
                    }
                }
            }
        }
    }

    private void saveFollowedFacebookId(String token, String facebookId) {
        RequestQueue.getInstance().executeRequest(getWssjServices().updateFollowStatus(token, new WssjSaveFacebookFollowRequest(facebookId)), new RequestQueue.SendLogErrorToServer<>());
    }

    private void saveUnfollowedFacebookId(String token, String facebookId) {
        RequestQueue.getInstance().executeRequest(getWssjServices().updateUnfollowStatus(token, new WssjSaveFacebookFollowRequest(facebookId)), new RequestQueue.SendLogErrorToServer<>());
    }

    private List<String> getListIdUnfollow() {
        Preference preference = DatabaseHelper.getInstance().getPreference();
        int day = getTaskSetting().getMaxDay();
        WssjUnfollowResponse response = RequestQueue.getInstance().executeRequest(getWssjServices().getListUserUnfollow(preference.getAccessToken(), new WssjUserUnfollowRequest(day)), new RequestQueue.SendLogErrorToServer<>());
        if (response != null && response.isSuccess()) {
            WssjUnfollowResponse.Data data = response.getData();
            if (data != null) {
                List<WssjUnfollowResponse.Information> informationList = data.getFacebookInfoList();
                if (informationList != null && !informationList.isEmpty()) {
                    return informationList.stream().map(item -> {
                        if (item != null) {
                            return item.getFacebookId();
                        }
                        return null;
                    }).filter(Objects::nonNull).collect(Collectors.toList());
                }
            }
        }
        return null;
    }

    @Override
    protected boolean hasOption() {
        return false;
    }

    @Override
    protected boolean hasUrl() {
        return false;
    }

    @Override
    protected String getSearchTarget() {
        return "フォロー返ししてくれないユーザー";
    }

    @Override
    protected String getTaskName() {
        return "フォロー返ししてくれないユーザーのフォローを解除する";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionFollowTask.TASK_4_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }

    @Override
    protected String getActionTypeText() {
        return RobotTaskFactory.ActionFollowTask.ACTION_UNFOLLOW_TYPE_TEXT;
    }
}
