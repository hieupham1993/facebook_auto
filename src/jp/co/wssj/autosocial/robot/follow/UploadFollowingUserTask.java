package jp.co.wssj.autosocial.robot.follow;

import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import jp.co.wssj.autosocial.api.requests.WssjFilterFacebookFriendIdRequest;
import jp.co.wssj.autosocial.api.requests.WssjSaveFacebookFollowRequest;
import jp.co.wssj.autosocial.api.responses.WssjGetUserFacebook;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.models.entities.Preference;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.RequestQueue;
import jp.co.wssj.autosocial.utils.TextUtils;

/**
 * Created by HieuPT on 12/7/2017.
 */
public abstract class UploadFollowingUserTask extends FollowListUserTask {

    public UploadFollowingUserTask(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected List<User> initUserList() {
        List<User> listUser = initIntentList();
        if (listUser != null) {
            Logger.debug(getLogTag(), "Found " + listUser.size());
            if (!listUser.isEmpty()) {
                Preference preference = DatabaseHelper.getInstance().getPreference();
                if (!TextUtils.isEmpty(preference.getAccessToken())) {
                    WssjGetUserFacebook wssjUserFacebook = RequestQueue.getInstance().executeRequest(getWssjServices().getFollowableListFacebook(preference.getAccessToken(), getWssjListFacebookIdRequest(listUser)), new RequestQueue.SendLogErrorToServer<>());
                    if (wssjUserFacebook != null) {
                        WssjGetUserFacebook.Data wssjUserFacebookData = wssjUserFacebook.getData();
                        if (wssjUserFacebookData != null) {
                            List<String> facebookIdList = wssjUserFacebookData.getFacebookIdList();
                            if (facebookIdList != null && !facebookIdList.isEmpty()) {
                                return listUser.stream().filter(user -> user != null && facebookIdList.contains(user.getUserId())).collect(Collectors.toList());
                            } else {
                                return null;
                            }
                        }
                    }
                }
            }
        }
        return listUser;
    }

    private WssjFilterFacebookFriendIdRequest getWssjListFacebookIdRequest(List<User> userList) {
        WssjFilterFacebookFriendIdRequest requestBody = new WssjFilterFacebookFriendIdRequest();
        if (userList != null && !userList.isEmpty()) {
            List<String> facebookIdList = new ArrayList<>();
            for (User user : userList) {
                if (user != null) {
                    facebookIdList.add(user.getUserId());
                }
            }
            requestBody.addAllFacebookId(facebookIdList);
        }
        return requestBody;
    }

    @Override
    protected void onRegisterFollowingSuccess(User user) {
        saveUserIdFacebook(user.getUserId());
    }

    private void saveUserIdFacebook(String userId) {
        Preference preference = DatabaseHelper.getInstance().getPreference();
        RequestQueue.getInstance().executeRequest(getWssjServices().saveFacebookFollow(preference.getAccessToken(), new WssjSaveFacebookFollowRequest(userId)), new RequestQueue.SendLogErrorToServer<>());
    }

    protected abstract List<User> initIntentList();
}
