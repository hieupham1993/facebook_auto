package jp.co.wssj.autosocial.robot.friend;

import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

import jp.co.wssj.autosocial.messenger.Message;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.TextUtils;
import jp.co.wssj.autosocial.utils.Utils;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.AddFriendErrorResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.IActionResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.MessagingErrorResponse;

/**
 * Created by HieuPT on 12/4/2017.
 */

public abstract class BaseAddFriendTask extends BaseFriendTask {

    private static final int DELAY_SEND_MESSAGE_MIN_TIME = 30;

    private static final int DELAY_SEND_MESSAGE_MAX_TIME = 300;

    public BaseAddFriendTask(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected void doWork() {
        List<User> listUser = initUserList();
        if (listUser != null && !listUser.isEmpty()) {
            stopLogIntervalTask();
            Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_SEARCH_FINISH, getSearchTarget()));
            Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_START, getActionTypeText()));
            startAddFriends(listUser);
        }
    }

    private void startAddFriends(List<User> listUser) {
        for (User user : listUser) {
            if (!isReachMaxAction()) {
                if (user != null && user.canAddFriend()) {
                    sendAddFriendRequestTo(user.getProfileUrl(), new IActionResponse<Void, AddFriendErrorResponse>() {

                        @Override
                        public void onSuccess(Void data) {
                            onSendAddFriendRequestSuccess(user);
                            increaseSuccessCount(user.getProfileUrl());
                            if (hasMessage()) {
                                BaseAddFriendTask.this.wait(Utils.getRandomNumber(DELAY_SEND_MESSAGE_MIN_TIME, DELAY_SEND_MESSAGE_MAX_TIME), TimeUnit.SECONDS);
                                String message = TextUtils.replaceAll(Constants.MESSAGE_REPLACE_NAME_REGEX, getTaskSetting().getTaskMessage(), user.getName());
                                sendMessageTo(user.getUserId(), message, new IActionResponse<Void, MessagingErrorResponse>() {

                                    @Override
                                    public void onSuccess(Void data) {
                                        prepareForNextAction();
                                    }

                                    @Override
                                    public void onError(MessagingErrorResponse errorResponse) {
                                        switch (errorResponse.getCode()) {
                                            case MessagingErrorResponse.CODE_SECURITY_CHECK:
                                                String message = String.format(Constants.LogFormat.FB_SECURITY_WARN, getActionTypeText());
                                                sendMessage(Message.obtain(Constants.Msg.MSG_FB_SECURITY_WARN, message));
                                                break;
                                            default:
//                                                Logger.append(getLogTag(), String.format(Constants.LogFormat.ACTION_SEND_MESSAGE_FAILURE, user.getProfileUrl()));
//                                                Logger.append(getLogTag(), Constants.LogFormat.ACTION_SEND_MESSAGE_FAILURE_EXTRA_INFO);
                                                prepareForNextAction();
                                                break;
                                        }
                                    }
                                });
                            } else {
                                prepareForNextAction();
                            }
                        }

                        private void prepareForNextAction() {
                            if (!isReachMaxAction()) {
                                BaseAddFriendTask.this.wait(getRandomInterval(), TimeUnit.SECONDS);
                            }
                        }

                        @Override
                        public void onError(AddFriendErrorResponse errorResponse) {
//                            Logger.append(getLogTag(), String.format(Constants.LogFormat.ACTION_SEND_FRIEND_REQUEST_FAILURE, user.getProfileUrl()));
//                            Logger.append(getLogTag(), Constants.LogFormat.ACTION_SEND_FRIEND_REQUEST_FAILURE_EXTRA_INFO);
                        }
                    });
                }
            } else {
                break;
            }
        }
    }

    protected abstract List<User> initUserList();

    protected abstract void onSendAddFriendRequestSuccess(User user);
}
