package jp.co.wssj.autosocial.robot.friend;

import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

import jp.co.wssj.autosocial.models.entities.Article;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;

/**
 * Created by HieuPT on 11/16/2017.
 */
public class FriendTask1 extends UploadSentAddFriendRequestTask {

    private static final String TAG = FriendTask1.class.getSimpleName();

    public FriendTask1(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected List<User> initIntentList() {
        List<User> userList = new ArrayList<>();
        List<Article> articles = getUserMostRecentArticles(getTaskSetting().getTaskUrl());
        if (articles != null && !articles.isEmpty()) {
            for (Article article : articles) {
                List<User> reactionList = getArticleReactions(article.getUrl());
                if (reactionList != null && !reactionList.isEmpty()) {
                    for (User user : reactionList) {
                        if (user != null && !userList.contains(user)) {
                            userList.add(user);
                        }
                    }
                }
            }
        }
        return userList;
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return true;
    }

    @Override
    protected String getSearchTarget() {
        return "記事";
    }

    @Override
    protected String getTaskName() {
        return "指定したユーザーの記事にいいねしている人に友達申請する";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionFriendTask.TASK_1_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
