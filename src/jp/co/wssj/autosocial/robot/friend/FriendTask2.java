package jp.co.wssj.autosocial.robot.friend;

import org.openqa.selenium.WebDriver;

import java.util.List;

import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;

/**
 * Created by Nguyen Huu Ta on 5/12/2017.
 */

public class FriendTask2 extends UploadSentAddFriendRequestTask {

    private static final String TAG = FriendTask2.class.getSimpleName();

    public FriendTask2(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return true;
    }

    @Override
    protected String getSearchTarget() {
        return "指定したユーザーの友達";
    }

    @Override
    protected String getTaskName() {
        return "指定したユーザーの友達に友達申請する";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionFriendTask.TASK_2_ID;
    }

    @Override
    protected List<User> initIntentList() {
        return getFriendList(getTaskSetting().getTaskUrl());
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
