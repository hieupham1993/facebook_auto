package jp.co.wssj.autosocial.robot.friend;

import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

import jp.co.wssj.autosocial.models.entities.Article;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;

/**
 * Created by Nguyen Huu Ta on 6/12/2017.
 */

public class FriendTask4 extends UploadSentAddFriendRequestTask {

    private static final String TAG = FriendTask4.class.getSimpleName();

    public FriendTask4(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected List<User> initIntentList() {
        List<User> userList = new ArrayList<>();
        List<Article> articles = getFanPageMostRecentArticles(getTaskSetting().getTaskUrl());
        if (articles != null && !articles.isEmpty()) {
            for (Article article : articles) {
                List<User> reactionList = getArticleReactions(article.getUrl());
                if (reactionList != null && !reactionList.isEmpty()) {
                    for (User user : reactionList) {
                        if (user != null && !userList.contains(user)) {
                            userList.add(user);
                        }
                    }
                }
            }
        }
        return userList;
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return true;
    }

    @Override
    protected String getSearchTarget() {
        return "記事";
    }

    @Override
    protected String getTaskName() {
        return "指定したFacebookページの記事にいいねしている人に友達申請する";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionFriendTask.TASK_4_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
