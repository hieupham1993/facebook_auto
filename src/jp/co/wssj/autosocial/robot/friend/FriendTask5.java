package jp.co.wssj.autosocial.robot.friend;

import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

import jp.co.wssj.autosocial.models.entities.Article;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;
import jp.co.wssj.autosocial.utils.Logger;

/**
 * Created by Nguyen Huu Ta on 6/12/2017.
 */

public class FriendTask5 extends UploadSentAddFriendRequestTask {

    private static final String TAG = FriendTask5.class.getSimpleName();

    public FriendTask5(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected List<User> initIntentList() {
        List<User> userList = new ArrayList<>();
        List<Article> articles = getGroupMostRecentArticles(getTaskSetting().getTaskUrl());
        if (articles == null || articles.isEmpty()) {
            Logger.debug(TAG, "#initIntentList: Cannot get articles by search");
            articles = getGroupMostRecentArticlesViaGroupTimeline(getTaskSetting().getTaskUrl());
        }
        if (articles != null && !articles.isEmpty()) {
            for (Article article : articles) {
                List<User> reactionList = getArticleReactions(article.getUrl());
                if (reactionList != null && !reactionList.isEmpty()) {
                    for (User user : reactionList) {
                        if (user != null && !userList.contains(user)) {
                            userList.add(user);
                        }
                    }
                }
            }
        }
        return userList;
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return true;
    }

    @Override
    protected String getSearchTarget() {
        return "記事";
    }

    @Override
    protected String getTaskName() {
        return "指定したFacebookグループの記事にいいねしている人に友達申請する";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionFriendTask.TASK_5_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
