package jp.co.wssj.autosocial.robot.friend;

import org.openqa.selenium.WebDriver;

import java.util.List;

import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;

/**
 * Created by HieuPT on 12/4/2017.
 */
public class FriendTask6 extends UploadSentAddFriendRequestTask {

    private static final String TAG = FriendTask6.class.getSimpleName();

    public FriendTask6(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected List<User> initIntentList() {
        return getGroupMembers(getTaskSetting().getTaskUrl());
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return true;
    }

    @Override
    protected String getSearchTarget() {
        return "グループに参加している人";
    }

    @Override
    protected String getTaskName() {
        return "指定したFacebookグループに参加している人に友達申請する";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionFriendTask.TASK_6_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
