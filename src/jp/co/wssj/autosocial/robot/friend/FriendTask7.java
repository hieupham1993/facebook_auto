package jp.co.wssj.autosocial.robot.friend;

import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

import jp.co.wssj.autosocial.api.requests.WssjSaveFacebookFriendRequest;
import jp.co.wssj.autosocial.api.requests.WssjUserUnFriendRequest;
import jp.co.wssj.autosocial.api.responses.WssjListUnFriendResponse;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.models.entities.Preference;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.RequestQueue;
import jp.co.wssj.autosocial.utils.Utils;
import jp.co.wssj.autosocial.webcontroller.facebook.FacebookWebController;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.AcceptFriendResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.IActionResponse;

/**
 * Created by Nguyen Huu Ta on 7/12/2017.
 */

public class FriendTask7 extends BaseFriendTask {

    private static final String TAG = FriendTask7.class.getSimpleName();

    public FriendTask7(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected void doWork() {
        int dayUnFriend = getTaskSetting().getMaxDay();
        WssjListUnFriendResponse response = getListFriend(dayUnFriend);
        if (response != null && response.isSuccess()) {
            List<WssjListUnFriendResponse.UnFriendData> listUser = response.getData();
            if (listUser != null && listUser.size() > 0) {
                stopLogIntervalTask();
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_SEARCH_FINISH, getSearchTarget()));
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_START, getActionTypeText()));
                Logger.debug(getLogTag(), "WssjListUnFriendResponse  " + listUser.size());
                for (WssjListUnFriendResponse.UnFriendData user : listUser) {
                    String linkProfile = FacebookWebController.FB_BASE_URL + "/" + user.getFacebookId();
                    unFriend(linkProfile, new IActionResponse<Void, AcceptFriendResponse>() {

                        @Override
                        public void onSuccess(Void data) {
                            saveUserIdFacebook(user.getFacebookId());
                            increaseSuccessCount(linkProfile);
                            FriendTask7.this.wait(Utils.getRandomNumber(5, 10), TimeUnit.SECONDS);
                        }
                    });
                }
            }
        }
    }

    private void saveUserIdFacebook(String userId) {
        Preference preference = DatabaseHelper.getInstance().getPreference();
        RequestQueue.getInstance().executeRequest(getWssjServices().saveFacebookFriend(preference.getAccessToken(), new WssjSaveFacebookFriendRequest(userId, WssjSaveFacebookFriendRequest.STATUS_UPDATE)), new RequestQueue.SendLogErrorToServer<>());
    }

    @Override
    protected boolean hasOption() {
        return false;
    }

    @Override
    protected boolean hasUrl() {
        return false;
    }

    @Override
    protected String getSearchTarget() {
        return "承認されない友達申請";
    }

    @Override
    protected String getTaskName() {
        return "承認されない友達申請を取り下げる";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionFriendTask.TASK_7_ID;
    }

    private WssjListUnFriendResponse getListFriend(int dayUnFriend) {
        Preference preference = DatabaseHelper.getInstance().getPreference();
        return RequestQueue.getInstance().executeRequest(getWssjServices().getListUserUnFriend(preference.getAccessToken(), new WssjUserUnFriendRequest(dayUnFriend)), new RequestQueue.SendLogErrorToServer<>());
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
