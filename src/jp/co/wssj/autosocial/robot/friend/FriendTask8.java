package jp.co.wssj.autosocial.robot.friend;

import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.Utils;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.AcceptFriendResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.IActionResponse;

/**
 * Created by Nguyen Huu Ta on 6/12/2017.
 */

public class FriendTask8 extends BaseFriendTask {

    private static final String TAG = FriendTask8.class.getSimpleName();

    public FriendTask8(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected void doWork() {
        List<User> listUser = getListUserAcceptFriend();
        if (listUser != null && listUser.size() > 0) {
            stopLogIntervalTask();
            Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_SEARCH_FINISH, getSearchTarget()));
            Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_START, getActionTypeText()));
            for (User user : listUser) {
                acceptFriend(user.getProfileUrl(), new IActionResponse<Void, AcceptFriendResponse>() {

                    @Override
                    public void onSuccess(Void data) {
                        increaseSuccessCount(user.getProfileUrl());
                        FriendTask8.this.wait(Utils.getRandomNumber(30, 60), TimeUnit.SECONDS);
                    }

                    @Override
                    public void onError(AcceptFriendResponse errorResponse) {
                        FriendTask8.this.wait(Utils.getRandomNumber(30, 60), TimeUnit.SECONDS);
                    }
                });
            }
        }
    }

    @Override
    protected boolean hasOption() {
        return false;
    }

    @Override
    protected boolean hasUrl() {
        return false;
    }

    @Override
    protected String getSearchTarget() {
        return "他ユーザーからの友達申請";
    }

    @Override
    protected String getTaskName() {
        return "他ユーザーからの友達申請を全て承認する";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionFriendTask.TASK_8_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
