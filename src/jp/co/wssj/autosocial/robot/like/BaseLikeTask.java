package jp.co.wssj.autosocial.robot.like;

import org.openqa.selenium.WebDriver;

import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.models.entities.Article;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTask;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.IActionResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.LikeErrorResponse;

/**
 * Created by HieuPT on 12/5/2017.
 */
public abstract class BaseLikeTask extends RobotTask {

    public BaseLikeTask(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected String getActionTypeText() {
        return RobotTaskFactory.ActionLikeTask.ACTION_TYPE_TEXT;
    }

    @Override
    public int getActionType() {
        return RobotTaskFactory.ACTION_TYPE_LIKE;
    }

    protected void likeArticle(Article article, IActionResponse<Void, LikeErrorResponse> response) {
        if (article != null) {
            User loggedInUser = App.getInstance().getLoggedFacebookUser();
            if (loggedInUser != null) {
                DatabaseHelper databaseHelper = DatabaseHelper.getInstance();
                if (!databaseHelper.isBadLikePost(loggedInUser.getUserId(), article.getArticleId())) {
                    likeArticle(article.getUrl(), new IActionResponse<Void, LikeErrorResponse>() {

                        @Override
                        public void onSuccess(Void data) {
                            if (response != null) {
                                response.onSuccess(data);
                            }
                        }

                        @Override
                        public void onError(LikeErrorResponse errorResponse) {
                            databaseHelper.updateBadLikePost(loggedInUser.getUserId(), article.getArticleId());
                            if (response != null) {
                                response.onError(errorResponse);
                            }
                        }
                    });
                }
            }
        }
    }
}
