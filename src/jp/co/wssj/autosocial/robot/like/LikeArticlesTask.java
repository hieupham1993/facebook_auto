package jp.co.wssj.autosocial.robot.like;

import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

import jp.co.wssj.autosocial.models.entities.Article;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.IActionResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.LikeErrorResponse;

/**
 * Created by HieuPT on 12/5/2017.
 */
public abstract class LikeArticlesTask extends BaseLikeTask {

    public LikeArticlesTask(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected void doWork() {
        List<Article> articles = initArticleList();
        if (articles != null && !articles.isEmpty()) {
            stopLogIntervalTask();
            Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_SEARCH_FINISH, getSearchTarget()));
            Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_START, getActionTypeText()));
            for (Article article : articles) {
                if (!isReachMaxAction()) {
                    if (article != null) {
                        likeArticle(article, new IActionResponse<Void, LikeErrorResponse>() {

                            @Override
                            public void onSuccess(Void data) {
                                increaseSuccessCount(article.getUrl());
                                if (!isReachMaxAction()) {
                                    LikeArticlesTask.this.wait(getRandomInterval(), TimeUnit.SECONDS);
                                }
                            }

                            @Override
                            public void onError(LikeErrorResponse errorResponse) {
//                                Logger.append(getLogTag(), String.format(Constants.LogFormat.ACTION_LIKE_FAILURE, article.getUrl()));
//                                Logger.append(getLogTag(), Constants.LogFormat.ACTION_LIKE_FAILURE_EXTRA_INFO);
                            }
                        });
                    }
                }
            }
        }
    }

    protected abstract List<Article> initArticleList();
}
