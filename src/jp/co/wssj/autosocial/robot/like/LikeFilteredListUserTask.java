package jp.co.wssj.autosocial.robot.like;

import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import jp.co.wssj.autosocial.api.IWssjServices;
import jp.co.wssj.autosocial.api.requests.WssjFilterFacebookLikeRequest;
import jp.co.wssj.autosocial.api.requests.WssjSaveFacebookLikeRequest;
import jp.co.wssj.autosocial.api.responses.WssjFilterFacebookLikeResponse;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.models.entities.Preference;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.RequestQueue;
import jp.co.wssj.autosocial.utils.TextUtils;

/**
 * Created by HieuPT on 12/29/2017.
 */
public abstract class LikeFilteredListUserTask extends LikeLatestUserArticleTask {

    private final Map<String, String> userArticleMap;

    public LikeFilteredListUserTask(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
        userArticleMap = new HashMap<>();
    }

    @Override
    protected List<User> initUserList() {
        List<User> userList = null;
        Map<String, List<User>> userMap = initIntentUserList();
        if (userMap != null && !userMap.isEmpty()) {
            userList = initUserList(userMap);
            if (!userList.isEmpty()) {
                filterUserList(userList, userMap);
            }
        }
        return userList;
    }

    private void filterUserList(List<User> userList, Map<String, List<User>> userMap) {
        Preference preference = DatabaseHelper.getInstance().getPreference();
        if (!TextUtils.isEmpty(preference.getAccessToken())) {
            WssjFilterFacebookLikeRequest requestBody = buildFilterFacebookLikeRequest(userMap);
            WssjFilterFacebookLikeResponse response = RequestQueue.getInstance().executeRequest(IWssjServices.SERVICES.getFilteredListLikeFacebook(preference.getAccessToken(), requestBody), new RequestQueue.SendLogErrorToServer<>());
            if (response != null) {
                WssjFilterFacebookLikeResponse.Data responseData = response.getData();
                if (responseData != null) {
                    List<WssjFilterFacebookLikeResponse.WssjLikeMapData> facebookDataList = responseData.getFacebookData();
                    if (facebookDataList != null && !facebookDataList.isEmpty()) {
                        List<String> userIdList = new ArrayList<>();
                        facebookDataList.forEach(data -> {
                            if (data != null) {
                                List<String> facebookIdList = data.getFacebookIdList();
                                if (facebookIdList != null && !facebookIdList.isEmpty()) {
                                    facebookIdList.forEach(facebookId -> {
                                        if (!userIdList.contains(facebookId)) {
                                            userIdList.add(facebookId);
                                            userArticleMap.put(facebookId, data.getPostId());
                                        }
                                    });
                                }
                            }
                        });
                        if (!userIdList.isEmpty()) {
                            userList.removeIf(user -> !userIdList.contains(user.getUserId()));
                        } else {
                            userList.clear();
                        }
                    } else {
                        userList.clear();
                    }
                }
            }
        }
    }

    private WssjFilterFacebookLikeRequest buildFilterFacebookLikeRequest(Map<String, List<User>> userMap) {
        List<WssjFilterFacebookLikeRequest.WssjLikeMapData> dataList = new ArrayList<>();
        for (String postId : userMap.keySet()) {
            List<User> userList = userMap.get(postId);
            if (userList != null && !userList.isEmpty()) {
                List<String> userIdList = userList.stream().map(user -> {
                    if (user != null) {
                        return user.getUserId();
                    }
                    return Constants.EMPTY_STRING;
                }).filter(s -> !TextUtils.isEmpty(s)).collect(Collectors.toList());
                if (!userIdList.isEmpty()) {
                    dataList.add(new WssjFilterFacebookLikeRequest.WssjLikeMapData(postId, userIdList));
                }
            }
        }
        if (!dataList.isEmpty()) {
            return new WssjFilterFacebookLikeRequest(dataList);
        }
        return null;
    }

    private List<User> initUserList(Map<String, List<User>> userMap) {
        List<User> userList = new ArrayList<>();
        for (String postId : userMap.keySet()) {
            List<User> reactionList = userMap.get(postId);
            if (reactionList != null && !reactionList.isEmpty()) {
                reactionList.forEach(user -> {
                    if (user != null && !userList.contains(user)) {
                        userList.add(user);
                    }
                });
            }
        }
        return userList;
    }

    @Override
    protected void onLikeSucceeded(User user, String articleUrl) {
        if (user != null) {
            String userId = user.getUserId();
            saveLike(userId, userArticleMap.get(userId));
        }
    }

    private void saveLike(String userId, String postId) {
        if (!TextUtils.isEmpty(userId) && !TextUtils.isEmpty(postId)) {
            Preference preference = DatabaseHelper.getInstance().getPreference();
            RequestQueue.getInstance().executeRequest(getWssjServices().saveFacebookLike(preference.getAccessToken(), new WssjSaveFacebookLikeRequest(userId, postId)), new RequestQueue.SendLogErrorToServer<>());
        }
    }

    protected abstract Map<String, List<User>> initIntentUserList();
}
