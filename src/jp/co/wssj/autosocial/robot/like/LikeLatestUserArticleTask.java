package jp.co.wssj.autosocial.robot.like;

import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.models.entities.Article;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.TextUtils;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.IActionResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.LikeErrorResponse;

/**
 * Created by HieuPT on 12/5/2017.
 */
public abstract class LikeLatestUserArticleTask extends BaseLikeTask {

    public LikeLatestUserArticleTask(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected void doWork() {
        List<User> userList = initUserList();
        if (userList != null) {
            Logger.debug(getLogTag(), "Found " + userList.size());
            if (!userList.isEmpty()) {
                stopLogIntervalTask();
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_SEARCH_FINISH, getSearchTarget()));
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_START, getActionTypeText()));
                User loggedFbUser = App.getInstance().getLoggedFacebookUser();
                userList.removeIf(user -> {
                    if (user != null) {
                        if (!TextUtils.isEmpty(user.getUserId())) {
                            return user.getUserId().equals(loggedFbUser.getUserId());
                        }
                    }
                    return true;
                });
                for (User user : userList) {
                    if (!isReachMaxAction()) {
                        if (user != null) {
                            Article userArticle = getUserLatestArticle(user.getProfileUrl());
                            if (userArticle != null) {
                                likeArticle(userArticle, new IActionResponse<Void, LikeErrorResponse>() {

                                    @Override
                                    public void onSuccess(Void data) {
                                        onLikeSucceeded(user, userArticle.getUrl());
                                        increaseSuccessCount(userArticle.getUrl());
                                        if (!isReachMaxAction()) {
                                            LikeLatestUserArticleTask.this.wait(getRandomInterval(), TimeUnit.SECONDS);
                                        }
                                    }

                                    @Override
                                    public void onError(LikeErrorResponse errorResponse) {
//                                        Logger.append(getLogTag(), String.format(Constants.LogFormat.ACTION_LIKE_FAILURE, userArticle.getUrl()));
//                                        Logger.append(getLogTag(), Constants.LogFormat.ACTION_LIKE_FAILURE_EXTRA_INFO);
                                    }
                                });
                            }
                        }
                    }
                }
            }
        }
    }

    protected void onLikeSucceeded(User user, String articleUrl) {

    }

    protected abstract List<User> initUserList();
}
