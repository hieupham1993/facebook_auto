package jp.co.wssj.autosocial.robot.like;

import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.models.entities.Article;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;
import jp.co.wssj.autosocial.utils.Logger;

/**
 * Created by HieuPT on 11/14/2017.
 * <p>
 * Like bài post mới nhất của người đã like bài viết của mình
 */
public class LikeTask1 extends LikeFilteredListUserTask {

    private static final String TAG = LikeTask1.class.getSimpleName();

    public LikeTask1(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected Map<String, List<User>> initIntentUserList() {
        Map<String, List<User>> userMap = new HashMap<>();
        List<Article> articles = getSelfMostRecentArticles(App.getInstance().getLoggedFacebookUser());
        if (articles == null || articles.isEmpty()) {
            Logger.debug(TAG, "#initIntentUserList: Cannot get articles by search");
            articles = getSelfMostRecentArticlesViaActivityLog(App.getInstance().getLoggedFacebookUser());
        }
        if (articles != null && !articles.isEmpty()) {
            for (Article article : articles) {
                List<User> reactions = getArticleReactions(article.getUrl(), false);
                if (reactions != null && !reactions.isEmpty()) {
                    userMap.put(article.getArticleId(), reactions);
                }
            }
        }
        return userMap;
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return false;
    }

    @Override
    protected String getSearchTarget() {
        return "記事";
    }

    @Override
    protected String getTaskName() {
        return "自分の記事にいいねしている人にいいねする";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionLikeTask.TASK_1_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
