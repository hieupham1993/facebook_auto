package jp.co.wssj.autosocial.robot.like;

import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.models.entities.Article;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;
import jp.co.wssj.autosocial.utils.Logger;

/**
 * Created by Nguyen Huu Ta on 17/11/2017.
 * <p>
 * Like bài post mới nhất của những người đã like vào bài post của mình trong 1 group nào đó (URL bài viết)
 */

public class LikeTask10 extends LikeFilteredListUserTask {

    private static final String TAG = LikeTask10.class.getSimpleName();

    public LikeTask10(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected Map<String, List<User>> initIntentUserList() {
        Map<String, List<User>> userMap = new HashMap<>();
        List<Article> articles = getGroupMostRecentArticles(getTaskSetting().getTaskUrl(), App.getInstance().getLoggedFacebookUser());
        if (articles == null || articles.isEmpty()) {
            Logger.debug(TAG, "#initIntentUserList: Cannot get articles by search");
            articles = getSelfGroupArticlesViaActivityLog(getTaskSetting().getTaskUrl(), App.getInstance().getLoggedFacebookUser());
        }
        if (articles != null && !articles.isEmpty()) {
            for (Article article : articles) {
                if (article != null) {
                    List<User> reactions = getArticleReactions(article.getUrl(), false);
                    if (reactions != null && !reactions.isEmpty()) {
                        userMap.put(article.getArticleId(), reactions);
                    }
                }
            }
        }
        return userMap;
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return true;
    }

    @Override
    protected String getSearchTarget() {
        return "自分の記事にいいねしている人";
    }

    @Override
    protected String getTaskName() {
        return "指定したFacebookグループで、自分の記事にいいねしている人にいいねする";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionLikeTask.TASK_10_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
