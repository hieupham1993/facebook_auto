package jp.co.wssj.autosocial.robot.like;

import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.wssj.autosocial.models.entities.Article;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;

/**
 * Created by Nguyen Huu Ta on 17/11/2017.
 * <p>
 * Like bài post mới nhất của người đã like bài viết của một user nào đó (URL user)
 */

public class LikeTask2 extends LikeFilteredListUserTask {

    private static final String TAG = LikeTask2.class.getSimpleName();

    public LikeTask2(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected Map<String, List<User>> initIntentUserList() {
        Map<String, List<User>> userMap = new HashMap<>();
        List<Article> articles = getUserMostRecentArticles(getTaskSetting().getTaskUrl());
        if (articles != null && !articles.isEmpty()) {
            for (Article article : articles) {
                List<User> reactions = getArticleReactions(article.getUrl(), false);
                if (reactions != null && !reactions.isEmpty()) {
                    userMap.put(article.getArticleId(), reactions);
                }
            }
        }
        return userMap;
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return true;
    }

    @Override
    protected String getSearchTarget() {
        return "記事";
    }

    @Override
    protected String getTaskName() {
        return "指定したユーザーの記事にいいねしている人にいいねする";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionLikeTask.TASK_2_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
