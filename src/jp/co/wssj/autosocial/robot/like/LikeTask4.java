package jp.co.wssj.autosocial.robot.like;

import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

import jp.co.wssj.autosocial.models.entities.Article;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;

/**
 * Created by Nguyen Huu Ta on 17/11/2017.
 * <p>
 * Like bài post mới nhất của một fan page nào đó (URL bài viết fan page)
 */

public class LikeTask4 extends LikeArticlesTask {

    private static final String TAG = LikeTask4.class.getSimpleName();

    public LikeTask4(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected List<Article> initArticleList() {
        List<Article> articles = new ArrayList<>();
        articles.add(getLatestFanPageArticle(getTaskSetting().getTaskUrl()));
        return articles;
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return true;
    }

    @Override
    protected String getSearchTarget() {
        return "ページの最新記事";
    }

    @Override
    protected String getTaskName() {
        return "指定したFacebookページの最新記事にいいねする";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionLikeTask.TASK_4_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
