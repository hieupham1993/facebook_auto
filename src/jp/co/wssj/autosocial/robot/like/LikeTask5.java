package jp.co.wssj.autosocial.robot.like;

import org.openqa.selenium.WebDriver;

import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.IActionResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.LikeErrorResponse;

/**
 * Created by Nguyen Huu Ta on 17/11/2017.
 * <p>
 * Like chính fan page đấy (URL fan page)
 */

public class LikeTask5 extends BaseLikeTask {

    private static final String TAG = LikeTask5.class.getSimpleName();

    public LikeTask5(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected void doWork() {
        stopLogIntervalTask();
        Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_SEARCH_FINISH, getSearchTarget()));
        Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_START, getActionTypeText()));
        likeFanPage(getTaskSetting().getTaskUrl(), new IActionResponse<Void, LikeErrorResponse>() {

            @Override
            public void onSuccess(Void data) {
                increaseSuccessCount(getTaskSetting().getTaskUrl());
            }
        });
    }

    @Override
    protected boolean hasOption() {
        return false;
    }

    @Override
    protected boolean hasUrl() {
        return true;
    }

    @Override
    protected String getSearchTarget() {
        return "ページ";
    }

    @Override
    protected String getTaskName() {
        return "指定したFacebookページ自体にいいねする";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionLikeTask.TASK_5_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
