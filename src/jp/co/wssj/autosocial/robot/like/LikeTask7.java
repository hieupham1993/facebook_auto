package jp.co.wssj.autosocial.robot.like;

import org.openqa.selenium.WebDriver;

import java.util.List;

import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;

/**
 * Created by Nguyen Huu Ta on 17/11/2017.
 * <p>
 * Like bài post mới nhất của những người được facebook cho là Maybe you know
 */

public class LikeTask7 extends LikeLatestUserArticleTask {

    private static final String TAG = LikeTask7.class.getSimpleName();

    public LikeTask7(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected List<User> initUserList() {
        return getListPeopleYouMayKnow();
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return false;
    }

    @Override
    protected String getSearchTarget() {
        return "記事";
    }

    @Override
    protected String getTaskName() {
        return "「知り合いかも」で出てくる人の最新記事にいいねする";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionLikeTask.TASK_7_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
