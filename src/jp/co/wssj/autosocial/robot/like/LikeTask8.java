package jp.co.wssj.autosocial.robot.like;

import org.openqa.selenium.WebDriver;

import java.util.List;

import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;

/**
 * Created by Nguyen Huu Ta on 17/11/2017.
 * <p>
 * Like bài post mới nhất của những người tham gia vào 1 group nào đó (URL group)
 */

public class LikeTask8 extends LikeLatestUserArticleTask {

    private static final String TAG = LikeTask8.class.getSimpleName();

    public LikeTask8(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected List<User> initUserList() {
        return getGroupMembers(getTaskSetting().getTaskUrl());
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return true;
    }

    @Override
    protected String getSearchTarget() {
        return "Facebookグループに参加している人";
    }

    @Override
    protected String getTaskName() {
        return "指定したFacebookグループに参加している人にいいねする";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionLikeTask.TASK_8_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
