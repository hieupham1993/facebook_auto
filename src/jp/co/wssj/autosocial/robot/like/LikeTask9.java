package jp.co.wssj.autosocial.robot.like;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;

/**
 * Created by Nguyen Huu Ta on 17/11/2017.
 * <p>
 * Like bài post mới nhất của những người đã post bài vào 1 group nào đó (URL bài viết)
 */

public class LikeTask9 extends LikeLatestUserArticleTask {

    private static final String TAG = LikeTask9.class.getSimpleName();

    public LikeTask9(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected List<User> initUserList() {
        return getListUserLikeTask9(getTaskSetting().getTaskUrl());
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return true;
    }

    @Override
    protected String getSearchTarget() {
        return "Facebookグループに投稿している人";
    }

    @Override
    protected String getTaskName() {
        return "指定したFacebookグループに投稿している人にいいねする";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionLikeTask.TASK_9_ID;
    }

    private List<User> getListUserLikeTask9(String urlGroup) {
        openUrl(urlGroup);
        WebElement leftCol = getElementById("leftCol");
        WebElement link = getElementByCssSelector(leftCol, "h1#seo_h1_tag > a");
        clickElement(link);
        wait(5, TimeUnit.SECONDS);
        int count = 0;
        while (count < 3) {
            scrollToBottom();
            count++;
            wait(2, TimeUnit.SECONDS);
        }
        WebElement elementContainerPosts = getElementById("pagelet_group_mall");
        List<User> listUser = new ArrayList<>();
        if (elementContainerPosts != null) {
            WebElement elementContentPost = getElementByCssSelector(elementContainerPosts, "div._5pcb");
            List<WebElement> listElementItemPost = getElementsByCssSelector(elementContentPost, "div.clearfix > a._5pb8");
            List<String> listStringLinkProfile = new ArrayList<>();


            for (WebElement itemPost : listElementItemPost) {
                String linkProfile = itemPost.getAttribute("href");
                if (listStringLinkProfile.indexOf(linkProfile) == -1) {
                    listStringLinkProfile.add(linkProfile);
                    User user = new User(linkProfile);
                    listUser.add(user);
                }
            }
        }
        return listUser;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
