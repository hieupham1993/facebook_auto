package jp.co.wssj.autosocial.robot.message;

import org.openqa.selenium.WebDriver;

import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.robot.RobotTask;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;

/**
 * Created by HieuPT on 12/4/2017.
 */

public abstract class BaseMessageTask extends RobotTask {

    public BaseMessageTask(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected String getActionTypeText() {
        return RobotTaskFactory.ActionSendMessageTask.ACTION_TYPE_TEXT;
    }

    @Override
    public int getActionType() {
        return RobotTaskFactory.ACTION_TYPE_SEND_MESSAGE;
    }
}
