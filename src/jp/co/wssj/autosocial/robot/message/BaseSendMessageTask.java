package jp.co.wssj.autosocial.robot.message;

import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

import jp.co.wssj.autosocial.messenger.Message;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.TextUtils;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.IActionResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.MessagingErrorResponse;

/**
 * Created by HieuPT on 12/4/2017.
 */

public abstract class BaseSendMessageTask extends BaseMessageTask {

    public BaseSendMessageTask(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected void doWork() {
        if (hasMessage()) {
            List<User> listUser = initSendList();
            if (listUser != null && !listUser.isEmpty()) {
                stopLogIntervalTask();
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_SEARCH_FINISH, getSearchTarget()));
                Logger.append(getLogTag(), String.format(Constants.LogFormat.TASK_EXECUTE_START, getActionTypeText()));
                startSendMessage(listUser);
            }
        }
    }

    private void startSendMessage(List<User> listUser) {
        for (User user : listUser) {
            if (!isReachMaxAction()) {
                if (user != null) {
                    String message = TextUtils.replaceAll(Constants.MESSAGE_REPLACE_NAME_REGEX, getTaskSetting().getTaskMessage(), user.getName());
                    sendMessageTo(user.getUserId(), message, new IActionResponse<Void, MessagingErrorResponse>() {

                        @Override
                        public void onSuccess(Void data) {
                            onSendMessageSuccess(user);
                            increaseSuccessCount(user.getProfileUrl());
                            prepareForNextAction();
                        }

                        @Override
                        public void onError(MessagingErrorResponse errorResponse) {
                            switch (errorResponse.getCode()) {
                                case MessagingErrorResponse.CODE_SECURITY_CHECK:
                                    String message = String.format(Constants.LogFormat.FB_SECURITY_WARN, getActionTypeText());
                                    sendMessage(Message.obtain(Constants.Msg.MSG_FB_SECURITY_WARN, message));
                                    break;
                                default:
//                                    Logger.append(getLogTag(), String.format(Constants.LogFormat.ACTION_SEND_MESSAGE_FAILURE, user.getProfileUrl()));
//                                    Logger.append(getLogTag(), Constants.LogFormat.ACTION_SEND_MESSAGE_FAILURE_EXTRA_INFO);
                                    break;
                            }
                        }
                    });
                }
            } else {
                break;
            }
        }
    }

    private void prepareForNextAction() {
        if (!isReachMaxAction()) {
            int waitTime = getRandomInterval();
            if (waitTime != 0) {
                BaseSendMessageTask.this.wait(waitTime, TimeUnit.SECONDS);
            }
        }
    }

    protected abstract List<User> initSendList();

    protected abstract void onSendMessageSuccess(User user);
}
