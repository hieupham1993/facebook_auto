package jp.co.wssj.autosocial.robot.message;

import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import jp.co.wssj.autosocial.api.requests.WssjListFacebookIdRequest;
import jp.co.wssj.autosocial.api.requests.WssjSaveUserFacebook;
import jp.co.wssj.autosocial.api.responses.WssjGetUserFacebook;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.models.entities.Preference;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.RequestQueue;
import jp.co.wssj.autosocial.utils.TextUtils;

/**
 * Created by HieuPT on 12/4/2017.
 */

public abstract class FilteredSendListMessageTask extends BaseSendMessageTask {

    public FilteredSendListMessageTask(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected List<User> initSendList() {
        List<User> listUser = initListSendIntent();
        if (listUser != null && !listUser.isEmpty()) {
            Logger.debug(getLogTag(), "Found " + listUser.size());
            Preference preference = DatabaseHelper.getInstance().getPreference();
            if (!TextUtils.isEmpty(preference.getAccessToken())) {
                WssjGetUserFacebook wssjUserFacebook = RequestQueue.getInstance().executeRequest(getWssjServices().getListUserIdFacebook(preference.getAccessToken(), getWssjListFacebookIdRequest(listUser)), new RequestQueue.SendLogErrorToServer<>());
                if (wssjUserFacebook != null) {
                    WssjGetUserFacebook.Data wssjUserFacebookData = wssjUserFacebook.getData();
                    if (wssjUserFacebookData != null) {
                        List<String> facebookIdList = wssjUserFacebookData.getFacebookIdList();
                        if (facebookIdList != null && !facebookIdList.isEmpty()) {
                            return listUser.stream().filter(user -> user != null && facebookIdList.contains(user.getUserId())).collect(Collectors.toList());
                        } else {
                            return null;
                        }
                    }
                }
            }
        }
        return listUser;
    }

    private WssjListFacebookIdRequest getWssjListFacebookIdRequest(List<User> userList) {
        WssjListFacebookIdRequest requestBody = new WssjListFacebookIdRequest();
        requestBody.setTaskType(getTaskType());
        if (userList != null && !userList.isEmpty()) {
            List<String> facebookIdList = new ArrayList<>();
            for (User user : userList) {
                if (user != null) {
                    facebookIdList.add(user.getUserId());
                }
            }
            requestBody.addAllFacebookId(facebookIdList);
        }
        return requestBody;
    }

    @Override
    protected void onSendMessageSuccess(User user) {
        saveUserIdFacebook(user.getUserId());
    }

    private void saveUserIdFacebook(String userId) {
        Preference preference = DatabaseHelper.getInstance().getPreference();
        RequestQueue.getInstance().executeRequest(getWssjServices().saveUserIdFacebook(preference.getAccessToken(), new WssjSaveUserFacebook(getTaskType(), userId)), new RequestQueue.SendLogErrorToServer<>());
    }

    protected abstract List<User> initListSendIntent();
}
