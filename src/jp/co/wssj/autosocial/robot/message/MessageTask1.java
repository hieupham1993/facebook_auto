package jp.co.wssj.autosocial.robot.message;

import org.openqa.selenium.WebDriver;

import java.util.List;

import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;

/**
 * Created by Nguyen Huu Ta on 18/11/2017.
 */

public class MessageTask1 extends FilteredSendListMessageTask {

    private static final String TAG = MessageTask1.class.getSimpleName();

    public MessageTask1(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return false;
    }

    @Override
    protected String getSearchTarget() {
        return "友達申請してくれた人";
    }

    @Override
    protected List<User> initListSendIntent() {
        return getListFriendRequest();
    }

    @Override
    protected String getTaskName() {
        return "友達申請してくれた人にメッセージ送信";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionSendMessageTask.TASK_1_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
