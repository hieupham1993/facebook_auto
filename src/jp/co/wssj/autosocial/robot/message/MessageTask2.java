package jp.co.wssj.autosocial.robot.message;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;
import jp.co.wssj.autosocial.utils.Constants;

/**
 * Created by Nguyen Huu Ta on 24/11/2017.
 */

public class MessageTask2 extends FilteredSendListMessageTask {

    private static final String TAG = MessageTask2.class.getSimpleName();

    public MessageTask2(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return false;
    }

    @Override
    protected String getSearchTarget() {
        return "友達承認してくれた人";
    }

    @Override
    protected List<User> initListSendIntent() {
        return getListUserSendFriendOfMessageTask2();
    }

    @Override
    protected String getTaskName() {
        return "友達承認してくれた人にメッセージ送信";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionSendMessageTask.TASK_2_ID;
    }

    private List<User> getListUserSendFriendOfMessageTask2() {
        List<User> listUser = new ArrayList<>();
        List<String> listUserId = new ArrayList<>();
        String userIdLogin = "/" + App.getInstance().getLoggedFacebookUser().getUserId();
        String urlNext = FB_BASE_URL + userIdLogin + "/allactivity?privacy_source=activity_log&log_filter=cluster_8";
        openUrl(urlNext);
        int count = 0;
        while (count < 3) {
            scrollToBottom();
            count++;
            wait(2, TimeUnit.SECONDS);
        }
        List<WebElement> list = getElementsByCssSelector("div[class='_42ef']");
        if (list != null && list.size() > 0) {
            list.remove(0);
            for (WebElement web : list) {
                List<WebElement> tag = getElementsByCssSelector(web, "a[class='profileLink']");
                if (tag != null && tag.size() > 1) {
                    WebElement tagA = tag.get(1);
                    String userId = tagA.getAttribute("data-hovercard");
                    String userName = tagA.getText();
                    userId = userId.replace("/ajax/hovercard/user.php?id=", Constants.EMPTY_STRING);
                    if (!listUserId.contains(userId)) {
                        listUserId.add(userId);
                        User user = new User(userId, User.TYPE_ID);
                        user.setName(userName);
                        listUser.add(user);
                    }
                }
            }
        }
        return listUser;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
