package jp.co.wssj.autosocial.robot.message;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;
import jp.co.wssj.autosocial.utils.TextUtils;

/**
 * Created by Nguyen Huu Ta on 27/11/2017.
 */

public class MessageTask3 extends FilteredSendListMessageTask {

    private static final String TAG = MessageTask3.class.getSimpleName();

    public MessageTask3(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return false;
    }

    @Override
    protected String getSearchTarget() {
        return "メッセージ送信してくれた人";
    }

    @Override
    protected List<User> initListSendIntent() {
        List<User> listConversation = getListConversation();
        List<User> intentList = new ArrayList<>();
        for (User user : listConversation) {
            openUrl(user.getMessengerUrl());
            waitDefault();
            WebElement globalContainer = getElementById("globalContainer");
            if (globalContainer != null) {
                List<WebElement> messageElements = getElementsByCssSelector(globalContainer, "h5._ih3._-ne");
                if (messageElements != null && !messageElements.isEmpty()) {
                    WebElement lastMessageElement = messageElements.get(messageElements.size() - 1);
                    if (lastMessageElement != null) {
                        String classAttr = lastMessageElement.getAttribute("class");
                        if (!TextUtils.isEmpty(classAttr) && !classAttr.contains("accessible_elem")) {
                            intentList.add(user);
                        }
                    }
                }
            }
        }
        return intentList;
    }

    @Override
    protected String getTaskName() {
        return "メッセージ送信してくれた人にメッセージ送信";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionSendMessageTask.TASK_3_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
