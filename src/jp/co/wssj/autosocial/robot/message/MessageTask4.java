package jp.co.wssj.autosocial.robot.message;

import org.openqa.selenium.WebDriver;

import java.util.List;

import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;

/**
 * Created by HieuPT on 11/24/2017.
 */
public class MessageTask4 extends FilteredSendListMessageTask {

    private static final String TAG = MessageTask4.class.getSimpleName();

    public MessageTask4(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return false;
    }

    @Override
    protected String getSearchTarget() {
        return "フォローしてくれた人";
    }

    @Override
    protected List<User> initListSendIntent() {
        User loggedFbUser = App.getInstance().getLoggedFacebookUser();
        return getFollowerList(loggedFbUser.getProfileUrl());
    }

    @Override
    protected String getTaskName() {
        return "フォローしてくれた人にメッセージ送信";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionSendMessageTask.TASK_4_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
