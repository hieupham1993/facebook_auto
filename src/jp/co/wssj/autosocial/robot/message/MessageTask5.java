package jp.co.wssj.autosocial.robot.message;

import org.openqa.selenium.WebDriver;

import java.util.List;

import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;

/**
 * Created by HieuPT on 11/24/2017.
 */

public class MessageTask5 extends FilteredSendListMessageTask {

    private static final String TAG = MessageTask5.class.getSimpleName();

    public MessageTask5(WebDriver webDriver, TaskSetting taskSetting) {
        super(webDriver, taskSetting);
    }

    @Override
    protected boolean hasOption() {
        return true;
    }

    @Override
    protected boolean hasUrl() {
        return true;
    }

    @Override
    protected String getSearchTarget() {
        return "グループに参加している人";
    }

    @Override
    protected List<User> initListSendIntent() {
        return getGroupMembers(getTaskSetting().getTaskUrl());
    }

    @Override
    protected String getTaskName() {
        return "指定したFacebookグループに参加している人にメッセージ送信";
    }

    @Override
    public int getTaskType() {
        return RobotTaskFactory.ActionSendMessageTask.TASK_5_ID;
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }
}
