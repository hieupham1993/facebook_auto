package jp.co.wssj.autosocial.tasks;

import java.util.concurrent.TimeUnit;

import jp.co.wssj.autosocial.utils.Logger;

/**
 * Created by HieuPT on 11/14/2017.
 */
abstract class BaseTask implements Runnable, IDestroyable {

    protected final void sleep(int time, TimeUnit timeUnit) {
        try {
            Thread.sleep(timeUnit.toMillis(time));
        } catch (InterruptedException e) {
            Logger.catching(e);
        }
    }
}
