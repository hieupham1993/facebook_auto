package jp.co.wssj.autosocial.tasks;

import jp.co.wssj.autosocial.messenger.Message;
import jp.co.wssj.autosocial.messenger.Messenger;

/**
 * Created by HieuPT on 11/29/2017.
 */
public abstract class BroadcastTask extends HandledExceptionTask {

    private Messenger messenger;

    public final void setMessenger(Messenger messenger) {
        this.messenger = messenger;
    }

    public final Messenger getMessenger() {
        return messenger;
    }

    protected final void sendMessage(Message message) {
        if (messenger != null) {
            messenger.sendMessage(message);
        }
    }
}
