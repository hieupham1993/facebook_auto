package jp.co.wssj.autosocial.tasks;

import org.joda.time.DateTime;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import java.util.Objects;
import java.util.Set;

import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.api.IWssjServices;
import jp.co.wssj.autosocial.api.responses.WssjLoginResponse;
import jp.co.wssj.autosocial.api.responses.WssjTaskResponse;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.exceptions.WebDriverNotFoundException;
import jp.co.wssj.autosocial.models.entities.TaskSetting;
import jp.co.wssj.autosocial.robot.RobotTask;
import jp.co.wssj.autosocial.robot.RobotTaskFactory;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.RequestQueue;
import jp.co.wssj.autosocial.utils.WebDriverUtils;
import jp.co.wssj.autosocial.webcontroller.facebook.FacebookWebController;

/**
 * Created by HieuPT on 11/11/2017.
 */
public class BrowserTask extends BroadcastTask {

    private static final String TAG = BrowserTask.class.getSimpleName();

    private static final int TASK_DONE_WAITING_TIME_MIN = 10;

    private WebDriver webDriver;

    private int lastTaskId = -1;

    private long startWorkingTime;

    private boolean isDestroyed;

    private boolean isWorkRestarted;

    private final String token;

    private final IWssjServices services;

    public BrowserTask() {
        services = IWssjServices.SERVICES;
        token = DatabaseHelper.getInstance().getPreference().getAccessToken();
        Objects.requireNonNull(token, "Access token must not be null");
    }

    private void initWebDriver() throws WebDriverNotFoundException {
        webDriver = WebDriverUtils.createWebDriver();
        webDriver.get(FacebookWebController.FB_BASE_URL);
        Set<Cookie> cookies = App.getInstance().getFacebookCookies();
        if (cookies != null && !cookies.isEmpty()) {
            for (Cookie cookie : cookies) {
                if (cookie != null) {
                    webDriver.manage().addCookie(cookie);
                }
            }
        }
        webDriver.get(FacebookWebController.FB_BASE_URL);
    }

    @Override
    protected void doWork() throws Exception {
        if (!isDestroyed && System.currentTimeMillis() > startWorkingTime) {
            if (webDriver == null) {
                initWebDriver();
                WssjLoginResponse.FacebookAccountInfo facebookAccount = App.getInstance().getFacebookAccount();
                if (facebookAccount != null) {
                    Logger.append(TAG, String.format(Constants.LogFormat.TASK_START, facebookAccount.getEmail()));
                }
            }
            if (isWorkRestarted) {
                Logger.append(TAG, Constants.LogFormat.ROBOT_RESTARTED);
                isWorkRestarted = false;
            }
            WssjTaskResponse taskResponse;
            if (lastTaskId > 0) {
                taskResponse = RequestQueue.getInstance().executeRequest(services.getTask(token, lastTaskId), new RequestQueue.SendLogErrorToServer<>());
            } else {
                taskResponse = RequestQueue.getInstance().executeRequest(services.getTask(token), new RequestQueue.SendLogErrorToServer<>());
            }
            if (taskResponse != null) {
                if (taskResponse.isSuccess()) {
                    WssjTaskResponse.Data taskData = taskResponse.getData();
                    if (taskData != null) {
                        lastTaskId = taskData.getTaskId();
                        TaskSetting taskSetting = createTaskSetting(taskData);
                        RobotTask task = RobotTaskFactory.getTask(taskData.getTypeAction(), taskData.getTypeTask(), webDriver, taskSetting);
                        if (task != null) {
                            task.setMessenger(getMessenger());
                            task.perform();
                        }
                    } else {
                        lastTaskId = -1;
                        Logger.append(TAG, Constants.LogFormat.ALL_TASK_DONE);
                        isWorkRestarted = true;
                        startWorkingTime = DateTime.now().plusMinutes(TASK_DONE_WAITING_TIME_MIN).getMillis();
                    }
                }
            }
        }
    }

    private TaskSetting createTaskSetting(WssjTaskResponse.Data taskData) {
        TaskSetting.Option taskOption = new TaskSetting.Option(taskData.getTimeActionMin(), taskData.getTimeActionMax(), taskData.getMaxAction());
        return new TaskSetting(taskData.getTaskId(), taskData.getRank(), taskData.getUrl(), taskOption, taskData.getMessage(), taskData.getNumberHasRun(), taskData.getMaxDate());
    }

    @Override
    public void destroy() {
        Logger.debug(TAG, "#destroy");
        isDestroyed = true;
        if (webDriver != null) {
            try {
                webDriver.quit();
            } catch (WebDriverException e) {
                Logger.warning(getClass().getSimpleName(), "WebDriverException: " + e.getMessage());
                Logger.catching(e);
            } finally {
                webDriver = null;
            }
        }
    }
}
