package jp.co.wssj.autosocial.tasks;

import org.apache.commons.lang3.exception.ExceptionUtils;

import jp.co.wssj.autosocial.annotations.DoNotReport;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.Utils;

public abstract class HandledExceptionTask extends BaseTask {

    @Override
    public final void run() {
        try {
            doWork();
        } catch (Exception e) {
            String logMessage = getClass().getSimpleName() + " - " + e.getClass().getSimpleName() + ": " + e.getMessage();
            Logger.warning(logMessage);
            Logger.catching(e);
            if (!e.getClass().isAnnotationPresent(DoNotReport.class)) {
                Utils.sendLogToServer(ExceptionUtils.getStackTrace(e));
            }
            onExceptionOccur(e);
        }
    }

    protected void onExceptionOccur(Exception e) {

    }

    protected abstract void doWork() throws Exception;
}
