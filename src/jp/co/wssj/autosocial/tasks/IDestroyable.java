package jp.co.wssj.autosocial.tasks;

/**
 * Created by HieuPT on 11/13/2017.
 */
public interface IDestroyable {

    default void destroy() {
    }
}
