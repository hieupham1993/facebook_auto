package jp.co.wssj.autosocial.tasks;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import jp.co.wssj.autosocial.App;
import jp.co.wssj.autosocial.api.IWssjServices;
import jp.co.wssj.autosocial.api.requests.WssjLoginRequest;
import jp.co.wssj.autosocial.api.responses.WssjLoginResponse;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.exceptions.WebDriverNotFoundException;
import jp.co.wssj.autosocial.messenger.Message;
import jp.co.wssj.autosocial.models.entities.Preference;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.RequestQueue;
import jp.co.wssj.autosocial.utils.WebDriverUtils;
import jp.co.wssj.autosocial.webcontroller.facebook.FacebookWebController;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.IActionResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.LoginErrorResponse;

/**
 * Created by HieuPT on 11/14/2017.
 */
public class LoginFacebookTask extends BroadcastTask {

    private static final String TAG = LoginFacebookTask.class.getSimpleName();

    private FacebookWebController facebookController;

    private WebDriver webDriver;

    private final String account;

    private final String password;

    private final IWssjServices services;

    private final boolean saveToServer;

    public LoginFacebookTask(String account, String password, boolean saveToServer) {
        this.saveToServer = saveToServer;
        this.account = account;
        this.password = password;
        services = IWssjServices.SERVICES;
    }

    private void initWebDriver() throws WebDriverNotFoundException {
        webDriver = WebDriverUtils.createWebDriver();
        webDriver.get(FacebookWebController.FB_BASE_URL);
    }

    @Override
    protected void doWork() throws Exception {
        initWebDriver();
        facebookController = new FacebookWebController(webDriver);
        facebookController.login(account, password, new IActionResponse<Void, LoginErrorResponse>() {

            @Override
            public void onSuccess(Void data) {
                User user = facebookController.getMyUserInfo();
                updateFbAccount();
                sendMessage(Message.obtain(Constants.Msg.MSG_LOGIN_SUCCESS, user));
                if (saveToServer) {
                    saveFacebookAccount(account, password);
                }
            }

            private void updateFbAccount() {
                WssjLoginResponse.FacebookAccountInfo facebookAccount = App.getInstance().getFacebookAccount();
                if (facebookAccount != null) {
                    if (!account.equals(facebookAccount.getEmail()) || !password.equals(facebookAccount.getPassword())) {
                        App.getInstance().setFacebookAccount(new WssjLoginResponse.FacebookAccountInfo(account, password));
                    }
                } else {
                    App.getInstance().setFacebookAccount(new WssjLoginResponse.FacebookAccountInfo(account, password));
                }
                App.getInstance().setFacebookCookies(webDriver.manage().getCookies());
            }

            @Override
            public void onError(LoginErrorResponse errorResponse) {
                switch (errorResponse.getCode()) {
                    case LoginErrorResponse.CODE_USER_NAME_OR_PASSWORD_INVALID:
                        Logger.append(TAG, Constants.LogFormat.FB_LOGIN_INCORRECT_INFO);
                        break;
                    case LoginErrorResponse.CODE_SECURE_CHECK_POINT:
                        Logger.append(TAG, Constants.LogFormat.FB_ACCOUNT_SECURE_CHECK);
                        break;
                }
                sendMessage(Message.obtain(Constants.Msg.MSG_LOGIN_FAILURE));
            }
        });
        destroy();
    }

    @Override
    protected void onExceptionOccur(Exception e) {
        Logger.debug("LoginFacebookTask#onExceptionOccur");
        sendMessage(Message.obtain(Constants.Msg.MSG_LOGIN_FAILURE));
        destroy();
    }

    private void saveFacebookAccount(String account, String password) {
        Preference preference = DatabaseHelper.getInstance().getPreference();
        RequestQueue.getInstance().executeRequest(services.saveFacebookAccount(preference.getAccessToken(), new WssjLoginRequest(account, password)), new RequestQueue.SendLogErrorToServer<>());
    }

    @Override
    public void destroy() {
        Logger.debug(TAG, "#destroy");
        if (webDriver != null) {
            try {
                webDriver.quit();
            } catch (WebDriverException e) {
                Logger.warning(TAG, "WebDriverException: " + e.getMessage());
                Logger.catching(e);
            } finally {
                webDriver = null;
            }
        }
    }
}
