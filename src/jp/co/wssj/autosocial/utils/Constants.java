package jp.co.wssj.autosocial.utils;

import jp.co.wssj.autosocial.BuildConfig;

/**
 * Created by HieuPT on 11/10/2017.
 */
public final class Constants {

    public static final String BASE_WSSJ_URL = BuildConfig.DEBUG ? BuildConfig.SERVER_DEV : BuildConfig.SERVER_RELEASE;

    public static final String EMPTY_STRING = "";

    public static final String SPACE_CHARACTER = " ";

    public static final String INTERNET_ERROR_MESSAGE = "サーバーへ接続できません。インターネット環境をチェックしてみてください。";

    public static final String LOG4J_CONFIG_FILE_NAME = "log4j2.xml";

    public static final String CHROME_OPTION_DISABLE_NOTIFICATIONS = "--disable-notifications";

    public static final String CHROME_OPTION_IN_PRIVATE_MODE = "--incognito";

    public static final String WEB_ELEMENT_INNER_HTML_ATTR = "innerHTML";

    public static final String WEB_ELEMENT_VALUE_ATTR = "value";

    public static final String MESSAGE_REPLACE_NAME_REGEX = "\\[NAME\\]";

    public static final String VERSION_TEXT = "Version %s";

    public static final String NEW_APP_VERSION_AVAILABLE = "お使いのツールは最新ではありません。\n" +
            "必ず、最新版をダウンロードしてください。";

    public static final String DIALOG_TITLE = "警告";

    public static final int WEB_DRIVER_WINDOW_WIDTH = 1920;

    public static final int WEB_DRIVER_WINDOW_HEIGHT = 1080;

    private Constants() {
        //no instance
    }

    public static class LogFormat {

        public static final String FB_START_LOGIN = "メールアドレス「%s」のアカウントでログインします。";

        public static final String FB_FINISH_LOGIN = "ログインしました。";

        public static final String ROBOT_START = "タスクを実行します。";

        public static final String ROBOT_STOP = "停止しました。";

        public static final String ROBOT_RESTARTED = "タスク実行を再開します。";

        public static final String TASK_START = "メールアドレス「%s」のタスクを実行します。";

        public static final String TASK_EXECUTE = "タスク「%s,【%s】%s」を実行します。";

        public static final String TASK_EXECUTE_WITH_URL = "タスク「%s,【%s】%s」を実行します。(%s)";

        public static final String TASK_EXECUTE_WITH_MESSAGE = "タスク「%s,【%s】%s」を実行します。(※友達申請時にメッセージを送信する, メッセージ内容:%s)";

        public static final String TASK_EXECUTE_WITH_URL_AND_MESSAGE = "タスク「%s,【%s】%s」を実行します。(%s, ※友達申請時にメッセージを送信する, メッセージ内容:%s)";

        public static final String TASK_EXECUTE_WITH_OPTION = "タスク「%s,【%s】%s,間隔%s〜%s秒、上限%s回」を実行します。";

        public static final String TASK_EXECUTE_WITH_OPTION_AND_URL = "タスク「%s,【%s】%s(%s),間隔%s〜%s秒、上限%s回」を実行します。";

        public static final String TASK_EXECUTE_WITH_OPTION_AND_MESSAGE = "タスク「%s,【%s】%s(※友達申請時にメッセージを送信する, メッセージ内容:%s),間隔%s〜%s秒、上限%s回」を実行します。";

        public static final String TASK_EXECUTE_WITH_FULL_SETTING = "タスク「%s,【%s】%s(%s, ※友達申請時にメッセージを送信する, メッセージ内容:%s),間隔%s〜%s秒、上限%s回」を実行します。";

        public static final String TASK_EXECUTE_START = "%sを開始します。";

        public static final String TASK_SEARCH_START = "%sを検索します。";

        public static final String TASK_SEARCHING = "検索しています。";

        public static final String TASK_SEARCH_FINISH = "%sの検索が完了しました。";

        public static final String ACTION_LOG = "%s回目の%sが完了しました。（%s）";

        public static final String NO_ACTION_LOG = "%1$s 対象を見つかりませんでした。新しいタスクを設定して%1$sを獲得しましょう";

        public static final String ACTION_SEND_MESSAGE_FAILURE = "メッセージ送信失敗しました：%s";

        public static final String ACTION_SEND_MESSAGE_FAILURE_EXTRA_INFO = "（18際以下やメッセージ受信制限のユーザーなどにメッセージが送れない場合があります）";

        public static final String ACTION_SEND_FRIEND_REQUEST_FAILURE = "友達申請失敗しました：%s";

        public static final String ACTION_SEND_FRIEND_REQUEST_FAILURE_EXTRA_INFO = "（このユーザーは5000人の友達がいるか、友達申請を制限している場合があります）";

        public static final String ACTION_FOLLOW_FAILURE = "フォロー失敗しました：%s";

        public static final String ACTION_FOLLOW_FAILURE_EXTRA_INFO = "（その人に既にフォローされたか、他人からフォローするのが制限されている場合があります。）";

        public static final String ACTION_LIKE_FAILURE = "いいね失敗しました：%s";

        public static final String ACTION_LIKE_FAILURE_EXTRA_INFO = "（記事に既にいいねされたか、その記事にいいねするのを制限されている場合があります）";

        public static final String ALL_TASK_DONE = "最後のタスクまで実行が完了しました。10分後に最初のアカウントから再実行します。※今すぐ再実行したい場合は「停止」ボタンを押してから「開始」ボタンを押してください。";

        public static final String FB_SECURITY_WARN = "Facebookがスパム行動を見つかった可能性があるため、一旦停止しています。\n時間を経過してから、再度起動してください。\n（%1$s間隔を上げたり、一日の%1$s上限数を下げたりするのも検討してください。）";

        public static final String FB_ACCOUNT_SECURE_CHECK = "ログイン失敗した。\n原因：Facebookからアカウント認証の要求があります。 ブラウザーを開き、アカウントの認証を行ってから、再度ログインしなおしてください。";

        public static final String FB_LOGIN_INCORRECT_INFO = "Facebookにログインできませんでした。アカウント名またはパスワードを確認してください。";

        private LogFormat() {
            //no instance
        }
    }

    public static class Facebook {

        public static final String FB_HOST = "www.facebook.com";

        public static final String FB_SHORT_HOST = "www.fb.com";

        public static final String FB_SECURE_HOST = "secure.facebook.com";

        public static final String FB_BADGE_HOST = "badge.facebook.com";

        public static final String FB_PIXEL_HOST = "pixel.facebook.com";

        public static final String FB_BETA_HOST = "www.beta.facebook.com";

        public static final String FB_PRODUCT_HOST = "www.prod.facebook.com";

        public static final String FB_MBASIC_HOST = "mbasic.facebook.com";

        public static final String FB_M_HOST = "m.facebook.com";

        public static final String FB_H_HOST = "h.facebook.com";

        public static final String FB_TOUCH_HOST = "touch.facebook.com";

        public static final String FB_IPHONE_HOST = "iphone.facebook.com";

        public static final String FB_D_HOST = "d.facebook.com";

        private Facebook() {
            //no instance
        }
    }

    public static class Msg {

        public static final int MSG_LOGIN_SUCCESS = 1;

        public static final int MSG_LOGIN_FAILURE = 2;

        public static final int MSG_FB_SECURITY_WARN = 3;

        private Msg() {
            //no instance
        }
    }
}
