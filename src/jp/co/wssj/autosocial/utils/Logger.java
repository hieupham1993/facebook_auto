package jp.co.wssj.autosocial.utils;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;

import javafx.application.Platform;
import javafx.scene.web.WebEngine;
import jp.co.wssj.autosocial.BuildConfig;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.models.entities.Preference;

public class Logger {

    private static final org.apache.logging.log4j.Logger log4j = org.apache.logging.log4j.LogManager.getLogger(Logger.class);

    private static final String BASE_LOG_MESSAGE = "[" + BuildConfig.PLATFORM + "][" + BuildConfig.VERSION_NAME + "]";

    private static Preference preference;

    private static Logger logger;

    public static void append(String message) {
        append(null, message);
    }

    public static void append(String tag, String message) {
        logger.print(message);
        log4j.info(getLogMessage(tag, message));
    }

    public static void debug(String message) {
        debug(null, message);
    }

    public static void debug(String tag, String message) {
        log4j.debug(getLogMessage(tag, message));
    }

    public static void warning(String message) {
        warning(null, message);
    }

    public static void warning(String tag, String message) {
        log4j.warn(getLogMessage(tag, message));
    }

    public static void error(String message) {
        error(null, message);
    }

    public static void error(String tag, String message) {
        log4j.error(getLogMessage(tag, message));
    }

    public static void catching(Throwable throwable) {
        log4j.catching(throwable);
    }

    public static synchronized void setup(WebEngine context) {
        if (logger == null) {
            logger = new Logger();
        }
        preference = DatabaseHelper.getInstance().getPreference();
        logger.setWebEngine(context);
    }

    private static String getLogMessage(String tag, String message) {
        StringBuilder messageBuilder = new StringBuilder(BASE_LOG_MESSAGE);
        if (!TextUtils.isEmpty(tag)) {
            messageBuilder.append("[").append(tag).append("]");
        }
        if (preference != null && !TextUtils.isEmpty(preference.getEmail())) {
            messageBuilder.append("[").append(preference.getEmail()).append("]");
        }
        messageBuilder.append(Constants.SPACE_CHARACTER).append(message);
        return messageBuilder.toString();
    }

    private WebEngine webEngine;

    private Logger() {
        //no instance
    }

    private void setWebEngine(WebEngine webEngine) {
        this.webEngine = webEngine;
    }

    private String getCurrentTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return formatter.format(DateTime.now().toDate());
    }

    private void print(String message) {
        synchronized (this) {
            if (webEngine != null) {
                Platform.runLater(() -> {
                    String logMessage = "[" + getCurrentTime() + "]: " + message;
                    String formattedMessage = logMessage.replaceAll("\\r\\n|\\r|\\n", Constants.SPACE_CHARACTER);
                    String script = "appendLog('" + formattedMessage + "');";
                    webEngine.executeScript(script);
                });
            }
        }
    }


}
