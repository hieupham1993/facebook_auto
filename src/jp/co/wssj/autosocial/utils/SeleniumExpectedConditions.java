package jp.co.wssj.autosocial.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import java.util.List;

public class SeleniumExpectedConditions {

    public static final int TEXT_TYPE_TEXT = 1;

    public static final int TEXT_TYPE_INNER_HTML = 2;

    public static final int TEXT_TYPE_VALUE = 3;

    public static ExpectedCondition<Boolean> textTobePresent(final WebElement element, int textType) {
        return webDriver -> {
            try {
                String text = null;
                if (element != null) {
                    switch (textType) {
                        case TEXT_TYPE_TEXT:
                            text = element.getText();
                            break;
                        case TEXT_TYPE_INNER_HTML:
                            text = element.getAttribute(Constants.WEB_ELEMENT_INNER_HTML_ATTR);
                            break;
                        case TEXT_TYPE_VALUE:
                            text = element.getAttribute(Constants.WEB_ELEMENT_VALUE_ATTR);
                            break;
                        default:
                            break;
                    }
                }
                return !TextUtils.isEmpty(text);
            } catch (StaleElementReferenceException e) {
                Logger.catching(e);
                return null;
            }
        };
    }

    public static ExpectedCondition<Boolean> textToBePresentInElement(WebElement element, String text) {
        return new ExpectedCondition<Boolean>() {

            public Boolean apply(WebDriver driver) {
                try {
                    String elementText = element.getText();
                    if (elementText != null) {
                        elementText = elementText.toLowerCase();
                        return elementText.equals(text);
                    }
                } catch (StaleElementReferenceException e) {
                    Logger.catching(e);
                }
                return null;
            }

            public String toString() {
                return String.format("text ('%s') to be present in element %s", text, element);
            }
        };
    }

    public static ExpectedCondition<Boolean> absenceOfElementLocated(By locator) {
        return new ExpectedCondition<Boolean>() {

            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    driver.findElement(locator);
                    return false;
                } catch (NoSuchElementException | StaleElementReferenceException e) {
                    Logger.catching(e);
                    return true;
                }
            }

            @Override
            public String toString() {
                return "Element to not being present: " + locator;
            }
        };
    }

    public static ExpectedCondition<Boolean> absenceOfElementLocated(WebElement parentElement, By locator) {
        return new ExpectedCondition<Boolean>() {

            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    parentElement.findElement(locator);
                    return false;
                } catch (Exception e) {
                    Logger.catching(e);
                    return true;
                }
            }

            @Override
            public String toString() {
                return "Element to not being present: " + locator;
            }
        };
    }

    public static ExpectedCondition<Boolean> presenceOfElementLocated(WebElement parentElement, By locator) {
        return new ExpectedCondition<Boolean>() {

            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    WebElement element = parentElement.findElement(locator);
                    return element != null;
                } catch (Exception e) {
                    Logger.catching(e);
                    return true;
                }
            }

            @Override
            public String toString() {
                return "Element to not being present: " + locator;
            }
        };
    }

    public static ExpectedCondition<WebElement> presenceAndDisplayOfElement(By locator) {
        return webDriver -> {
            WebElement element = null;
            if (webDriver != null) {
                List<WebElement> elements = webDriver.findElements(locator);
                if (elements != null) {
                    element = elements.stream().filter(WebDriverUtils::isElementPresentAndDisplayed).findFirst().orElse(null);
                }
            }
            return element;
        };
    }

    public static ExpectedCondition<List<WebElement>> numberOfElementsToBeMoreThan(WebElement parentElement, By locator, Integer number) {
        return new ExpectedCondition<List<WebElement>>() {

            private Integer currentNumber = 0;

            public List<WebElement> apply(WebDriver webDriver) {
                List<WebElement> elements = parentElement.findElements(locator);
                this.currentNumber = elements.size();
                return this.currentNumber > number ? elements : null;
            }

            public String toString() {
                return String.format("number to be more than \"%s\". Current number: \"%s\"", number, this.currentNumber);
            }
        };
    }
}
