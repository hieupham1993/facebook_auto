package jp.co.wssj.autosocial.utils;

import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TextUtils {

    private static final String TAG = TextUtils.class.getSimpleName();

    public static boolean isEmpty(String text) {
        return text == null || text.isEmpty();
    }

    public static long convertTimeTextToMillis(String text, String pattern) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(pattern);
            Date date = formatter.parse(text);
            DateTime dateTime = new DateTime(date);
            int hoursInSec = dateTime.getHourOfDay() * 60 * 60;
            int minutesInSec = dateTime.getMinuteOfHour() * 60;
            int sec = dateTime.getSecondOfMinute();
            return (hoursInSec + minutesInSec + sec) * 1000;
        } catch (ParseException e) {
            Logger.warning(TAG, "Cannot parse text to date: " + e.getMessage());
            Logger.catching(e);
            return 0;
        }
    }

    public static String replaceAll(String regex, String text, String replacement) {
        if (replacement != null && !TextUtils.isEmpty(text) && regex != null) {
            return text.replaceAll(regex, replacement);
        }
        return text;
    }

    public static boolean equal(String text1, String text2) {
        if (!isEmpty(text1) && !isEmpty(text2)) {
            return text1.equals(text2);
        }
        return false;
    }

    private TextUtils() {
        //no instance
    }
}
