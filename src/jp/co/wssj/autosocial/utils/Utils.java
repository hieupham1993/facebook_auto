package jp.co.wssj.autosocial.utils;

import org.apache.commons.io.FileUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import jp.co.wssj.autosocial.api.IWssjServices;
import jp.co.wssj.autosocial.api.requests.WssjWriteLogRequest;
import jp.co.wssj.autosocial.database.DatabaseHelper;
import jp.co.wssj.autosocial.models.entities.Preference;
import jp.co.wssj.autosocial.webcontroller.facebook.FacebookWebController;

/**
 * Created by HieuPT on 11/10/2017.
 */
public final class Utils {

    private static final String TAG = Utils.class.getSimpleName();

    public static void captureScreenshot(WebDriver webDriver) {
        try {
            File scrFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("screenshot", String.valueOf(System.currentTimeMillis()) + ".png"));
        } catch (IOException e) {
            Logger.catching(e);
        }
    }

    public static int getRandomNumber(int min, int max) {
        if (max > min) {
            Random random = new Random();
            return random.nextInt(max - min + 1) + min;
        } else {
            throw new IllegalArgumentException("Max must be greater than min");
        }
    }

    public static void stopService(ExecutorService service) {
        Logger.debug(TAG, "#stopService");
        if (service != null) {
            try {
                service.shutdown();
                service.awaitTermination(5, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                Logger.warning(TAG, "InterruptedException: " + e.getMessage());
                Logger.catching(e);
            } finally {
                service.shutdownNow();
            }
        }
    }

    public static String convertToFbWebUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            URI uri = URI.create(url);
            if (Constants.Facebook.FB_HOST.equals(uri.getHost())) {
                if (uri.getPath().contains("permalink.php")) {
                    Map<String, String> queryParams = getQueryMap(uri);
                    if (queryParams != null && !queryParams.isEmpty()) {
                        if (queryParams.containsKey("id") && queryParams.containsKey("story_fbid")) {
                            return FacebookWebController.FB_BASE_URL + "/" + queryParams.get("id") + "/posts/" + queryParams.get("story_fbid");
                        }
                    }
                }
                return uri.toASCIIString();
            } else if (Constants.Facebook.FB_M_HOST.equals(uri.getHost())
                    || Constants.Facebook.FB_H_HOST.equals(uri.getHost())
                    || Constants.Facebook.FB_TOUCH_HOST.equals(uri.getHost())
                    || Constants.Facebook.FB_IPHONE_HOST.equals(uri.getHost())
                    || Constants.Facebook.FB_D_HOST.equals(uri.getHost())
                    || Constants.Facebook.FB_MBASIC_HOST.equals(uri.getHost())) {
                if (uri.getPath().contains("story.php")
                        || uri.getPath().contains("photo.php")
                        || uri.getPath().contains("permalink.php")) {
                    Map<String, String> queryParams = getQueryMap(uri);
                    if (queryParams != null && !queryParams.isEmpty()) {
                        if (queryParams.containsKey("id") && queryParams.containsKey("story_fbid")) {
                            return FacebookWebController.FB_BASE_URL + "/" + queryParams.get("id") + "/posts/" + queryParams.get("story_fbid");
                        }
                    }
                } else {
                    return rebuildFbUrl(uri);
                }
            } else if (Constants.Facebook.FB_SECURE_HOST.equals(uri.getHost())
                    || Constants.Facebook.FB_BETA_HOST.equals(uri.getHost())
                    || Constants.Facebook.FB_BADGE_HOST.equals(uri.getHost())
                    || Constants.Facebook.FB_PRODUCT_HOST.equals(uri.getHost())
                    || Constants.Facebook.FB_PIXEL_HOST.equals(uri.getHost())) {
                return rebuildFbUrl(uri);
            } else if (Constants.Facebook.FB_SHORT_HOST.equals(uri.getHost())
                    || uri.getHost().contains("facebook")) {
                return rebuildFbUrl(uri);
            }
        }
        return url;
    }

    private static String rebuildFbUrl(URI uri) {
        if (uri != null) {
            StringBuilder urlBuilder = new StringBuilder(FacebookWebController.FB_BASE_URL);
            if (!TextUtils.isEmpty(uri.getPath())) {
                urlBuilder.append(uri.getPath());
                if (!TextUtils.isEmpty(uri.getQuery())) {
                    urlBuilder.append("?").append(uri.getQuery());
                }
            }
            return urlBuilder.toString();
        }
        return Constants.EMPTY_STRING;
    }

    private static Map<String, String> getQueryMap(URI uri) {
        Map<String, String> queryParams = new HashMap<>();
        List<NameValuePair> nameValuePairs = URLEncodedUtils.parse(uri, Charset.forName("UTF-8"));
        for (NameValuePair pair : nameValuePairs) {
            queryParams.put(pair.getName(), pair.getValue());
        }
        return queryParams;
    }

    public static void sendLogToServer(String message) {
        if (!TextUtils.isEmpty(message)) {
            Preference preference = DatabaseHelper.getInstance().getPreference();
            if (!TextUtils.isEmpty(preference.getAccessToken())) {
                RequestQueue.getInstance().addRequest(IWssjServices.SERVICES.writeLog(preference.getAccessToken(), new WssjWriteLogRequest(message)), null);
            }
        }
    }

    public static void openBrowserSystem(String url) {
        try {
            Desktop.getDesktop().browse(URI.create(url));
        } catch (IOException e) {
            Logger.warning(TAG, "IOException: " + e.getMessage());
            Logger.catching(e);
        }
    }

    public static String simpleLineEndText(String text) {
        return text.replaceAll("\\r\\n|\\n\\r|\\r|\\n", "\n");
    }

    public static boolean isTimeExpired(long time) {
        return time < System.currentTimeMillis();
    }

    public static void grantFilePermission(File file) {
        if (file != null && file.exists()) {
            try {
                Set<PosixFilePermission> perms = new HashSet<>();
                perms.add(PosixFilePermission.OWNER_READ);
                perms.add(PosixFilePermission.OWNER_WRITE);
                perms.add(PosixFilePermission.OWNER_EXECUTE);
                perms.add(PosixFilePermission.GROUP_READ);
                perms.add(PosixFilePermission.GROUP_WRITE);
                perms.add(PosixFilePermission.GROUP_EXECUTE);
                perms.add(PosixFilePermission.OTHERS_READ);
                perms.add(PosixFilePermission.OTHERS_WRITE);
                perms.add(PosixFilePermission.OTHERS_EXECUTE);
                Files.setPosixFilePermissions(file.toPath(), perms);
            } catch (IOException e) {
                Logger.warning(TAG, "#grantFilePermission: " + e.getMessage());
            }
        }
    }

    public static int compareVersion(String version1, String version2) {
        String[] arr1 = version1.split("\\.");
        String[] arr2 = version2.split("\\.");

        int i = 0;
        while (i < arr1.length || i < arr2.length) {
            if (i < arr1.length && i < arr2.length) {
                if (Integer.parseInt(arr1[i]) < Integer.parseInt(arr2[i])) {
                    return -1;
                } else if (Integer.parseInt(arr1[i]) > Integer.parseInt(arr2[i])) {
                    return 1;
                }
            } else if (i < arr1.length) {
                if (Integer.parseInt(arr1[i]) != 0) {
                    return 1;
                }
            } else if (i < arr2.length) {
                if (Integer.parseInt(arr2[i]) != 0) {
                    return -1;
                }
            }

            i++;
        }
        return 0;
    }

    private Utils() {
        //no instance
    }
}
