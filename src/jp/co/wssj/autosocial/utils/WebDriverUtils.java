package jp.co.wssj.autosocial.utils;

import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;

import jp.co.wssj.autosocial.exceptions.WebDriverNotFoundException;

/**
 * Created by HieuPT on 2/2/2018.
 */
public class WebDriverUtils {

    private static final String TAG = WebDriverUtils.class.getSimpleName();

    public static WebDriver createWebDriver() throws WebDriverNotFoundException {
        WebDriver webDriver;
        try {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments(Constants.CHROME_OPTION_DISABLE_NOTIFICATIONS);
            chromeOptions.addArguments(Constants.CHROME_OPTION_IN_PRIVATE_MODE);
            String baseLocation = System.getProperty("user.dir") + "/webdriver/chrome";
            if (SystemUtils.IS_OS_WINDOWS) {
                chromeOptions.setBinary(new File(baseLocation + "/windows/App/Chrome-bin", "chrome.exe"));
            } else if (SystemUtils.IS_OS_MAC) {
                chromeOptions.setBinary(new File(baseLocation + "/mac/Google Chrome.app/Contents/MacOS", "Google Chrome"));
            }
            chromeOptions.setHeadless(true);
            webDriver = new ChromeDriver(chromeOptions);
        } catch (WebDriverException e) {
            Logger.catching(e);
            throw new WebDriverNotFoundException("Chrome is not found.", e);
        }
        webDriver.manage().window().setSize(new Dimension(Constants.WEB_DRIVER_WINDOW_WIDTH, Constants.WEB_DRIVER_WINDOW_HEIGHT));
        return webDriver;
    }

    public static boolean isElementPresentAndDisplayed(WebElement element) {
        try {
            if (element != null) {
                return element.isDisplayed();
            }
        } catch (WebDriverException ignored) {
        }
        return false;
    }

    private WebDriverUtils() {
        //no instance
    }
}
