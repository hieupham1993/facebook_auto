package jp.co.wssj.autosocial.webcontroller;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.TextUtils;
import jp.co.wssj.autosocial.utils.Utils;
import jp.co.wssj.autosocial.utils.WebDriverUtils;

/**
 * Created by HieuPT on 11/11/2017.
 */
public abstract class BaseWebController {

    private static final String TAG = BaseWebController.class.getSimpleName();

    private static final int WEB_DRIVER_WAIT_TIMEOUT = 10;

    private static final int DEFAULT_WAIT_TIME = 500;

    private final WebDriver webDriver;

    private final WebDriverWait webDriverWait;

    public BaseWebController(WebDriver webDriver) {
        this.webDriver = webDriver;
        webDriverWait = new WebDriverWait(webDriver, WEB_DRIVER_WAIT_TIMEOUT);
    }

    protected WebDriver getWebDriver() {
        return webDriver;
    }

    protected final JavascriptExecutor getJavascriptExecutor() {
        if (webDriver instanceof JavascriptExecutor) {
            return (JavascriptExecutor) webDriver;
        } else {
            throw new IllegalArgumentException("This web driver does not support javascript");
        }
    }

    protected final void waitDefault() {
        wait(DEFAULT_WAIT_TIME, TimeUnit.MILLISECONDS);
    }

    protected final void wait(long time, TimeUnit timeUnit) {
        if (time > 0) {
            try {
                Thread.sleep(timeUnit.toMillis(time));
            } catch (InterruptedException e) {
                Logger.warning(TAG, "InterruptedException: " + e.getMessage());
            }
        }
    }

    protected final void scrollToElement(WebElement element) {
        try {
            getJavascriptExecutor().executeScript("arguments[0].scrollIntoView();", element);
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
        }
    }

    protected final void scrollToTop() {
        try {
            getJavascriptExecutor().executeScript("window.scrollTo(0, 0);");
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
        }
    }

    protected final void scrollToBottom() {
        try {
            getJavascriptExecutor().executeScript("window.scrollTo(0, document.body.scrollHeight);");
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
        }
    }

    protected final void simulateSendKeys(WebElement element, String text) {
        if (element != null && !TextUtils.isEmpty(text)) {
            element.click();
            Actions actions = new Actions(webDriver);
            for (char c : text.toCharArray()) {
                actions.sendKeys(Character.toString(c));
                actions.pause(Utils.getRandomNumber(50, 300));
            }
            actions.build().perform();
        }
    }

    protected final void clickElement(WebElement element) {
        if (element != null) {
            waitDefault();
            try {
                if (isElementPresentAndDisplayed(element)) {
                    element.click();
                }
            } catch (WebDriverException e) {
                Logger.warning(TAG, "WebDriverException: " + e.getMessage());
            }
        }
    }

    protected final void moveToElement(WebElement element) {
        if (element != null) {
            waitDefault();
            try {
                Actions actions = new Actions(webDriver);
                actions.moveToElement(element);
                actions.build().perform();
            } catch (WebDriverException e) {
                Logger.warning(TAG, "WebDriverException: " + e.getMessage());
            }
        }
    }

    protected final boolean isElementPresentAndDisplayed(WebElement element) {
        return WebDriverUtils.isElementPresentAndDisplayed(element);
    }

    protected final void openUrl(String url) {
        webDriver.get(url);
    }

    protected final Object executeJavaScript(String script, Object... objects) {
        return getJavascriptExecutor().executeScript(script, objects);
    }

    protected final <E extends WebElement> E getElementByCondition(ExpectedCondition<E> expectedCondition) {
        try {
            return webDriverWait.until(expectedCondition);
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final <T> T waitUntil(ExpectedCondition<T> expectedCondition) {
        try {
            return webDriverWait.until(expectedCondition);
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final WebElement getElementByCssSelector(WebElement parentElement, String selector) {
        try {
            return parentElement.findElement(By.cssSelector(selector));
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final List<WebElement> getElementsByCssSelector(WebElement parentElement, String selector) {
        try {
            return parentElement.findElements(By.cssSelector(selector));
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final List<WebElement> getElementsByCssSelector(String selector) {
        try {
            return webDriver.findElements(By.cssSelector(selector));
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final WebElement getElementByCssSelector(String selector) {
        try {
            return webDriver.findElement(By.cssSelector(selector));
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final WebElement getElementByClassName(String className) {
        try {
            return webDriver.findElement(By.className(className));
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final WebElement getElementByClassName(WebElement parentElement, String className) {
        try {
            return parentElement.findElement(By.className(className));
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final List<WebElement> getElementsByClassName(String className) {
        try {
            return webDriver.findElements(By.className(className));
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final WebElement getElementById(String id) {
        try {
            return webDriver.findElement(By.id(id));
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final WebElement getElementById(WebElement parentElement, String id) {
        try {
            return parentElement.findElement(By.id(id));
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final List<WebElement> getElementsById(String id) {
        try {
            return webDriver.findElements(By.id(id));
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final List<WebElement> getElementsById(WebElement parentElement, String id) {
        try {
            return parentElement.findElements(By.id(id));
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final WebElement getElementByName(String name) {
        try {
            return webDriver.findElement(By.name(name));
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final WebElement getElementByTagName(String name) {
        try {
            return webDriver.findElement(By.tagName(name));
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final WebElement getElementByTagName(WebElement parentElement, String name) {
        try {
            return parentElement.findElement(By.tagName(name));
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
    }

    protected final boolean isElementExist(String cssSelector) {
        try {
            List<WebElement> elements = webDriver.findElements(By.cssSelector(cssSelector));
            return elements != null && !elements.isEmpty();
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return false;
        }
    }

    protected final boolean isElementExist(WebElement parentElement, String cssSelector) {
        try {
            List<WebElement> elements = parentElement.findElements(By.cssSelector(cssSelector));
            return elements != null && !elements.isEmpty();
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return false;
        }
    }

    protected final boolean isElementExist(By by) {
        try {
            List<WebElement> elements = webDriver.findElements(by);
            return elements != null && !elements.isEmpty();
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return false;
        }
    }

    protected final boolean isElementExist(WebElement parentElement, By by) {
        try {
            List<WebElement> elements = parentElement.findElements(by);
            return elements != null && !elements.isEmpty();
        } catch (Exception e) {
            Logger.warning(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
            return false;
        }
    }
}
