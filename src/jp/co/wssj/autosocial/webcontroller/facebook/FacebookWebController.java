package jp.co.wssj.autosocial.webcontroller.facebook;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import jp.co.wssj.autosocial.models.entities.Article;
import jp.co.wssj.autosocial.models.entities.FanPage;
import jp.co.wssj.autosocial.models.entities.Group;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.utils.Constants;
import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.SeleniumExpectedConditions;
import jp.co.wssj.autosocial.utils.TextUtils;
import jp.co.wssj.autosocial.utils.Utils;
import jp.co.wssj.autosocial.webcontroller.BaseWebController;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.AcceptFriendResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.AddFriendErrorResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.FollowingErrorResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.IActionResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.LikeErrorResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.LoginErrorResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.MessagingErrorResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.UnFollowErrorResponse;
import okhttp3.HttpUrl;

/**
 * Created by HieuPT on 11/11/2017.
 */
public class FacebookWebController extends BaseWebController {

    public static final String FB_BASE_URL = "https://www.facebook.com";

    public static final String FB_PROFILE_URL = FB_BASE_URL + "/profile.php";

    public static final String FB_MESSAGES_BASE_URL = FB_BASE_URL + "/messages/t/";

    public static final String FB_FRIEND_REQUEST_URL = FB_BASE_URL + "/friends/requests";

    public static final String FB_PEOPLE_YOU_MAY_KNOW_URL = FB_BASE_URL + "/find-friends/browser";

    public static final String FB_SEARCH_POST_BASE_URL = FB_BASE_URL + "/search/posts/";

    public static final String FB_ACTIVITY_LOG_POSTS_URL = FB_BASE_URL + "/%s/allactivity?privacy_source=activity_log&log_filter=cluster_11";

    public static final String FB_ACTIVITY_LOG_GROUP_POSTS_URL = FB_BASE_URL + "/%s/allactivity?privacy_source=activity_log&log_filter=groupposts";

    public static final String FB_SETTING_URL = FB_BASE_URL + "/settings";

    public static final int FB_LOAD_MORE_MAX_PAGE = 4;

    private static final String TAG = FacebookWebController.class.getSimpleName();

    public FacebookWebController(WebDriver webDriver) {
        super(webDriver);
    }

    private boolean isLoggedIn() {
        WebElement emailElem = getElementById("email");
        return emailElem == null;
    }

    public final void login(String account, String password, IActionResponse<Void, LoginErrorResponse> response) {
        Logger.debug(TAG, "#login: " + account + " - " + password);
        if (!TextUtils.isEmpty(account) && !TextUtils.isEmpty(password)) {
            if (!isLoggedIn()) {
                doLogin(account, password, response);
            } else {
                logout();
                doLogin(account, password, response);
            }
        } else {
            IActionResponse.onError(response, new LoginErrorResponse(LoginErrorResponse.CODE_USER_NAME_OR_PASSWORD_EMPTY));
        }
    }

    private void doLogin(String userId, String password, IActionResponse<Void, LoginErrorResponse> response) {
        openUrl(FB_BASE_URL);
        WebElement emailElem = getElementById("email");
        WebElement passElem = getElementById("pass");
        if (emailElem != null && passElem != null) {
            wait(1, TimeUnit.SECONDS);
            simulateSendKeys(emailElem, userId);
            wait(Utils.getRandomNumber(300, 600), TimeUnit.MILLISECONDS);
            new Actions(getWebDriver()).sendKeys(Keys.TAB).build().perform();
            simulateSendKeys(passElem, password);
            wait(Utils.getRandomNumber(300, 600), TimeUnit.MILLISECONDS);
            new Actions(getWebDriver()).sendKeys(Keys.ENTER).build().perform();
            checkLogin(userId, password, response);
            return;
        }
        IActionResponse.onErrorSyncServer(response, new LoginErrorResponse(LoginErrorResponse.CODE_LOAD_PAGE_ERROR, String.format("UserId: %s - Password: %s", userId, password)));
    }

    private void checkLogin(String userId, String password, IActionResponse<Void, LoginErrorResponse> response) {
        waitDefault();
        String currentUrl = getWebDriver().getCurrentUrl();
        if (!TextUtils.isEmpty(currentUrl) && currentUrl.contains("login.php")) {
            IActionResponse.onErrorSyncServer(response, new LoginErrorResponse(LoginErrorResponse.CODE_USER_NAME_OR_PASSWORD_INVALID, String.format("UserId: %s - Password: %s", userId, password)));
        } else {
            if (isElementExist("form.checkpoint")) {
                IActionResponse.onErrorSyncServer(response, new LoginErrorResponse(LoginErrorResponse.CODE_SECURE_CHECK_POINT, String.format("UserId: %s - Password: %s", userId, password)));
            } else {
                IActionResponse.onSuccess(response);
            }
        }
    }

    public final void logout() {
        Logger.debug(TAG, "#loggout");
        if (isLoggedIn()) {
            getWebDriver().manage().deleteAllCookies();
        }
    }

    public final String getProfileUrl(String userName) {
        Logger.debug(TAG, "#getProfileUrl");
        if (!TextUtils.isEmpty(userName)) {
            return FB_BASE_URL + "/" + userName;
        }
        return Constants.EMPTY_STRING;
    }

    public final String getSelfProfileUrl() {
        Logger.debug(TAG, "#getSelfProfileUrl");
        return FB_PROFILE_URL;
    }

    public final void openProfilePage(String userName) {
        Logger.debug(TAG, "#openProfilePage");
        if (!TextUtils.isEmpty(userName)) {
            openUrl(getProfileUrl(userName));
        }
    }

    public final void openSelfProfilePage() {
        openUrl(getSelfProfileUrl());
    }

    public final Article getSelfLatestArticle(User user, boolean initReactions) {
        Logger.debug(TAG, "#getSelfLatestArticle");
        Article article = null;
        List<Article> articleList = getSelfMostRecentArticles(user, 1);
        if (articleList != null && !articleList.isEmpty()) {
            article = articleList.get(0);
            if (article != null && initReactions) {
                List<User> reactionList = getArticleReactions(article.getUrl());
                article.clearReactions();
                article.addReactions(reactionList);
            }
        }
        return article;
    }

    public final List<Article> getSelfMostRecentArticles(User user) {
        return getSelfMostRecentArticles(user, 20);
    }

    public final List<Article> getSelfMostRecentArticles(User user, int limit) {
        Logger.debug(TAG, "#getSelfMostRecentArticles: limit=" + limit);
        return getMostRecentArticles(getSearchPostByMeUrl(user), limit, false);
    }

    private String getSearchPostByMeUrl(User user) {
        Logger.debug(TAG, "#getSearchPostByMeUrl");
        if (user != null) {
            try {
                HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(FB_SEARCH_POST_BASE_URL)).newBuilder();
                urlBuilder.addEncodedQueryParameter("q", user.getName());
                urlBuilder.addEncodedQueryParameter("filters_rp_author", URLEncoder.encode("{\"name\":\"author_me\",\"args\":\"\"}", "UTF-8"));
                urlBuilder.addEncodedQueryParameter("filters_rp_chrono_sort", URLEncoder.encode("{\"name\":\"chronosort\",\"args\":\"\"}", "UTF-8"));
                return urlBuilder.build().uri().toString();
            } catch (UnsupportedEncodingException e) {
                Logger.warning(TAG, "UnsupportedEncodingException: " + e.getMessage());
                Logger.catching(e);
            }
        }
        return null;
    }

    public final Article getUserLatestArticle(String userProfileUrl) {
        return getUserLatestArticle(userProfileUrl, false);
    }

    public final Article getUserLatestArticle(String userProfileUrl, boolean initReactions) {
        Logger.debug(TAG, "#getUserLatestArticle: " + userProfileUrl);
        Article article = null;
        List<Article> articleList = getUserMostRecentArticles(userProfileUrl, 1);
        if (articleList != null && !articleList.isEmpty()) {
            article = articleList.get(0);
            if (article != null && initReactions) {
                List<User> reactionList = getArticleReactions(article.getUrl());
                article.clearReactions();
                article.addReactions(reactionList);
            }
        }
        return article;
    }

    public final Article getLatestFanPageArticle(String pageUrl) {
        return getLatestFanPageArticle(pageUrl, false);
    }

    public final Article getLatestFanPageArticle(String pageUrl, boolean initReactions) {
        Logger.debug(TAG, "#getLatestFanPageArticle: " + pageUrl);
        Article article = null;
        List<Article> articleList = getFanPageMostRecentArticles(pageUrl, 1);
        if (articleList != null && !articleList.isEmpty()) {
            article = articleList.get(0);
            if (article != null && initReactions) {
                List<User> reactionList = getArticleReactions(article.getUrl());
                article.clearReactions();
                article.addReactions(reactionList);
            }
        }
        return article;
    }

    public List<Article> getFanPageMostRecentArticles(String pageUrl) {
        return getFanPageMostRecentArticles(pageUrl, 20);
    }

    public List<Article> getFanPageMostRecentArticles(String pageUrl, int limit) {
        Logger.debug(TAG, "#getFanPageMostRecentArticles: " + pageUrl);
        String convertedUrl = Utils.convertToFbWebUrl(pageUrl);
        if (!TextUtils.isEmpty(convertedUrl)) {
            FanPage fanPage = getFanPageInfo(pageUrl);
            if (fanPage != null
                    && !TextUtils.isEmpty(fanPage.getName())
                    && !TextUtils.isEmpty(fanPage.getUniqueId())) {
                return getMostRecentArticles(getSearchPostByAuthorUrl(fanPage.getName(), fanPage.getUniqueId()), limit, false);
            }
        }
        return null;
    }

    public final FanPage getFanPageInfo(String pageUrl) {
        Logger.debug(TAG, "#getFanPageInfo: " + pageUrl);
        String convertedUrl = Utils.convertToFbWebUrl(pageUrl);
        if (!TextUtils.isEmpty(convertedUrl)) {
            FanPage fanPage = FanPage.fromUrl(convertedUrl);
            if (fanPage != null) {
                openUrl(fanPage.getPageUrl());
                WebElement coverNameElement = getElementByCssSelector("a._64-f");
                if (coverNameElement != null) {
                    String coverName = coverNameElement.getText();
                    if (!TextUtils.isEmpty(convertedUrl)) {
                        fanPage.setName(coverName);
                    }
                }
                WebElement uniqueIdMetadata = getElementByCssSelector("meta[property='al:android:url']");
                if (uniqueIdMetadata != null) {
                    String metadataContent = uniqueIdMetadata.getAttribute("content");
                    if (!TextUtils.isEmpty(metadataContent)) {
                        try {
                            URI uri = URI.create(metadataContent);
                            String host = uri.getHost();
                            if (!TextUtils.isEmpty(host) && host.contains("page")) {
                                String uniqueId = uri.getPath();
                                if (!TextUtils.isEmpty(uniqueId) && uniqueId.startsWith("/")) {
                                    uniqueId = uniqueId.substring(1);
                                }
                                fanPage.setUniqueId(uniqueId);
                            }
                        } catch (IllegalArgumentException e) {
                            Logger.warning(TAG, "IllegalArgumentException: " + e.getMessage());
                            Logger.catching(e);
                        }
                    }
                }
                return fanPage;
            }
        }
        return null;
    }

    public List<Article> getGroupMostRecentArticles(String groupUrl) {
        return getGroupMostRecentArticles(groupUrl, 20);
    }

    public List<Article> getGroupMostRecentArticles(String groupUrl, int limit) {
        return getGroupMostRecentArticles(groupUrl, limit, null);
    }

    public List<Article> getGroupMostRecentArticles(String groupUrl, User user) {
        return getGroupMostRecentArticles(groupUrl, 20, user);
    }

    public List<Article> getGroupMostRecentArticles(String groupUrl, int limit, User user) {
        Logger.debug(TAG, "#getGroupMostRecentArticles: " + groupUrl);
        String convertedUrl = Utils.convertToFbWebUrl(groupUrl);
        if (!TextUtils.isEmpty(convertedUrl)) {
            Group group = getGroupInfo(groupUrl);
            if (group != null
                    && !TextUtils.isEmpty(group.getName())
                    && !TextUtils.isEmpty(group.getUniqueId())) {
                String searchPostUrl;
                if (user != null) {
                    searchPostUrl = getSearchPostByMeToGroupUrl(user, group.getUniqueId());
                } else {
                    searchPostUrl = getSearchPostToGroupUrl(group.getName(), group.getUniqueId());
                }
                return getMostRecentArticles(searchPostUrl, limit, true);
            }
        }
        return null;
    }

    public List<Article> getGroupMostRecentArticlesViaGroupTimeline(String groupUrl) {
        return getGroupMostRecentArticlesViaGroupTimeline(groupUrl, 20);
    }

    public List<Article> getGroupMostRecentArticlesViaGroupTimeline(String groupUrl, int limit) {
        Logger.debug(TAG, "#getGroupMostRecentArticlesViaGroupTimeline: groupUrl = " + groupUrl + " - limit = " + limit);
        String convertedUrl = Utils.convertToFbWebUrl(groupUrl);
        if (!TextUtils.isEmpty(convertedUrl) && limit > 0) {
            Group group = Group.fromUrl(convertedUrl);
            if (group != null) {
                openUrl(group.getGroupDiscussionUrl());
                try {
                    WebElement pageletGroupMailElement = waitUntil(ExpectedConditions.presenceOfElementLocated(By.id("pagelet_group_mall")));
                    if (pageletGroupMailElement != null) {
                        loadMoreGroupTimeline(pageletGroupMailElement, 1, 8);
                        List<WebElement> articleElements = getElementsByCssSelector(pageletGroupMailElement, "div.userContentWrapper");
                        if (articleElements != null && !articleElements.isEmpty()) {
                            List<String> postLinkList = new ArrayList<>();
                            for (WebElement articleElement : articleElements) {
                                if (postLinkList.size() < limit && articleElement != null) {
                                    WebElement timestampElement = getElementByCssSelector(articleElement, "span.fsm.fwn.fcg > a._5pcq");
                                    if (timestampElement != null) {
                                        String href = timestampElement.getAttribute("href");
                                        if (!TextUtils.isEmpty(href)) {
                                            postLinkList.add(href);
                                        }
                                    }
                                } else if (postLinkList.size() >= limit) {
                                    break;
                                }
                            }
                            if (!postLinkList.isEmpty()) {
                                return postLinkList.stream().filter(href -> !TextUtils.isEmpty(href)).map(Article::from).filter(Objects::nonNull).collect(Collectors.toList());
                            } else {
                                Logger.debug(TAG, "#getGroupMostRecentArticlesViaGroupTimeline: cannot get any article link");
                            }
                        } else {
                            Logger.debug(TAG, "#getGroupMostRecentArticlesViaGroupTimeline: cannot find any articles");
                        }
                    } else {
                        Logger.debug(TAG, "#getGroupMostRecentArticlesViaGroupTimeline: initial_browse_result empty");
                    }
                } catch (NullPointerException e) {
                    Logger.warning(TAG, "NullPointerException: " + e.getMessage());
                    Logger.catching(e);
                }
            } else {
                Logger.debug(TAG, "#getGroupMostRecentArticlesViaGroupTimeline: " + groupUrl + " is not group url");
            }
        }
        return null;
    }

    private void loadMoreGroupTimeline(WebElement parentElement, int currentPage, int maxPage) {
        if (currentPage <= maxPage) {
            waitDefault();
            List<WebElement> loadMoreElements = getElementsByCssSelector(parentElement, "#pagelet_group_pager");
            WebElement loadMoreElement = null;
            if (loadMoreElements != null && !loadMoreElements.isEmpty()) {
                loadMoreElement = loadMoreElements.get(0);
            }
            if (loadMoreElement != null) {
                List<WebElement> itemElements = getElementsByCssSelector(parentElement, "div.userContentWrapper");
                if (itemElements != null) {
                    scrollToElement(loadMoreElement);
                    wait(2, TimeUnit.SECONDS);
                    Boolean result = waitUntil(ExpectedConditions.or(SeleniumExpectedConditions.numberOfElementsToBeMoreThan(parentElement, By.cssSelector("div.userContentWrapper"), itemElements.size())));
                    if (result != null && result) {
                        loadMoreGroupTimeline(parentElement, currentPage + 1, maxPage);
                    }
                }
            }
        }
    }

    public final Group getGroupInfo(String groupUrl) {
        Logger.debug(TAG, "#getGroupInfo: " + groupUrl);
        String convertedUrl = Utils.convertToFbWebUrl(groupUrl);
        if (!TextUtils.isEmpty(convertedUrl)) {
            Group group = Group.fromUrl(convertedUrl);
            if (group != null) {
                openUrl(group.getGroupUrl());
                WebElement coverNameElement = getElementById("seo_h1_tag");
                if (coverNameElement != null) {
                    String coverName = coverNameElement.getText();
                    if (!TextUtils.isEmpty(convertedUrl)) {
                        group.setName(coverName);
                    }
                }
                WebElement uniqueIdMetadata = getElementByCssSelector("meta[property='al:android:url']");
                if (uniqueIdMetadata != null) {
                    String metadataContent = uniqueIdMetadata.getAttribute("content");
                    if (!TextUtils.isEmpty(metadataContent)) {
                        try {
                            URI uri = URI.create(metadataContent);
                            String host = uri.getHost();
                            if (!TextUtils.isEmpty(host) && host.contains("group")) {
                                String uniqueId = uri.getPath();
                                if (!TextUtils.isEmpty(uniqueId) && uniqueId.startsWith("/")) {
                                    uniqueId = uniqueId.substring(1);
                                }
                                group.setUniqueId(uniqueId);
                            }
                        } catch (IllegalArgumentException e) {
                            Logger.warning(TAG, "IllegalArgumentException: " + e.getMessage());
                            Logger.catching(e);
                        }
                    }
                }
                return group;
            }
        }
        return null;
    }

    private void closeImageDialogIfPresence() {
        wait(2, TimeUnit.SECONDS);
        List<WebElement> photosSnowLiftElements = getElementsById("photos_snowlift");
        WebElement photosSnowLiftElement = null;
        if (photosSnowLiftElements != null && !photosSnowLiftElements.isEmpty()) {
            photosSnowLiftElement = photosSnowLiftElements.get(0);
        }
        if (photosSnowLiftElement != null) {
            waitUntil(ExpectedConditions.attributeToBe(photosSnowLiftElement, "aria-busy", "false"));
            WebElement closeDialogButton = getElementByCssSelector(photosSnowLiftElement, "a[role='button']");
            if (closeDialogButton != null) {
                clickElement(closeDialogButton);
            }
        }
    }

    private WebElement openArticleReactions(String articleUrl) {
        String convertedUrl = Utils.convertToFbWebUrl(articleUrl);
        if (!TextUtils.isEmpty(convertedUrl)) {
            openUrl(convertedUrl);
            closeImageDialogIfPresence();
            WebElement reactionLink = waitUntil(SeleniumExpectedConditions.presenceAndDisplayOfElement(By.cssSelector("a._2x4v")));
            if (reactionLink != null) {
                clickElement(reactionLink);
                WebElement dialogElement = waitUntil(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div._4t2a > div > div > div._4-i2._50f4")));
                if (dialogElement != null) {
                    WebElement tabListElement = getElementByCssSelector(dialogElement, "ul[role='tablist']");
                    if (tabListElement != null) {
                        String defaultActiveTabKey = tabListElement.getAttribute("defaultactivetabkey");
                        if (!TextUtils.isEmpty(defaultActiveTabKey)) {
                            String reactionListElementId = "reaction_profile_browser";
                            String reactionLoadMoreElementId = "reaction_profile_pager";
                            if (!"all".equals(defaultActiveTabKey)) {
                                reactionListElementId += defaultActiveTabKey;
                                reactionLoadMoreElementId += defaultActiveTabKey;
                            }
                            WebElement reactionListElement = getElementById(reactionListElementId);
                            if (reactionListElement != null) {
                                waitDefault();
                                loadMoreReactions(reactionLoadMoreElementId, 1, FB_LOAD_MORE_MAX_PAGE);
                                return reactionListElement;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    public final List<User> getArticleReactions(String articleUrl) {
        return getArticleReactions(articleUrl, true);
    }

    public final List<User> getArticleReactions(String articleUrl, boolean parseExtraInfo) {
        Logger.debug(TAG, "#getArticleReactions: " + articleUrl);
        String convertedUrl = Utils.convertToFbWebUrl(articleUrl);
        List<User> reactionList = new ArrayList<>();
        if (!TextUtils.isEmpty(convertedUrl)) {
            WebElement reactionListElement = openArticleReactions(convertedUrl);
            if (reactionListElement != null) {
                List<WebElement> userItemElements = getElementsByCssSelector(reactionListElement, "div.clearfix");
                if (userItemElements != null && !userItemElements.isEmpty()) {
                    userItemElements.parallelStream().forEach(itemElement -> {
                        if (itemElement != null && !isElementExist(itemElement, "div.fsm.fwn.fcg")) {
                            WebElement userLink = getElementByCssSelector(itemElement, "a.lfloat._ohe");
                            if (userLink != null) {
                                String href = userLink.getAttribute("href");
                                User user = new User(href);
                                if (parseExtraInfo) {
                                    user.setFollowable(true);
                                    if (isElementExist(itemElement, "div.fsl.fwb.fcb")) {
                                        WebElement profileNameElement = getElementByCssSelector(itemElement, "div.fsl.fwb.fcb");
                                        if (profileNameElement != null) {
                                            user.setName(profileNameElement.getText());
                                        }
                                    }
                                    if (isElementExist(itemElement, "button.FriendRequestAdd")) {
                                        WebElement addFriendButtonElement = getElementByCssSelector(itemElement, "button.FriendRequestAdd");
                                        if (isElementPresentAndDisplayed(addFriendButtonElement)) {
                                            user.setCanAddFriend(true);
                                        }
                                    }
                                }
                                reactionList.add(user);
                            }
                        }
                    });
                }
            }
        }
        Logger.debug(TAG, "#getArticleReactions: Url = " + articleUrl + " - reaction = " + reactionList.size());
        return reactionList;
    }

    private void loadMoreReactions(String loadMoreElementId, int currentPage, int maxPage) {
        if (currentPage <= maxPage) {
            waitDefault();
            WebElement loadMoreElement = getElementById(loadMoreElementId);
            if (isElementPresentAndDisplayed(loadMoreElement)) {
                WebElement loadMoreButtonElement = getElementByCssSelector(loadMoreElement, "a.uiMorePagerPrimary");
                if (isElementPresentAndDisplayed(loadMoreButtonElement)) {
                    scrollToElement(loadMoreElement);
                    clickElement(loadMoreButtonElement);
                    waitUntil(ExpectedConditions.elementToBeClickable(loadMoreButtonElement));
                    waitDefault();
                    loadMoreReactions(loadMoreElementId, currentPage + 1, maxPage);
                }
            }
        }
    }

    public List<Article> getUserMostRecentArticles(String userProfileUrl) {
        return getUserMostRecentArticles(userProfileUrl, 20);
    }

    public List<Article> getUserMostRecentArticles(String userProfileUrl, int limit) {
        Logger.debug(TAG, "#getUserRecentArticles: " + userProfileUrl);
        String convertedUrl = Utils.convertToFbWebUrl(userProfileUrl);
        if (!TextUtils.isEmpty(convertedUrl)) {
            User user = getUserInfo(userProfileUrl);
            if (user != null
                    && !TextUtils.isEmpty(user.getName())
                    && !TextUtils.isEmpty(user.getUniqueId())) {
                return getMostRecentArticles(getSearchPostByAuthorUrl(user.getName(), user.getUniqueId()), limit, false);
            }
        }
        return null;
    }

    private String getSearchPostByAuthorUrl(String name, String uniqueId) {
        Logger.debug(TAG, "#getSearchPostByAuthorUrl: Name=" + name + " - UniqueId=" + uniqueId);
        try {
            HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(FB_SEARCH_POST_BASE_URL)).newBuilder();
            urlBuilder.addEncodedQueryParameter("q", name);
            urlBuilder.addEncodedQueryParameter("filters_rp_author", URLEncoder.encode("{\"name\":\"author\",\"args\":\"" + uniqueId + "\"}", "UTF-8"));
            urlBuilder.addEncodedQueryParameter("filters_rp_chrono_sort", URLEncoder.encode("{\"name\":\"chronosort\",\"args\":\"\"}", "UTF-8"));
            return urlBuilder.build().uri().toString();
        } catch (UnsupportedEncodingException e) {
            Logger.warning(TAG, "UnsupportedEncodingException: " + e.getMessage());
            Logger.catching(e);
        }
        return null;
    }

    private String getSearchPostToGroupUrl(String name, String uniqueId) {
        Logger.debug(TAG, "#getSearchPostToGroupUrl: Name=" + name + " - UniqueId=" + uniqueId);
        try {
            HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(FB_SEARCH_POST_BASE_URL)).newBuilder();
            urlBuilder.addEncodedQueryParameter("q", name);
            urlBuilder.addEncodedQueryParameter("filters_rp_group", URLEncoder.encode("{\"name\":\"group_posts\",\"args\":\"" + uniqueId + "\"}", "UTF-8"));
            urlBuilder.addEncodedQueryParameter("filters_rp_chrono_sort", URLEncoder.encode("{\"name\":\"chronosort\",\"args\":\"\"}", "UTF-8"));
            return urlBuilder.build().uri().toString();
        } catch (UnsupportedEncodingException e) {
            Logger.warning(TAG, "UnsupportedEncodingException: " + e.getMessage());
            Logger.catching(e);
        }
        return null;
    }

    private String getSearchPostByMeToGroupUrl(User user, String groupUniqueId) {
        Logger.debug(TAG, "#getSearchPostByMeToGroupUrl: GroupUniqueId=" + groupUniqueId);
        if (user != null) {
            try {
                HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(FB_SEARCH_POST_BASE_URL)).newBuilder();
                urlBuilder.addEncodedQueryParameter("q", user.getName());
                urlBuilder.addEncodedQueryParameter("filters_rp_group", URLEncoder.encode("{\"name\":\"group_posts\",\"args\":\"" + groupUniqueId + "\"}", "UTF-8"));
                urlBuilder.addEncodedQueryParameter("filters_rp_chrono_sort", URLEncoder.encode("{\"name\":\"chronosort\",\"args\":\"\"}", "UTF-8"));
                urlBuilder.addEncodedQueryParameter("filters_rp_author", URLEncoder.encode("{\"name\":\"author_me\",\"args\":\"\"}", "UTF-8"));
                return urlBuilder.build().uri().toString();
            } catch (UnsupportedEncodingException e) {
                Logger.warning(TAG, "UnsupportedEncodingException: " + e.getMessage());
                Logger.catching(e);
            }
        }
        return null;
    }

    /**
     * @param searchPostUrl
     * @param limit
     * @param takePostToGroup true if also want to take the post to another people or group
     * @return
     */
    private List<Article> getMostRecentArticles(String searchPostUrl, int limit, boolean takePostToGroup) {
        Logger.debug(TAG, "#getMostRecentArticles: searchPostUrl = " + searchPostUrl + " - limit = " + limit);
        if (!TextUtils.isEmpty(searchPostUrl) && limit > 0) {
            openUrl(searchPostUrl);
            try {
                WebElement initialBrowseResultElement = waitUntil(ExpectedConditions.presenceOfElementLocated(By.id("initial_browse_result")));
                if (initialBrowseResultElement != null) {
                    if (!isElementExist(initialBrowseResultElement, By.id("empty_result_error"))) {
                        loadMoreRecentArticles(initialBrowseResultElement, 1, 8);
                        List<WebElement> articleElements = getElementsByCssSelector(initialBrowseResultElement, "div._5bl2._3u1._41je");
                        if (articleElements != null && !articleElements.isEmpty()) {
                            List<String> postLinkList = new ArrayList<>();
                            for (WebElement articleElement : articleElements) {
                                if (postLinkList.size() < limit && articleElement != null) {
                                    WebElement infoElement = getElementByCssSelector(articleElement, "div._42ef");
                                    if (infoElement != null) {
                                        WebElement timestampElement = getElementByCssSelector(infoElement, "a._5pcq, div._lie > a");
                                        if (timestampElement != null) {
                                            String href = timestampElement.getAttribute("href");
                                            if (!TextUtils.isEmpty(href)) {
                                                if (takePostToGroup) {
                                                    postLinkList.add(href);
                                                } else if (!isElementExist(articleElement, "i.mhs.img")) {
                                                    if (!Group.isGroupUrl(href)) {
                                                        postLinkList.add(href);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else if (postLinkList.size() >= limit) {
                                    break;
                                }
                            }
                            if (!postLinkList.isEmpty()) {
                                return postLinkList.stream().filter(href -> !TextUtils.isEmpty(href)).map(Article::from).filter(Objects::nonNull).collect(Collectors.toList());
                            } else {
                                Logger.debug(TAG, "#getMostRecentArticles: cannot get any article link");
                            }
                        } else {
                            Logger.debug(TAG, "#getMostRecentArticles: cannot find any articles");
                        }
                    } else {
                        Logger.debug(TAG, "#getMostRecentArticles: result empty");
                    }
                } else {
                    Logger.debug(TAG, "#getMostRecentArticles: initial_browse_result empty");
                }
            } catch (NullPointerException e) {
                Logger.warning(TAG, "NullPointerException: " + e.getMessage());
                Logger.catching(e);
            }
        }
        return null;
    }

    private void loadMoreRecentArticles(WebElement parentElement, int currentPage, int maxPage) {
        if (currentPage <= maxPage) {
            waitDefault();
            if (!isElementExist(parentElement, By.id("browse_end_of_results_footer"))) {
                WebElement loadMoreElementWrapper = getElementByCssSelector(parentElement, "div._akp");
                if (isElementPresentAndDisplayed(loadMoreElementWrapper)) {
                    scrollToElement(loadMoreElementWrapper);
                    waitDefault();
                    WebElement loadMoreElement = getElementByCssSelector(loadMoreElementWrapper, "div");
                    waitUntil(ExpectedConditions.or(ExpectedConditions.attributeToBe(loadMoreElement, "class", "hidden_elem")
                            , ExpectedConditions.presenceOfElementLocated(By.id("browse_end_of_results_footer"))));
                    loadMoreRecentArticles(parentElement, currentPage + 1, maxPage);
                }
            }
        }
    }

    public List<Article> getSelfMostRecentArticlesViaActivityLog(User user) {
        return getSelfMostRecentArticlesViaActivityLog(user, 20);
    }

    public List<Article> getSelfMostRecentArticlesViaActivityLog(User user, int limit) {
        Logger.debug(TAG, "#getSelfMostRecentArticlesViaActivityLog: limit=" + limit);
        if (user != null) {
            openUrl(String.format(FB_ACTIVITY_LOG_POSTS_URL, user.getUniqueId()));
            if (isElementExist(By.id("fbTimelineLogBody"))) {
                WebElement fbTimelineLogBodyElement = getElementById("fbTimelineLogBody");
                if (fbTimelineLogBodyElement != null) {
                    wait(20, TimeUnit.SECONDS);
                    if (!isElementExist(fbTimelineLogBodyElement, "a.uiMorePagerPrimary")) {
                        scrollToBottom();
                        wait(1, TimeUnit.SECONDS);
                    }
                    loadMoreActivityLog(fbTimelineLogBodyElement, 1, 8);
                    List<WebElement> articleElements = getElementsByCssSelector(fbTimelineLogBodyElement, "ul.uiList._4kg > li");
                    if (articleElements != null && !articleElements.isEmpty()) {
                        List<String> postLinkList = new ArrayList<>();
                        for (WebElement articleElement : articleElements) {
                            if (postLinkList.size() < limit && articleElement != null) {
                                WebElement timestampElement = getElementByCssSelector(articleElement, "span._5shl.fss > a");
                                if (timestampElement != null) {
                                    String href = timestampElement.getAttribute("href");
                                    if (!TextUtils.isEmpty(href)) {
                                        postLinkList.add(href);
                                    }
                                }
                            } else if (postLinkList.size() >= limit) {
                                break;
                            }
                        }
                        if (!postLinkList.isEmpty()) {
                            return postLinkList.stream().filter(href -> !TextUtils.isEmpty(href)).map(Article::from).filter(Objects::nonNull).collect(Collectors.toList());
                        } else {
                            Logger.debug(TAG, "#getSelfMostRecentArticlesViaActivityLog: cannot get any article link");
                        }
                    } else {
                        Logger.debug(TAG, "#getSelfMostRecentArticlesViaActivityLog: cannot find any articles");
                    }
                } else {
                    Logger.debug(TAG, "#getSelfMostRecentArticlesViaActivityLog: fbTimelineLogBody null");
                }
            } else {
                Logger.debug(TAG, "#getSelfMostRecentArticlesViaActivityLog: fbTimelineLogBody not exist");
            }
        }
        return null;
    }

    public List<Article> getSelfGroupArticlesViaActivityLog(String groupUrl, User user) {
        return getSelfGroupArticlesViaActivityLog(groupUrl, user, 20);
    }

    public List<Article> getSelfGroupArticlesViaActivityLog(String groupUrl, User user, int limit) {
        Logger.debug(TAG, "#getSelfGroupArticlesViaActivityLog: limit=" + limit);
        if (user != null) {
            Group sourceGroup = Group.fromUrl(groupUrl);
            if (sourceGroup != null) {
                openUrl(String.format(FB_ACTIVITY_LOG_GROUP_POSTS_URL, user.getUniqueId()));
                if (isElementExist(By.id("fbTimelineLogBody"))) {
                    WebElement fbTimelineLogBodyElement = getElementById("fbTimelineLogBody");
                    if (fbTimelineLogBodyElement != null) {
                        wait(20, TimeUnit.SECONDS);
                        if (!isElementExist(fbTimelineLogBodyElement, "a.uiMorePagerPrimary")) {
                            scrollToBottom();
                            wait(1, TimeUnit.SECONDS);
                        }
                        loadMoreActivityLog(fbTimelineLogBodyElement, 1, 50);
                        List<WebElement> articleElements = getElementsByCssSelector(fbTimelineLogBodyElement, "ul.uiList._4kg > li");
                        if (articleElements != null && !articleElements.isEmpty()) {
                            List<String> postLinkList = new ArrayList<>();
                            for (WebElement articleElement : articleElements) {
                                if (postLinkList.size() < limit && articleElement != null) {
                                    WebElement timestampElement = getElementByCssSelector(articleElement, "span._5shl.fss > a");
                                    if (timestampElement != null) {
                                        String href = timestampElement.getAttribute("href");
                                        if (!TextUtils.isEmpty(href)) {
                                            Group targetGroup = Group.fromUrl(href);
                                            if (targetGroup != null && TextUtils.equal(sourceGroup.getGroupId(), targetGroup.getGroupId())) {
                                                postLinkList.add(href);
                                            }
                                        }
                                    }
                                } else if (postLinkList.size() >= limit) {
                                    break;
                                }
                            }
                            if (!postLinkList.isEmpty()) {
                                return postLinkList.stream().filter(href -> !TextUtils.isEmpty(href)).map(Article::from).filter(Objects::nonNull).collect(Collectors.toList());
                            } else {
                                Logger.debug(TAG, "#getSelfGroupArticlesViaActivityLog: cannot get any article link");
                            }
                        } else {
                            Logger.debug(TAG, "#getSelfGroupArticlesViaActivityLog: cannot find any articles");
                        }
                    } else {
                        Logger.debug(TAG, "#getSelfGroupArticlesViaActivityLog: fbTimelineLogBody null");
                    }
                } else {
                    Logger.debug(TAG, "#getSelfGroupArticlesViaActivityLog: fbTimelineLogBody not exist");
                }
            } else {
                Logger.debug(TAG, "#getSelfGroupArticlesViaActivityLog: " + groupUrl + " is not url group");
            }
        }
        return null;
    }

    private void loadMoreActivityLog(WebElement parentElement, int currentPage, int maxPage) {
        if (currentPage <= maxPage) {
            waitDefault();
            if (isElementExist(parentElement, "a.uiMorePagerPrimary")) {
                List<WebElement> postListItemElements = getElementsByCssSelector(parentElement, "ul.uiList._4kg > li");
                if (postListItemElements != null) {
                    WebElement loadMoreElement = getElementByCssSelector(parentElement, "a.uiMorePagerPrimary");
                    scrollToElement(loadMoreElement);
                    wait(2, TimeUnit.SECONDS);
                    Boolean waitResult = waitUntil(ExpectedConditions.and(
                            ExpectedConditions.presenceOfNestedElementLocatedBy(parentElement, By.cssSelector("a.uiMorePagerPrimary")),
                            SeleniumExpectedConditions.numberOfElementsToBeMoreThan(parentElement, By.cssSelector("ul.uiList._4kg > li"), postListItemElements.size())));
                    if (waitResult != null && waitResult) {
                        if (isElementExist(parentElement, "a.uiMorePagerPrimary")) {
                            loadMoreActivityLog(parentElement, currentPage + 1, maxPage);
                        }
                    }
                }
            }
        }
    }

    public final void likeFanPage(String pageUrl, IActionResponse<Void, LikeErrorResponse> response) {
        Logger.debug(TAG, "#likeFanPage: " + pageUrl);
        String convertedUrl = Utils.convertToFbWebUrl(pageUrl);
        if (!TextUtils.isEmpty(convertedUrl)) {
            openUrl(convertedUrl);
            wait(1, TimeUnit.SECONDS);
            WebElement likeButtonElement = getElementByCssSelector("button.likeButton");
            if (likeButtonElement != null) {
                clickElement(likeButtonElement);
                IActionResponse.onSuccess(response);
            } else {
                WebElement likedButtonElement = getElementByCssSelector("a.likedButton");
                if (likedButtonElement != null) {
                    IActionResponse.onError(response, new LikeErrorResponse(LikeErrorResponse.CODE_LIKED_ALREADY, String.format("PageUrl: %s", pageUrl)));
                } else {
                    IActionResponse.onErrorSyncServer(response, new LikeErrorResponse(LikeErrorResponse.CODE_CANNOT_INTERACTIVE, String.format("PageUrl: %s", pageUrl)));
                }
            }
        } else {
            IActionResponse.onErrorSyncServer(response, new LikeErrorResponse(LikeErrorResponse.CODE_INVALID_URL, String.format("PageUrl: %s", pageUrl)));
        }
    }

    public final void likeArticle(String articleUrl, IActionResponse<Void, LikeErrorResponse> response) {
        Logger.debug(TAG, "#likeArticle: " + articleUrl);
        String convertedUrl = Utils.convertToFbWebUrl(articleUrl);
        if (!TextUtils.isEmpty(convertedUrl)) {
            openUrl(convertedUrl);
            closeImageDialogIfPresence();
            waitDefault();
            WebElement likeElementWrapper = waitUntil(SeleniumExpectedConditions.presenceAndDisplayOfElement(By.cssSelector("div.clearfix._zw3")));
            if (likeElementWrapper != null) {
                WebElement likeLinkElement = getElementByCssSelector(likeElementWrapper, "a.UFILikeLink");
                if (likeLinkElement != null) {
                    String liked = likeLinkElement.getAttribute("aria-pressed");
                    if (TextUtils.isEmpty(liked) || "false".equals(liked.toLowerCase())) {
                        clickElement(likeLinkElement);
                        wait(1500, TimeUnit.MILLISECONDS);
                        Boolean likeAttribute = waitUntil(ExpectedConditions.attributeToBe(likeLinkElement, "aria-pressed", "true"));
                        if (likeAttribute != null && likeAttribute) {
                            IActionResponse.onSuccess(response);
                        } else {
                            IActionResponse.onErrorSyncServer(response, new LikeErrorResponse(LikeErrorResponse.CODE_TIME_OUT, String.format("ArticleUrl: %s", articleUrl)));
                        }
                    } else {
                        IActionResponse.onError(response, new LikeErrorResponse(LikeErrorResponse.CODE_LIKED_ALREADY, String.format("ArticleUrl: %s", articleUrl)));
                    }
                } else {
                    IActionResponse.onErrorSyncServer(response, new LikeErrorResponse(LikeErrorResponse.CODE_MISSING_REACT_PERMISSION, String.format("ArticleUrl: %s", articleUrl)));
                }
            } else {
                IActionResponse.onErrorSyncServer(response, new LikeErrorResponse(LikeErrorResponse.CODE_CANNOT_INTERACTIVE, String.format("ArticleUrl: %s", articleUrl)));
            }
        } else {
            IActionResponse.onErrorSyncServer(response, new LikeErrorResponse(LikeErrorResponse.CODE_INVALID_URL, String.format("ArticleUrl: %s", articleUrl)));
        }
    }

    public final void sendMessageTo(String userId, String message, IActionResponse<Void, MessagingErrorResponse> response) {
        Logger.debug(TAG, "#sendMessageTo: " + userId + " - " + message);
        if (!TextUtils.isEmpty(userId) && !TextUtils.isEmpty(message)) {
            openUrl(FB_MESSAGES_BASE_URL + userId);
            WebElement globalContainer = getElementById("globalContainer");
            if (globalContainer != null) {
                waitDefault();
                WebElement acceptButtonElement = getElementByCssSelector(globalContainer, "button._2t_");
                if (acceptButtonElement != null) {
                    clickElement(acceptButtonElement);
                    wait(1, TimeUnit.SECONDS);
                }
                WebElement inputElem = getElementByCssSelector(globalContainer, "div.navigationFocus");
                if (inputElem != null) {
                    clickElement(inputElem);
                    waitDefault();
                    Actions sendMessageActions = new Actions(getWebDriver());
                    buildActionSendKeys(sendMessageActions, message);
                    sendMessageActions.pause(1000);
                    sendMessageActions.sendKeys(Keys.ENTER);
                    sendMessageActions.build().perform();
                    wait(3, TimeUnit.SECONDS);
                    WebElement errorElement = getElementByCssSelector(globalContainer, "span._5ei9");
                    if (errorElement == null) {
                        WebElement captchaDialogElement = getElementByCssSelector("div.captcha_dialog");
                        if (captchaDialogElement == null) {
                            IActionResponse.onSuccess(response);
                        } else {
                            IActionResponse.onErrorSyncServer(response, new MessagingErrorResponse(MessagingErrorResponse.CODE_SECURITY_CHECK, String.format("UserId: %s - Message: %s", userId, message)));
                        }
                    } else {
                        IActionResponse.onErrorSyncServer(response, new MessagingErrorResponse(MessagingErrorResponse.CODE_SEND_ERROR, String.format("UserId: %s - Message: %s", userId, message)));
                    }
                    return;
                }
            }
            IActionResponse.onErrorSyncServer(response, new MessagingErrorResponse(MessagingErrorResponse.CODE_MESSENGER_LOADED_FAIL, String.format("UserId: %s - Message: %s", userId, message)));
        } else {
            if (TextUtils.isEmpty(userId)) {
                IActionResponse.onErrorSyncServer(response, new MessagingErrorResponse(MessagingErrorResponse.CODE_INVALID_USER_ID, String.format("UserId: %s - Message: %s", userId, message)));
            } else {
                IActionResponse.onErrorSyncServer(response, new MessagingErrorResponse(MessagingErrorResponse.CODE_MESSAGE_EMPTY, String.format("UserId: %s - Message: %s", userId, message)));
            }
        }
    }

    private void buildActionSendKeys(Actions actions, String text) {
        if (actions != null && !TextUtils.isEmpty(text)) {
            text = Utils.simpleLineEndText(text);
            char[] chars = text.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                char c = chars[i];
                if (c == '\n') {
                    actions.sendKeys(Keys.chord(Keys.SHIFT, Keys.ENTER));
                } else {
                    actions.sendKeys(String.valueOf(c));
                }
            }
        }
    }

    public final void followingTo(String userProfileUrl, IActionResponse<Void, FollowingErrorResponse> response) {
        Logger.debug(TAG, "#followingTo: " + userProfileUrl);
        String convertedUrl = Utils.convertToFbWebUrl(userProfileUrl);
        if (!TextUtils.isEmpty(convertedUrl)) {
            openUrl(convertedUrl);
            WebElement timelineProfileActionElement = getElementById("pagelet_timeline_profile_actions");
            if (timelineProfileActionElement != null) {
                List<WebElement> followStatusElements = getElementsByCssSelector(timelineProfileActionElement, "button[data-status='follow']");
                WebElement followStatusElement = null;
                if (followStatusElements != null && !followStatusElements.isEmpty()) {
                    followStatusElement = followStatusElements.get(0);
                }
                if (followStatusElement != null) {
                    //followed
                    IActionResponse.onError(response, new FollowingErrorResponse(FollowingErrorResponse.CODE_FOLLOWED, String.format("UserProfile: %s", userProfileUrl)));
                } else {
                    //not followed
                    List<WebElement> followingButtonElements = getElementsByCssSelector(timelineProfileActionElement, "a[rel='async-post']");
                    WebElement followingButton = null;
                    if (followingButtonElements != null && !followingButtonElements.isEmpty()) {
                        followingButton = followingButtonElements.get(0);
                    }
                    if (followingButton != null) {
                        clickElement(followingButton);
                        IActionResponse.onSuccess(response);
                    } else {
                        IActionResponse.onErrorSyncServer(response, new FollowingErrorResponse(FollowingErrorResponse.CODE_TARGET_USER_FOLLOW_DISABLE, String.format("UserProfile: %s", userProfileUrl)));
                    }
                }
                return;
            }
        }
        IActionResponse.onErrorSyncServer(response, new FollowingErrorResponse(FollowingErrorResponse.CODE_INVALID_URL, String.format("UserProfile: %s", userProfileUrl)));
    }

    /*
     * make friend
     *
     * */
    public final void sendAddFriendRequestTo(String userProfileUrl, IActionResponse<Void, AddFriendErrorResponse> response) {
        Logger.debug(TAG, "#sendAddFriendRequestTo: " + userProfileUrl);
        String convertedUrl = Utils.convertToFbWebUrl(userProfileUrl);
        if (!TextUtils.isEmpty(convertedUrl)) {
            openUrl(convertedUrl);
            WebElement timelineProfileActionElement = getElementById("pagelet_timeline_profile_actions");
            if (timelineProfileActionElement != null) {
                if (isElementExist(timelineProfileActionElement, "a.FriendRequestFriends")) {
                    //already friend
                    IActionResponse.onError(response, new AddFriendErrorResponse(AddFriendErrorResponse.CODE_FRIEND_ALREADY, String.format("UserProfile: %s", userProfileUrl)));
                } else {
                    //not friend
                    List<WebElement> addFriendButtonElements = getElementsByCssSelector(timelineProfileActionElement, "button.FriendRequestAdd");
                    WebElement addFriendButton = null;
                    if (addFriendButtonElements != null && !addFriendButtonElements.isEmpty()) {
                        addFriendButton = addFriendButtonElements.get(0);
                    }
                    if (addFriendButton != null) {
                        if (isElementPresentAndDisplayed(addFriendButton)) {
                            clickElement(addFriendButton);
                            doAddFriendPostProcess();
                            IActionResponse.onSuccess(response);
                        } else {
                            WebElement friendRequestSentButton = getElementByCssSelector(timelineProfileActionElement, "button.FriendRequestOutgoing");
                            if (isElementPresentAndDisplayed(friendRequestSentButton)) {
                                IActionResponse.onError(response, new AddFriendErrorResponse(AddFriendErrorResponse.CODE_FRIEND_REQUEST_SENT, String.format("UserProfile: %s", userProfileUrl)));
                            }
                        }
                    } else {
                        IActionResponse.onErrorSyncServer(response, new AddFriendErrorResponse(AddFriendErrorResponse.CODE_TARGET_USER_ADD_FRIEND_DISABLED, String.format("UserProfile: %s", userProfileUrl)));
                    }
                }
                return;
            }
        }
        IActionResponse.onErrorSyncServer(response, new AddFriendErrorResponse(AddFriendErrorResponse.CODE_INVALID_URL, String.format("UserProfile: %s", userProfileUrl)));
    }

    public final List<User> getListUserAcceptFriend() {
        Logger.debug(TAG, "#getListUserAcceptFriend");
        List<User> listUserResult = new ArrayList<>();
        openUrl(FB_FRIEND_REQUEST_URL + "?fcref=none");
        WebElement parentContent = getElementById("content");
        List<WebElement> elementAcceptFriends = getElementsByCssSelector(parentContent, "div.clearfix > div.lfloat._ohe > div.mtl._30d._5ewg._5n-u");
        if (elementAcceptFriends != null) {
            int size = elementAcceptFriends.size() - 1;
            WebElement itemAcceptFriend = elementAcceptFriends.get(size);
            WebElement webContainerListItem = getElementByCssSelector(itemAcceptFriend, "div.phl");
            WebElement webLoadMore = getElementByCssSelector(webContainerListItem, "div#FriendRequestMorePager");
            if (webLoadMore != null) {
                loadMoreListFriendRequestSent(webLoadMore, 1, 10);
            }
            List<WebElement> listItemFriend = getElementsByCssSelector(webContainerListItem, "div.friendRequestItem");
            if (listItemFriend != null) {
                for (WebElement itemFriend : listItemFriend) {
                    WebElement elementProfile = getElementByCssSelector(itemFriend, "a._ohe");
                    String linkProfile = elementProfile.getAttribute("href");
                    if (!TextUtils.isEmpty(linkProfile)) {
                        User user = new User(linkProfile);
                        listUserResult.add(user);
                    }
                }
            }
        }
        return listUserResult;
    }

    public final void acceptFriend(String linkProfile, IActionResponse<Void, AcceptFriendResponse> response) {
        Logger.debug(TAG, "#linkProfile: " + linkProfile);
        openUrl(linkProfile);
        WebElement elementAboveHeader = getElementById("pagelet_above_header_timeline");
        if (isElementPresentAndDisplayed(elementAboveHeader)) {
            WebElement webElementAccept = getElementByCssSelector(elementAboveHeader, "a.selected");
            clickElement(webElementAccept);
            IActionResponse.onSuccess(response);
        } else {
            IActionResponse.onError(response, new AcceptFriendResponse(0));
        }

    }

    public final void unFriend(String linkProfile, IActionResponse<Void, AcceptFriendResponse> response) {
        openUrl(linkProfile);
        WebElement elementTimelineProfileAction = getElementById("pagelet_timeline_profile_actions");
        if (elementTimelineProfileAction != null) {
            WebElement friendButton = getElementByCssSelector(elementTimelineProfileAction, "div.FriendButton");
            if (friendButton != null) {
                WebElement buttonUnFriend = getElementByCssSelector(friendButton, "button.FriendRequestOutgoing");
                if (isElementPresentAndDisplayed(buttonUnFriend)) {
                    clickElement(buttonUnFriend);
                    wait(Utils.getRandomNumber(5, 10), TimeUnit.SECONDS);
                    WebElement popupMenu = getElementById("friendFlyoutContent");
                    if (popupMenu != null) {
                        WebElement listActionMenu = getElementByCssSelector(popupMenu, "div.FriendListActionMenu > ul.uiMenuInner");
                        WebElement cancelFriend = getElementByCssSelector(listActionMenu, "li.uiMenuItem.FriendListCancel");
                        if (cancelFriend != null) {
                            clickElement(cancelFriend);
                            wait(Utils.getRandomNumber(5, 10), TimeUnit.SECONDS);
                            WebElement popupConfirm = getElementByCssSelector("div[class='_10 uiLayer _4-hy _3qw']");
                            if (isElementPresentAndDisplayed(popupConfirm)) {
                                WebElement buttonConfirm = getElementByCssSelector(popupConfirm, "button.layerConfirm.uiOverlayButton");
                                if (buttonConfirm != null) {
                                    clickElement(buttonConfirm);
                                }
                                wait(Utils.getRandomNumber(5, 10), TimeUnit.SECONDS);
                                IActionResponse.onSuccess(response);
                            }
                        }
                    }

                } else {
                    IActionResponse.onError(response, new AcceptFriendResponse(0));
                }
            } else {
                IActionResponse.onError(response, new AcceptFriendResponse(0));
            }
        }


    }

    private void doAddFriendPostProcess() {
        wait(3, TimeUnit.SECONDS);
        WebElement uiOverlayElement = getElementByCssSelector("div.uiOverlayFooter");
        if (uiOverlayElement != null) {
            WebElement confirmButton = getElementByCssSelector(uiOverlayElement, "button.layerConfirm");
            if (isElementPresentAndDisplayed(confirmButton)) {
                clickElement(confirmButton);
            }
        }
    }

    private String getFollowingListUrl(String userProfileUrl) {
        User user = new User(userProfileUrl);
        openUrl(user.getFriendsUrl());
        waitDefault();
        List<WebElement> tabElements = getElementsByCssSelector("a._3c_");
        if (tabElements != null && !tabElements.isEmpty()) {
            for (WebElement tab : tabElements) {
                if (tab != null) {
                    String ariaControlsContent = tab.getAttribute("aria-controls");
                    if (!TextUtils.isEmpty(ariaControlsContent) && ariaControlsContent.contains(":33")) {
                        String href = tab.getAttribute("href");
                        if (!TextUtils.isEmpty(href)) {
                            return href;
                        }
                    }
                }
            }
        }
        return Constants.EMPTY_STRING;
    }

    public final List<User> getFollowingList(String userProfileUrl) {
        Logger.debug(TAG, "#getFollowingList: " + userProfileUrl);
        List<User> followingList = getFollowList(userProfileUrl, false, false);
        Logger.debug(TAG, "#getFollowingList: " + userProfileUrl + " - Size: " + followingList.size());
        return followingList;
    }

    public final List<User> getFollowerList(String userProfileUrl) {
        Logger.debug(TAG, "#getFollowerList: " + userProfileUrl);
        List<User> followerList = getFollowerList(userProfileUrl, false);
        Logger.debug(TAG, "#getFollowerList: " + userProfileUrl + " - Size: " + followerList.size());
        return followerList;
    }

    public final List<User> getFollowerList(String userProfileUrl, boolean takeAll) {
        return getFollowList(userProfileUrl, true, takeAll);
    }

    public final List<User> getListFriendRequest() {
        Logger.debug(TAG, "#getListFriendRequest");
        List<User> friendRequests = new ArrayList<>();
        openUrl(FB_FRIEND_REQUEST_URL);
        waitDefault();
        List<WebElement> friendRequestElements = getElementsByCssSelector("div.friendRequestItem");
        if (friendRequestElements != null && !friendRequestElements.isEmpty()) {
            for (WebElement requestElement : friendRequestElements) {
                if (requestElement != null) {
                    WebElement userLinkElement = getElementByCssSelector(requestElement, "a.lfloat._ohe");
                    if (userLinkElement != null) {
                        String userLink = userLinkElement.getAttribute("href");
                        User user = new User(userLink);
                        WebElement profileNameElement = getElementByCssSelector(requestElement, "div._6-_");
                        if (profileNameElement != null) {
                            user.setName(profileNameElement.getText());
                        }
                        friendRequests.add(user);
                    }
                }
            }
        }
        return friendRequests;
    }

    private String getFollowerListUrl(String userProfileUrl) {
        User user = new User(userProfileUrl);
        openUrl(user.getFriendsUrl());
        waitDefault();
        List<WebElement> tabElements = getElementsByCssSelector("a._3c_");
        if (tabElements != null && !tabElements.isEmpty()) {
            for (WebElement tab : tabElements) {
                if (tab != null) {
                    String ariaControlsContent = tab.getAttribute("aria-controls");
                    if (!TextUtils.isEmpty(ariaControlsContent) && ariaControlsContent.contains(":32")) {
                        String href = tab.getAttribute("href");
                        if (!TextUtils.isEmpty(href)) {
                            return href;
                        }
                    }
                }
            }
        }
        return Constants.EMPTY_STRING;
    }

    private List<User> getFollowList(String userProfileUrl, boolean getFollower, boolean takeAll) {
        List<User> followList = new ArrayList<>();
        String followListUrl;
        if (getFollower) {
            followListUrl = getFollowerListUrl(userProfileUrl);
        } else {
            followListUrl = getFollowingListUrl(userProfileUrl);
        }
        if (!TextUtils.isEmpty(followListUrl)) {
            openUrl(followListUrl);
            waitDefault();
            WebElement wrapperListElement = waitUntil(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.fbProfileBrowserListContainer.fbProfileBrowserList")));
            if (wrapperListElement != null) {
                if (takeAll) {
                    loadMoreFollowList(wrapperListElement);
                } else {
                    loadMoreFollowList(wrapperListElement, 1, FB_LOAD_MORE_MAX_PAGE);
                }
                List<WebElement> userItemList = getElementsByCssSelector(wrapperListElement, "li.fbProfileBrowserListItem");
                if (userItemList != null && !userItemList.isEmpty()) {
                    userItemList.parallelStream().forEach(itemElement -> {
                        if (itemElement != null) {
                            WebElement userLink = getElementByCssSelector(itemElement, "a.lfloat._ohe");
                            if (userLink != null) {
                                String href = userLink.getAttribute("href");
                                User user = new User(href);
                                user.setCanAddFriend(true);
                                if (getFollower) {
                                    if (isElementExist(itemElement, "div.fsl.fwb.fcb")) {
                                        WebElement profileNameElement = getElementByCssSelector(itemElement, "div.fsl.fwb.fcb");
                                        if (profileNameElement != null) {
                                            user.setName(profileNameElement.getText());
                                        }
                                    }
                                } else {
                                    if (isElementExist(itemElement, "span.fsl.fwb.fcb")) {
                                        WebElement profileNameElement = getElementByCssSelector(itemElement, "span.fsl.fwb.fcb");
                                        if (profileNameElement != null) {
                                            String alternateName = null;
                                            List<WebElement> alternateNameElements = getElementsByCssSelector(profileNameElement, "span.alternate_name");
                                            WebElement alternateNameElement = null;
                                            if (alternateNameElements != null && !alternateNameElements.isEmpty()) {
                                                alternateNameElement = alternateNameElements.get(0);
                                            }
                                            if (alternateNameElement != null) {
                                                alternateName = alternateNameElement.getText();
                                            }
                                            String fullName = profileNameElement.getText();
                                            String finalName;
                                            if (!TextUtils.isEmpty(fullName) && !TextUtils.isEmpty(alternateName) && fullName.contains(alternateName)) {
                                                finalName = fullName.substring(0, fullName.lastIndexOf(alternateName)).trim();
                                            } else {
                                                finalName = fullName;
                                            }
                                            user.setName(finalName);
                                        }
                                    }
                                }
                                if (isElementExist(itemElement, "a[rel='async-post']")) {
                                    user.setFollowable(true);
                                }
                                followList.add(user);
                            }
                        }
                    });
                }
            }
        }
        return followList;
    }

    private void loadMoreFollowList(WebElement parentElement) {
        waitDefault();
        List<WebElement> loadMoreElements = getElementsByCssSelector(parentElement, "div.morePager");
        WebElement loadMoreElement = null;
        if (loadMoreElements != null && !loadMoreElements.isEmpty()) {
            loadMoreElement = loadMoreElements.get(0);
        }
        if (isElementPresentAndDisplayed(loadMoreElement)) {
            scrollToElement(loadMoreElement);
            waitDefault();
            if (isElementExist("div.morePager a")) {
                WebElement hrefElement = waitUntil(ExpectedConditions.elementToBeClickable(By.cssSelector("div.morePager a")));
                if (hrefElement != null) {
                    loadMoreFollowList(parentElement);
                }
            }
        }
    }

    private void loadMoreFollowList(WebElement parentElement, int currentPage, int maxPage) {
        if (currentPage <= maxPage) {
            waitDefault();
            List<WebElement> loadMoreElements = getElementsByCssSelector(parentElement, "div.morePager");
            WebElement loadMoreElement = null;
            if (loadMoreElements != null && !loadMoreElements.isEmpty()) {
                loadMoreElement = loadMoreElements.get(0);
            }
            if (isElementPresentAndDisplayed(loadMoreElement)) {
                scrollToElement(loadMoreElement);
                waitDefault();
                if (isElementExist("div.morePager a")) {
                    WebElement hrefElement = waitUntil(ExpectedConditions.elementToBeClickable(By.cssSelector("div.morePager a")));
                    if (hrefElement != null) {
                        loadMoreFollowList(parentElement, currentPage + 1, maxPage);
                    }
                }
            }
        }
    }

    public final User getMyUserInfo() {
        openUrl(FB_BASE_URL);
        waitDefault();
        WebElement profileIconElement = getElementByCssSelector("div[data-click='profile_icon']");
        if (profileIconElement != null) {
            WebElement profileUrlElement = getElementByCssSelector(profileIconElement, "a._2s25._606w");
            if (profileUrlElement != null) {
                String profileUrl = profileUrlElement.getAttribute("href");
                if (!TextUtils.isEmpty(profileUrl)) {
                    return getUserInfo(profileUrl);
                }
            }
        }
        return null;
    }

    public final User getUserInfo(String userProfileUrl) {
        Logger.debug(TAG, "#getUserInfo: " + userProfileUrl);
        String convertedUrl = Utils.convertToFbWebUrl(userProfileUrl);
        if (!TextUtils.isEmpty(convertedUrl)) {
            User user = new User(convertedUrl);
            openUrl(convertedUrl);
            WebElement avatarElement = getElementByCssSelector("a.profilePicThumb > img");
            if (avatarElement != null) {
                String avatarSrc = avatarElement.getAttribute("src");
                user.setAvatarUrl(avatarSrc);
            }
            WebElement coverNameElement = getElementById("fb-timeline-cover-name");
            if (coverNameElement != null) {
                String coverName = coverNameElement.getText();
                String alternateName = null;
                List<WebElement> alternateNameElements = getElementsByCssSelector(coverNameElement, "span.alternate_name");
                WebElement alternateNameElement = null;
                if (alternateNameElements != null && !alternateNameElements.isEmpty()) {
                    alternateNameElement = alternateNameElements.get(0);
                }
                if (alternateNameElement != null) {
                    alternateName = alternateNameElement.getText();
                }
                String finalName;
                if (!TextUtils.isEmpty(coverName) && !TextUtils.isEmpty(alternateName) && coverName.contains(alternateName)) {
                    finalName = coverName.substring(0, coverName.lastIndexOf(alternateName)).trim();
                } else {
                    finalName = coverName;
                }
                user.setName(finalName);
            }
            WebElement uniqueIdMetadata = getElementByCssSelector("meta[property='al:android:url']");
            if (uniqueIdMetadata != null) {
                String metadataContent = uniqueIdMetadata.getAttribute("content");
                if (!TextUtils.isEmpty(metadataContent)) {
                    try {
                        URI uri = URI.create(metadataContent);
                        String host = uri.getHost();
                        if (!TextUtils.isEmpty(host) && host.contains("profile")) {
                            String uniqueId = uri.getPath();
                            if (!TextUtils.isEmpty(uniqueId) && uniqueId.startsWith("/")) {
                                uniqueId = uniqueId.substring(1);
                            }
                            user.setUniqueId(uniqueId);
                        }
                    } catch (IllegalArgumentException e) {
                        Logger.warning(TAG, "IllegalArgumentException: " + e.getMessage());
                        Logger.catching(e);
                    }
                }
            }
            return user;
        }
        return null;
    }

    public final List<User> getListUserSendFriend() {
        Logger.debug(TAG, "#getListUserSendFriend");
        List<User> listUser = new ArrayList<>();
        openUrl(FB_FRIEND_REQUEST_URL + "?outgoing=1");
        List<WebElement> userBoxWrapperList = getElementsByCssSelector("div.phl");
        if (userBoxWrapperList != null && !userBoxWrapperList.isEmpty()) {
            WebElement userBoxWrapper = null;
            for (WebElement element : userBoxWrapperList) {
                if (element != null) {
                    WebElement friendRequestOutGoingButtonElement = getElementByCssSelector(element, "button.FriendRequestOutgoing");
                    if (isElementPresentAndDisplayed(friendRequestOutGoingButtonElement)) {
                        userBoxWrapper = element;
                    }
                }
            }
            if (userBoxWrapper != null) {
                WebElement friendRequestSentItem = getElementByCssSelector(userBoxWrapper, "div.ruUserBox");
                if (friendRequestSentItem != null) {
                    loadMoreListFriendRequestSent(userBoxWrapper, 1, 10);
                    List<WebElement> listCellUser = getElementsByCssSelector(userBoxWrapper, "div.ruUserBox");
                    if (listCellUser != null && !listCellUser.isEmpty()) {
                        for (WebElement cellUser : listCellUser) {
                            if (cellUser != null) {
                                WebElement element = getElementByCssSelector(cellUser, "a.lfloat._ohe");
                                if (element != null) {
                                    String urlProfile = element.getAttribute("href");
                                    if (!TextUtils.isEmpty(urlProfile)) {
                                        User user = new User(urlProfile);
                                        WebElement profileNameElement = getElementByCssSelector(cellUser, "div._6-_");
                                        if (profileNameElement != null) {
                                            user.setName(profileNameElement.getText());
                                        }
                                        listUser.add(user);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return listUser;
    }

    private void loadMoreListFriendRequestSent(WebElement parentElement, int currentPage, int maxPage) {
        if (currentPage <= maxPage) {
            waitDefault();
            WebElement loadMoreElement = getElementByCssSelector(parentElement, "a.uiMorePagerPrimary");
            if (isElementPresentAndDisplayed(loadMoreElement)) {
                clickElement(loadMoreElement);
                waitDefault();
                waitUntil(ExpectedConditions.elementToBeClickable(loadMoreElement));
                loadMoreListFriendRequestSent(parentElement, currentPage + 1, maxPage);
            }
        }
    }

    public final List<User> getListConversation() {
        return getListConversation(0);
    }

    public final List<User> getListConversation(long sinceTimeMillis) {
        Logger.debug(TAG, "#getListConversation: since " + sinceTimeMillis);
        List<User> listUser = new ArrayList<>();
        openUrl(FB_MESSAGES_BASE_URL);
        WebElement globalContainer = getElementById("globalContainer");
        if (globalContainer != null) {
            waitDefault();
            WebElement navigationElement = getElementByCssSelector(globalContainer, "div[role='navigation']");
            if (navigationElement != null) {
                loadMoreConversation(navigationElement, 1, 10);
                waitDefault();
                WebElement gridElement = getElementByCssSelector(navigationElement, "ul[role='grid']");
                if (gridElement != null) {
                    List<WebElement> conversationRowElements = getElementsByCssSelector(gridElement, "li[role='row']");
                    if (conversationRowElements != null && !conversationRowElements.isEmpty()) {
                        for (WebElement rowElement : conversationRowElements) {
                            if (rowElement != null) {
                                //check if conversation is group
                                if (!isElementExist(rowElement, "div._55lu")) {
                                    WebElement timestampElement = getElementByCssSelector(rowElement, "abbr.timestamp");
                                    if (timestampElement != null) {
                                        String timestampStrVal = timestampElement.getAttribute("data-utime");
                                        if (!TextUtils.isEmpty(timestampStrVal)) {
                                            double timestampDouble;
                                            try {
                                                timestampDouble = Double.parseDouble(timestampStrVal);
                                            } catch (NumberFormatException e) {
                                                Logger.catching(e);
                                                timestampDouble = 0;
                                            }
                                            long timestamp = (long) (timestampDouble * 1000);
                                            if (timestamp >= sinceTimeMillis) {
                                                WebElement linkElement = getElementByCssSelector(rowElement, "a[role='link']");
                                                if (linkElement != null) {
                                                    String messengerLink = linkElement.getAttribute("data-href");
                                                    if (!TextUtils.isEmpty(messengerLink)) {
                                                        URI uri = URI.create(messengerLink);
                                                        String[] uriPaths = uri.getPath().split("/");
                                                        if (uriPaths.length >= 4) {
                                                            String userId = uriPaths[3];
                                                            User user = new User(userId, User.TYPE_USER_NAME);
                                                            WebElement userNameElement = getElementByCssSelector(rowElement, "span._1ht6");
                                                            if (userNameElement != null) {
                                                                String userName = userNameElement.getText();
                                                                if (!TextUtils.isEmpty(userName)) {
                                                                    user.setName(userName);
                                                                }
                                                            }
                                                            listUser.add(user);
                                                        }
                                                    }
                                                }
                                            } else {
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return listUser;
    }

    private void loadMoreConversation(WebElement parentElement, int currentPage, int maxPage) {
        if (currentPage <= maxPage) {
            waitDefault();
            WebElement loadMoreElement = getElementByCssSelector(parentElement, "div._19hf");
            WebElement loadMoreLinkElement = getElementByCssSelector(loadMoreElement, "a");
            if (loadMoreLinkElement != null) {
                scrollToElement(loadMoreElement);
                waitDefault();
                WebElement continueLoadMoreElement = waitUntil(ExpectedConditions.presenceOfNestedElementLocatedBy(loadMoreElement, By.cssSelector("a")));
                if (continueLoadMoreElement != null) {
                    loadMoreConversation(parentElement, currentPage + 1, maxPage);
                }
            }
        }
    }

    public final List<User> getGroupMembers(String groupUrl) {
        Logger.debug(TAG, "#getGroupMembers: " + groupUrl);
        List<User> members = new ArrayList<>();
        String convertedUrl = Utils.convertToFbWebUrl(groupUrl);
        if (!TextUtils.isEmpty(convertedUrl)) {
            Group group = Group.fromUrl(convertedUrl);
            if (group != null) {
                openUrl(group.getGroupMemberUrl());
                waitDefault();
                WebElement memberListWrapElement = getElementById("groupsMemberSection_recently_joined");
                if (memberListWrapElement == null) {
                    memberListWrapElement = getElementById("groupsMemberSection_all_members");
                }
                if (memberListWrapElement != null) {
                    loadMoreGroupMembers(memberListWrapElement, 1, FB_LOAD_MORE_MAX_PAGE);
                    List<WebElement> listMemberItem = getElementsByCssSelector(memberListWrapElement, "div[data-name='GroupProfileGridItem']");
                    if (listMemberItem != null && !listMemberItem.isEmpty()) {
                        listMemberItem.parallelStream().forEach(memberItem -> {
                            if (memberItem != null) {
                                WebElement userLinkElement = getElementByCssSelector(memberItem, "a.lfloat._ohe");
                                if (userLinkElement != null) {
                                    String userLink = userLinkElement.getAttribute("href");
                                    User user = new User(userLink);
                                    user.setFollowable(true);
                                    if (isElementExist(memberItem, "div.fsl.fwb.fcb")) {
                                        WebElement profileNameElement = getElementByCssSelector(memberItem, "div.fsl.fwb.fcb");
                                        if (profileNameElement != null) {
                                            user.setName(profileNameElement.getText());
                                        }
                                    }
                                    List<WebElement> addFriendButtonElements = getElementsByCssSelector(memberItem, "button.FriendRequestAdd");
                                    WebElement addFriendButtonElement = null;
                                    if (addFriendButtonElements != null && !addFriendButtonElements.isEmpty()) {
                                        addFriendButtonElement = addFriendButtonElements.get(0);
                                    }
                                    if (isElementPresentAndDisplayed(addFriendButtonElement)) {
                                        user.setCanAddFriend(true);
                                    }
                                    members.add(user);
                                }
                            }
                        });
                    }
                }
            }
        }
        return members;
    }

    private void loadMoreGroupMembers(WebElement parentElement, int currentPage, int maxPage) {
        if (currentPage <= maxPage) {
            waitDefault();
            List<WebElement> loadMoreElements = getElementsByCssSelector(parentElement, "div.morePager");
            WebElement loadMoreElement = null;
            if (loadMoreElements != null && !loadMoreElements.isEmpty()) {
                loadMoreElement = loadMoreElements.get(0);
            }
            if (isElementPresentAndDisplayed(loadMoreElement)) {
                scrollToElement(loadMoreElement);
                waitDefault();
                if (isElementExist("div.morePager a")) {
                    WebElement hrefElement = waitUntil(ExpectedConditions.elementToBeClickable(By.cssSelector("div.morePager a")));
                    if (hrefElement != null) {
                        loadMoreGroupMembers(parentElement, currentPage + 1, maxPage);
                    }
                }
            }
        }
    }

    public List<User> getFriendList(String userProfileUrl) {
        Logger.debug(TAG, "#getFriendList: " + userProfileUrl);
        List<User> friendList = new ArrayList<>();
        if (!TextUtils.isEmpty(userProfileUrl)) {
            openUrl(new User(userProfileUrl).getFriendsUrl());
            waitDefault();
            WebElement timelineMedleyElement = getElementById("timeline-medley");
            if (timelineMedleyElement != null) {
                loadMoreFriendList(timelineMedleyElement, 1, FB_LOAD_MORE_MAX_PAGE);
                List<WebElement> friendItemElements = getElementsByCssSelector(timelineMedleyElement, "li._698");
                if (friendItemElements != null && !friendItemElements.isEmpty()) {
                    friendItemElements.parallelStream().forEach(friendItemElement -> {
                        if (friendItemElement != null) {
                            WebElement userLink = getElementByCssSelector(friendItemElement, "a.lfloat._ohe");
                            if (userLink != null) {
                                String href = userLink.getAttribute("href");
                                User user = new User(href);
                                user.setFollowable(true);
                                if (isElementExist(friendItemElement, "div.fsl.fwb.fcb")) {
                                    WebElement profileNameElement = getElementByCssSelector(friendItemElement, "div.fsl.fwb.fcb");
                                    if (profileNameElement != null) {
                                        user.setName(profileNameElement.getText());
                                    }
                                }
                                List<WebElement> addFriendButtonElements = getElementsByCssSelector(friendItemElement, "button.FriendRequestAdd");
                                WebElement addFriendButtonElement = null;
                                if (addFriendButtonElements != null && !addFriendButtonElements.isEmpty()) {
                                    addFriendButtonElement = addFriendButtonElements.get(0);
                                }
                                if (isElementPresentAndDisplayed(addFriendButtonElement)) {
                                    user.setCanAddFriend(true);
                                }
                                friendList.add(user);
                            }
                        }
                    });
                }
            }
        }
        return friendList;
    }

    private void loadMoreFriendList(WebElement parentElement, int currentPage, int maxPage) {
        if (currentPage <= maxPage) {
            waitDefault();
            WebElement loadMoreElement = getElementByCssSelector(parentElement, "img._359");
            if (isElementPresentAndDisplayed(loadMoreElement)) {
                scrollToElement(loadMoreElement);
                waitDefault();
                waitUntil(ExpectedConditions.attributeToBe(loadMoreElement, "class", "_359 img"));
                loadMoreFriendList(parentElement, currentPage + 1, maxPage);
            }
        }
    }

    public final List<User> getListPeopleYouMayKnow() {
        Logger.debug(TAG, "#getListPeopleYouMayKnow");
        List<User> userList = new ArrayList<>();
        openUrl(FB_PEOPLE_YOU_MAY_KNOW_URL);
        wait(1, TimeUnit.SECONDS);
        WebElement fbSearchResultsBoxElement = getElementById("fbSearchResultsBox");
        if (fbSearchResultsBoxElement != null) {
            loadMorePeopleYouMayKnowList(fbSearchResultsBoxElement, 1, FB_LOAD_MORE_MAX_PAGE);
            List<WebElement> friendBrowserElements = getElementsByCssSelector(fbSearchResultsBoxElement, "li.friendBrowserListUnit");
            if (friendBrowserElements != null && !friendBrowserElements.isEmpty()) {
                friendBrowserElements.parallelStream().forEach(friendItemElement -> {
                    if (friendItemElement != null) {
                        WebElement userLink = getElementByCssSelector(friendItemElement, "a.lfloat._ohe");
                        if (userLink != null) {
                            String href = userLink.getAttribute("href");
                            User user = new User(href);
                            user.setFollowable(true);
                            user.setCanAddFriend(true);
                            if (isElementExist(friendItemElement, "div.friendBrowserNameTitle")) {
                                WebElement profileNameElement = getElementByCssSelector(friendItemElement, "div.friendBrowserNameTitle");
                                if (profileNameElement != null) {
                                    user.setName(profileNameElement.getText());
                                }
                            }
                            userList.add(user);
                        }
                    }
                });
            }
        }
        return userList;
    }

    private void loadMorePeopleYouMayKnowList(WebElement parentElement, int currentPage, int maxPage) {
        if (currentPage <= maxPage) {
            waitDefault();
            WebElement loadMoreElement = getElementByCssSelector(parentElement, "div.friendBrowserMorePager");
            if (isElementPresentAndDisplayed(loadMoreElement)) {
                scrollToElement(loadMoreElement);
                waitDefault();
                if (isElementExist("div.friendBrowserMorePager a")) {
                    WebElement hrefElement = waitUntil(ExpectedConditions.elementToBeClickable(By.cssSelector("div.friendBrowserMorePager a")));
                    if (hrefElement != null) {
                        loadMorePeopleYouMayKnowList(parentElement, currentPage + 1, maxPage);
                    }
                }
            }
        }
    }

    public final void unfollow(String userProfileUrl, IActionResponse<Void, UnFollowErrorResponse> response) {
        Logger.debug(TAG, "#unfollow: " + userProfileUrl);
        String convertedUrl = Utils.convertToFbWebUrl(userProfileUrl);
        if (!TextUtils.isEmpty(convertedUrl)) {
            openUrl(convertedUrl);
            WebElement timelineProfileActionElement = getElementById("pagelet_timeline_profile_actions");
            if (timelineProfileActionElement != null) {
                List<WebElement> followStatusElements = getElementsByCssSelector(timelineProfileActionElement, "button[data-status='follow']");
                WebElement followStatusElement = null;
                if (followStatusElements != null && !followStatusElements.isEmpty()) {
                    followStatusElement = followStatusElements.get(0);
                }
                if (followStatusElement != null) {
                    //followed
                    moveToElement(followStatusElement);
                    WebElement menuElement = getElementByCssSelector("ul[role='menu']");
                    if (menuElement != null) {
                        List<WebElement> menuItemElements = getElementsByCssSelector(menuElement, "a[rel='async-post']");
                        if (menuItemElements != null && !menuItemElements.isEmpty()) {
                            WebElement unFollowButtonElement = menuItemElements.stream().filter(webElement -> {
                                if (webElement != null) {
                                    String ajaxify = webElement.getAttribute("ajaxify");
                                    return !TextUtils.isEmpty(ajaxify) && ajaxify.contains("unfollow_profile.php");
                                }
                                return false;
                            }).findFirst().orElse(null);
                            if (unFollowButtonElement != null) {
                                moveCursorAndClickOnUnfollowButton(followStatusElement, unFollowButtonElement);
                                IActionResponse.onSuccess(response);
                                return;
                            }
                        }
                    }
                    IActionResponse.onErrorSyncServer(response, new UnFollowErrorResponse(UnFollowErrorResponse.CODE_UNFOLLOW_BUTTON_ABSENCE, String.format("UserProfile: %s", userProfileUrl)));
                } else {
                    //not followed
                    IActionResponse.onError(response, new UnFollowErrorResponse(UnFollowErrorResponse.CODE_NOT_FOLLOWING_YET, String.format("UserProfile: %s", userProfileUrl)));
                }
            } else {
                IActionResponse.onErrorSyncServer(response, new UnFollowErrorResponse(UnFollowErrorResponse.CODE_UNFOLLOW_BUTTON_ABSENCE, String.format("UserProfile: %s", userProfileUrl)));
            }
        } else {
            IActionResponse.onErrorSyncServer(response, new UnFollowErrorResponse(UnFollowErrorResponse.CODE_INVALID_URL, String.format("UserProfile: %s", userProfileUrl)));
        }
    }

    private void moveCursorAndClickOnUnfollowButton(WebElement followStatusElement, WebElement unFollowButtonElement) {
        int followStatusElementY = followStatusElement.getLocation().getY();
        int unFollowButtonElementY = unFollowButtonElement.getLocation().getY();
        int distance = unFollowButtonElementY - followStatusElementY;
        waitDefault();
        Actions actions = new Actions(getWebDriver());
        int k = distance / 5;
        for (int i = 0; i < 5; i++) {
            actions.moveByOffset(0, k);
        }
        actions.pause(1000);
        actions.click();
        actions.build().perform();
    }
}
