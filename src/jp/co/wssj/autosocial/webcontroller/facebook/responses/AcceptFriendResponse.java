package jp.co.wssj.autosocial.webcontroller.facebook.responses;

/**
 * Created by Nguyen Huu Ta on 6/12/2017.
 */

public class AcceptFriendResponse extends ErrorResponse {

    public AcceptFriendResponse(int code) {
        super(code);
    }
}
