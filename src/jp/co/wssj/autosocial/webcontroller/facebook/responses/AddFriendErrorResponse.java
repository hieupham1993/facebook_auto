package jp.co.wssj.autosocial.webcontroller.facebook.responses;

/**
 * Created by HieuPT on 11/16/2017.
 */
public class AddFriendErrorResponse extends ErrorResponse {

    public static final int CODE_TARGET_USER_ADD_FRIEND_DISABLED = 1;

    public static final int CODE_FRIEND_ALREADY = 2;

    public static final int CODE_FRIEND_REQUEST_SENT = 3;

    public AddFriendErrorResponse(int code) {
        super(code);
    }

    public AddFriendErrorResponse(int code, String message) {
        super(code, message);
    }
}
