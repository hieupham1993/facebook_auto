package jp.co.wssj.autosocial.webcontroller.facebook.responses;

import jp.co.wssj.autosocial.utils.Constants;

public class ErrorResponse {

    public static final int CODE_INVALID_URL = -1000;

    public static final int CODE_LOAD_PAGE_ERROR = -1001;

    private int code;

    private String message;

    public ErrorResponse(int code) {
        this(code, Constants.EMPTY_STRING);
    }

    public ErrorResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
