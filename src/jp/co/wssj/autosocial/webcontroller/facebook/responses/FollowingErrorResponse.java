package jp.co.wssj.autosocial.webcontroller.facebook.responses;

public class FollowingErrorResponse extends ErrorResponse {

    public static final int CODE_TARGET_USER_FOLLOW_DISABLE = 1;

    public static final int CODE_FOLLOWED = 2;

    public FollowingErrorResponse(int code) {
        super(code);
    }

    public FollowingErrorResponse(int code, String message) {
        super(code, message);
    }
}
