package jp.co.wssj.autosocial.webcontroller.facebook.responses;

import jp.co.wssj.autosocial.utils.Logger;
import jp.co.wssj.autosocial.utils.Utils;

public interface IActionResponse<T, E extends ErrorResponse> {

    static <T, E extends ErrorResponse> void onSuccess(IActionResponse<T, E> delegate) {
        onSuccess(delegate, null);
    }

    static <T, E extends ErrorResponse> void onSuccess(IActionResponse<T, E> delegate, T data) {
        Logger.debug("#onSuccess");
        if (delegate != null) {
            delegate.onSuccess(data);
        }
    }

    static <T, E extends ErrorResponse> void onError(IActionResponse<T, E> delegate, E error) {
        onError(delegate, error, false);
    }

    static <T, E extends ErrorResponse> void onErrorSyncServer(IActionResponse<T, E> delegate, E error) {
        onError(delegate, error, true);
    }

    static <T, E extends ErrorResponse> void onError(IActionResponse<T, E> delegate, E error, boolean sync) {
        String message;
        if (error != null) {
            message = "#onError: |" + error.getClass().getName() + "|code=" + error.getCode() + "|message=" + error.getMessage() + "|";
        } else {
            message = "#onError";
        }
        Logger.debug(message);
        if (sync) {
            Utils.sendLogToServer(message);
        }
        if (delegate != null) {
            delegate.onError(error);
        }
    }

    default void onSuccess(T data) {
    }

    default void onError(E errorResponse) {
    }
}
