package jp.co.wssj.autosocial.webcontroller.facebook.responses;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class LoginErrorResponse extends ErrorResponse {

    public static final int CODE_USER_NAME_OR_PASSWORD_EMPTY = 1;

    public static final int CODE_USER_NAME_OR_PASSWORD_INVALID = 2;

    public static final int CODE_SECURE_CHECK_POINT = 3;

    public LoginErrorResponse(int code) {
        super(code);
    }

    public LoginErrorResponse(int code, String message) {
        super(code, message);
    }
}
