package jp.co.wssj.autosocial.webcontroller.facebook.responses;

public class MessagingErrorResponse extends ErrorResponse {

    public static final int CODE_INVALID_USER_ID = 1;

    public static final int CODE_MESSAGE_EMPTY = 2;

    public static final int CODE_MESSENGER_LOADED_FAIL = 3;

    public static final int CODE_SEND_ERROR = 4;

    public static final int CODE_SECURITY_CHECK = 5;

    public MessagingErrorResponse(int code) {
        super(code);
    }

    public MessagingErrorResponse(int code, String message) {
        super(code, message);
    }
}
