package jp.co.wssj.autosocial.webcontroller.facebook.responses;

/**
 * Created by HieuPT on 12/11/2017.
 */
public class UnFollowErrorResponse extends ErrorResponse {

    public static final int CODE_NOT_FOLLOWING_YET = 1;

    public static final int CODE_UNFOLLOW_BUTTON_ABSENCE = 2;

    public UnFollowErrorResponse(int code) {
        super(code);
    }

    public UnFollowErrorResponse(int code, String message) {
        super(code, message);
    }
}
