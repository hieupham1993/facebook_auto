package jp.co.wssj.autosocial.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by HieuPT on 1/29/2018.
 */
public class TestUtils {

    @Test
    public void simpleLineEndText_1() {
        String text = "Hello\nHow are you";
        String convertedText = Utils.simpleLineEndText(text);
        Assert.assertEquals("Hello\nHow are you", convertedText);
    }

    @Test
    public void simpleLineEndText_2() {
        String text = "Hello\r\nHow are you";
        String convertedText = Utils.simpleLineEndText(text);
        Assert.assertEquals("Hello\nHow are you", convertedText);
    }

    @Test
    public void simpleLineEndText_3() {
        String text = "Hello\rHow are you";
        String convertedText = Utils.simpleLineEndText(text);
        Assert.assertEquals("Hello\nHow are you", convertedText);
    }

    @Test
    public void simpleLineEndText_4() {
        String text = "Hello\n\rHow are you";
        String convertedText = Utils.simpleLineEndText(text);
        Assert.assertEquals("Hello\nHow are you", convertedText);
    }

    @Test
    public void simpleLineEndText_5() {
        String text = "Hello\n\r\nHow are you";
        String convertedText = Utils.simpleLineEndText(text);
        Assert.assertEquals("Hello\n\nHow are you", convertedText);
    }

    @Test
    public void simpleLineEndText_6() {
        String text = "Hello\r\n\r\nHow are you";
        String convertedText = Utils.simpleLineEndText(text);
        Assert.assertEquals("Hello\n\nHow are you", convertedText);
    }

    @Test
    public void simpleLineEndText_7() {
        String text = "Hello\r\n\r\r\nHow are you";
        String convertedText = Utils.simpleLineEndText(text);
        Assert.assertEquals("Hello\n\n\nHow are you", convertedText);
    }

    @Test
    public void simpleLineEndText_8() {
        String text = "Hello\r\n\n\r\nHow are you";
        String convertedText = Utils.simpleLineEndText(text);
        Assert.assertEquals("Hello\n\n\nHow are you", convertedText);
    }
}
