package jp.co.wssj.autosocial.webcontroller.facebook;

import org.apache.commons.lang3.SystemUtils;
import org.joda.time.DateTime;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import java.util.List;

import jp.co.wssj.autosocial.exceptions.WebDriverNotFoundException;
import jp.co.wssj.autosocial.exceptions.WebDriverNotSupportedException;
import jp.co.wssj.autosocial.models.entities.Article;
import jp.co.wssj.autosocial.models.entities.User;
import jp.co.wssj.autosocial.utils.WebDriverUtils;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.IActionResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.LikeErrorResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.LoginErrorResponse;
import jp.co.wssj.autosocial.webcontroller.facebook.responses.MessagingErrorResponse;

/**
 * Created by HieuPT on 1/29/2018.
 */
public class TestFacebookWebController {

    private static final boolean DEBUG = false;

    @BeforeClass
    public static void setupWebDriver() throws WebDriverNotFoundException, WebDriverNotSupportedException {
        String userDir = System.getProperty("user.dir");
        if (DEBUG) {
            if (SystemUtils.IS_OS_WINDOWS) {
                System.setProperty("webdriver.chrome.driver", userDir + "/webdriver/chromedriver.exe");
            } else if (SystemUtils.IS_OS_MAC) {
                System.setProperty("webdriver.chrome.driver", userDir + "/webdriver/chromedriver");
            }
        } else {
            if (SystemUtils.IS_OS_WINDOWS) {
                System.setProperty("phantomjs.binary.path", userDir + "/webdriver/phantomjs.exe");
            } else if (SystemUtils.IS_OS_MAC) {
                System.setProperty("phantomjs.binary.path", userDir + "/webdriver/phantomjs");
            }
        }
        initWebDriver();
        controller = new FacebookWebController(webDriver);
        login("taikhoantest456@gmail.com", "mandem456");
//        login("obito.test01@gmail.com", "obito1234");
    }

    private static FacebookWebController controller;

    private static WebDriver webDriver;

    private static User loggedUser;

    private static void initWebDriver() throws WebDriverNotFoundException, WebDriverNotSupportedException {
        webDriver = WebDriverUtils.createWebDriver();
        webDriver.get(FacebookWebController.FB_BASE_URL);
    }

    private static void login(String username, String password) {
        controller.login(username, password, new IActionResponse<Void, LoginErrorResponse>() {

            @Override
            public void onSuccess(Void data) {
                loggedUser = controller.getMyUserInfo();
            }
        });
    }

    @Test
    public void likeArticle_1() {
        String url = "https://www.facebook.com/photo.php?fbid=1595501113899059&set=a.265603380222179.63065.100003175566735&type=3";
        controller.likeArticle(url, new IActionResponse<Void, LikeErrorResponse>() {

            @Override
            public void onSuccess(Void data) {
                Assert.assertTrue(true);
            }

            @Override
            public void onError(LikeErrorResponse errorResponse) {
                Assert.fail("Error code: " + errorResponse.getCode());
            }
        });
    }

    @Test
    public void likeArticle_2() {
        String url = "https://www.facebook.com/phamtpthao96/posts/1973755309618342";
        controller.likeArticle(url, new IActionResponse<Void, LikeErrorResponse>() {

            @Override
            public void onSuccess(Void data) {
                Assert.assertTrue(true);
            }

            @Override
            public void onError(LikeErrorResponse errorResponse) {
                Assert.fail("Error code: " + errorResponse.getCode());
            }
        });
    }

    @Test
    public void likeArticle_3() {
        String url = "https://www.facebook.com/thaonguyen.hoang.376/videos/vb.100004075380260/1361577513988082/?type=2&video_source=user_video_tab";
        controller.likeArticle(url, new IActionResponse<Void, LikeErrorResponse>() {

            @Override
            public void onSuccess(Void data) {
                Assert.assertTrue(true);
            }

            @Override
            public void onError(LikeErrorResponse errorResponse) {
                Assert.fail("Error code: " + errorResponse.getCode());
            }
        });
    }

    @Test
    public void likeArticle_4() {
        String url = "https://www.facebook.com/permalink.php?story_fbid=444759052605308&id=100012136512192";
        controller.likeArticle(url, new IActionResponse<Void, LikeErrorResponse>() {

            @Override
            public void onSuccess(Void data) {
                Assert.assertTrue(true);
            }

            @Override
            public void onError(LikeErrorResponse errorResponse) {
                Assert.fail("Error code: " + errorResponse.getCode());
            }
        });
    }

    @Test
    public void likeArticle_5() {
        String url = "https://www.facebook.com/kenh18cong/videos/2015431768715429/";
        controller.likeArticle(url, new IActionResponse<Void, LikeErrorResponse>() {

            @Override
            public void onSuccess(Void data) {
                Assert.assertTrue(true);
            }

            @Override
            public void onError(LikeErrorResponse errorResponse) {
                Assert.fail("Error code: " + errorResponse.getCode());
            }
        });
    }

    @Test
    public void likeArticle_6() {
        String url = "https://www.facebook.com/Theteenagemind/photos/a.298003003673610.1073741828.297994060341171/1068294709977765/?type=3";
        controller.likeArticle(url, new IActionResponse<Void, LikeErrorResponse>() {

            @Override
            public void onSuccess(Void data) {
                Assert.assertTrue(true);
            }

            @Override
            public void onError(LikeErrorResponse errorResponse) {
                Assert.fail("Error code: " + errorResponse.getCode());
            }
        });
    }

    @Test
    public void likeArticle_7() {
        String url = "https://www.facebook.com/100006543614825/posts/1962866137274821/";
        controller.likeArticle(url, new IActionResponse<Void, LikeErrorResponse>() {

            @Override
            public void onSuccess(Void data) {
                Assert.assertTrue(true);
            }

            @Override
            public void onError(LikeErrorResponse errorResponse) {
                Assert.fail("Error code: " + errorResponse.getCode());
            }
        });
    }

    @Test
    public void sendMessage_1() {
        String targetUsername = "kushina.uzumakiki.7";
        String message = "I love you\nSo much";
        controller.sendMessageTo(targetUsername, message, new IActionResponse<Void, MessagingErrorResponse>() {

            @Override
            public void onSuccess(Void data) {
                Assert.assertTrue(true);
            }

            @Override
            public void onError(MessagingErrorResponse errorResponse) {
                Assert.fail("Error code: " + errorResponse.getCode());
            }
        });
    }

    @Test
    public void sendMessage_2() {
        String targetUsername = "nhi.suri.79274";
        String message = "Oh.\nMe too.\nCould you please accept my request? :D";
        controller.sendMessageTo(targetUsername, message, new IActionResponse<Void, MessagingErrorResponse>() {

            @Override
            public void onSuccess(Void data) {
                Assert.assertTrue(true);
            }

            @Override
            public void onError(MessagingErrorResponse errorResponse) {
                Assert.fail("Error code: " + errorResponse.getCode());
            }
        });
    }

    @Test
    public void sendMessage_3() {
        String targetUsername = "binhyen.nhe.39";
        String message = "I sent you one line of message. Please reply to me. Thank you so much. See ya.";
        controller.sendMessageTo(targetUsername, message, new IActionResponse<Void, MessagingErrorResponse>() {

            @Override
            public void onSuccess(Void data) {
                Assert.assertTrue(true);
            }

            @Override
            public void onError(MessagingErrorResponse errorResponse) {
                Assert.fail("Error code: " + errorResponse.getCode());
            }
        });
    }

    @Test
    public void getFanPageMostRecentArticles_1() {
        String pageUrl = "https://www.facebook.com/ponta.official/";
        List<Article> articles = controller.getFanPageMostRecentArticles(pageUrl);
        Assert.assertTrue(articles != null);
    }

    @Test
    public void getFanPageMostRecentArticles_2() {
        String pageUrl = "https://www.facebook.com/与沢翼-221125021364001/";
        List<Article> articles = controller.getFanPageMostRecentArticles(pageUrl);
        Assert.assertTrue(articles != null);
    }

    @Test
    public void getArticleReactions_1() {
        String url = "https://www.facebook.com/ponta.official/photos/a.268764346483082.84705.199120883447429/2256801904345973/?type=3";
        List<User> reactionList = controller.getArticleReactions(url, false);
        Assert.assertTrue(!reactionList.isEmpty());
    }

    @Test
    public void getArticleReactions_2() {
        String url = "https://www.facebook.com/tranthidieu.ninh/posts/1933832500213088";
        List<User> reactionList = controller.getArticleReactions(url, false);
        Assert.assertTrue(!reactionList.isEmpty());
    }

    @Test
    public void getArticleReactions_3() {
        String url = "https://www.facebook.com/dongnhi/videos/10159991008915541/";
        List<User> userList = controller.getArticleReactions(url);
        Assert.assertTrue(!userList.isEmpty());
    }

    @Test
    public void getArticleReactions_4() {
        String url = "https://www.facebook.com/dongnhi/posts/10159994479965541";
        List<User> userList = controller.getArticleReactions(url);
        Assert.assertTrue(!userList.isEmpty());
    }

    @Test
    public void getArticleReactions_5() {
        String url = "https://www.facebook.com/sieuanhhungMarvel/photos/a.380899025303933.88033.380890675304768/1756998111027344/?type=3";
        List<User> userList = controller.getArticleReactions(url);
        Assert.assertTrue(!userList.isEmpty());
    }

    @Test
    public void getArticleReactions_6() {
        String url = "https://www.facebook.com/photo.php?fbid=1271238959686914&set=a.166500523494102.53027.100004024651082&type=3";
        List<User> userList = controller.getArticleReactions(url);
        Assert.assertTrue(!userList.isEmpty());
    }

    @Test
    public void getArticleReactions_7() {
        String url = "https://www.facebook.com/photo.php?fbid=184600428956422&set=a.121481618601637.1073741828.100022193988952&type=3";
        List<User> userList = controller.getArticleReactions(url);
        Assert.assertTrue(!userList.isEmpty());
    }

    @Test
    public void getGroupMembers_1() {
        String groupUrl = "https://www.facebook.com/groups/1616461948620959/";
        List<User> userList = controller.getGroupMembers(groupUrl);
        Assert.assertTrue(!userList.isEmpty());
    }

    @Test
    public void getGroupMembers_2() {
        String groupUrl = "https://www.facebook.com/groups/fptjapan/";
        List<User> userList = controller.getGroupMembers(groupUrl);
        Assert.assertTrue(!userList.isEmpty());
    }

    @Test
    public void getListConversation_1() {
        List<User> userList = controller.getListConversation();
        Assert.assertTrue(!userList.isEmpty());
    }

    @Test
    public void getListConversation_2() {
        long since = DateTime.now().withTimeAtStartOfDay().minusWeeks(1).getMillis();
        List<User> userList = controller.getListConversation(since);
        Assert.assertTrue(!userList.isEmpty());
    }

    @Test
    public void getListConversation_3() {
        long since = DateTime.now().withTimeAtStartOfDay().getMillis();
        List<User> userList = controller.getListConversation(since);
        Assert.assertTrue(userList.isEmpty());
    }

    @Test
    public void getListConversation_4() {
        long since = DateTime.now().withTimeAtStartOfDay().minusMonths(1).getMillis();
        List<User> userList = controller.getListConversation(since);
        Assert.assertTrue(!userList.isEmpty());
    }

    @Test
    public void getUserMostRecentArticles_1() {
        String userUrl = "https://www.facebook.com/arriell.wff";
        int limit = 1;
        List<Article> articles = controller.getUserMostRecentArticles(userUrl, limit);
        Assert.assertNull(articles);
    }

    @Test
    public void getUserMostRecentArticles_2() {
        String userUrl = "https://www.facebook.com/sakura.haruno.750";
        int limit = 1;
        List<Article> articles = controller.getUserMostRecentArticles(userUrl, limit);
        Assert.assertNotNull(articles);
        Assert.assertTrue(articles.size() == 1);
    }

    @Test
    public void getUserMostRecentArticles_3() {
        String userUrl = "https://www.facebook.com/profile.php?id=100010516632972";
        int limit = 1;
        List<Article> articles = controller.getUserMostRecentArticles(userUrl, limit);
        Assert.assertNotNull(articles);
        Assert.assertTrue(articles.size() == 1);
    }

    @Test
    public void getUserMostRecentArticles_4() {
        String url = "https://www.facebook.com/profile.php?id=100022193988952";
        List<Article> articles = controller.getUserMostRecentArticles(url);
        Assert.assertTrue(articles.size() == 20);
    }

    @Test
    public void getUserMostRecentArticles_5() {
        String url = "https://www.facebook.com/nhandt.2512";
        int limit = 1;
        List<Article> articles = controller.getUserMostRecentArticles(url, limit);
        Assert.assertTrue(articles.size() == limit);
    }

    @Test
    public void getFollowerList_1() {
        String profileUrl = "https://www.facebook.com/pe.ruby9x";
        List<User> followerUsers = controller.getFollowerList(profileUrl, true);
        Assert.assertTrue(!followerUsers.isEmpty());
    }

    @Test
    public void getFollowingList_1() {
        String profileUrl = "https://www.facebook.com/pe.ruby9x";
        List<User> followingUsers = controller.getFollowingList(profileUrl);
        Assert.assertTrue(!followingUsers.isEmpty());
    }

    @Test
    public void getFollowingList_2() {
        String profileUrl = "https://www.facebook.com/YokoHosoi1107";
        List<User> followingUsers = controller.getFollowingList(profileUrl);
        Assert.assertTrue(followingUsers.isEmpty());
    }

    @Test
    public void getFollowingList_3() {
        String profileUrl = "https://www.facebook.com/tiara8tiara";
        List<User> followingUsers = controller.getFollowingList(profileUrl);
        Assert.assertTrue(!followingUsers.isEmpty());
    }

    @Test
    public void getFollowingList_4() {
        String profileUrl = "https://www.facebook.com/trangksbkk56";
        List<User> followingUsers = controller.getFollowingList(profileUrl);
        Assert.assertTrue(!followingUsers.isEmpty());
    }

    @Test
    public void getListPeopleYouMayKnow_1() {
        List<User> peoples = controller.getListPeopleYouMayKnow();
        Assert.assertTrue(!peoples.isEmpty());
    }

    @Test
    public void getSelfMostRecentArticlesViaActivityLog_1() {
        List<Article> articles = controller.getSelfMostRecentArticlesViaActivityLog(loggedUser);
        Assert.assertNotNull(articles);
        Assert.assertFalse(articles.isEmpty());
    }

    @Test
    public void getGroupMostRecentArticlesViaGroupTimeline_1() {
        String groupUrl = "https://www.facebook.com/groups/1629036267152599/";
        List<Article> articles = controller.getGroupMostRecentArticlesViaGroupTimeline(groupUrl);
        Assert.assertNotNull(articles);
        Assert.assertFalse(articles.isEmpty());
    }

    @Test
    public void getGroupMostRecentArticlesViaGroupTimeline_2() {
        String groupUrl = "https://www.facebook.com/groups/219426461939942/";
        List<Article> articles = controller.getGroupMostRecentArticlesViaGroupTimeline(groupUrl);
        Assert.assertNotNull(articles);
        Assert.assertFalse(articles.isEmpty());
    }

    @AfterClass
    public static void destroy() {
        if (webDriver != null) {
            try {
                webDriver.close();
                webDriver.quit();
            } catch (WebDriverException ignored) {
            } finally {
                webDriver = null;
            }
        }
    }
}
